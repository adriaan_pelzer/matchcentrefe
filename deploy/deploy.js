var filesToPatch = [
    {
        file: '../assets/shared/scripts/matchCentre.core.js',
        replacements: [
            [ /initialSiteContent: \'http\:\/\/[\d\.]+:\d+/, 'initialSiteContent: \'http://54.217.155.2:8000' ],
        ],
    },
];

var fs = require ( 'fs' );
var colors = require ( 'colors' );
var _ = require ( 'underscore' );

var logErr = function ( message ) {
    console.log ( message.red );
}

var logWarn = function ( message ) {
    console.log ( message.yellow );
}

var log = function ( message ) {
    console.log ( message.green );
}

var patchFile = function ( file, replacements, replacewiths ) {
    fs.readFile ( file, function ( err, content ) {
        var i, Replace, With;
        var contentString = content.toString ();

        if ( err ) {
            logErr ( 'Error parsing ' + file );
            logErr ( err );
        } else {
            for ( i = 0; i < replacements.length; i++ ) {
                Replace = replacements[i], With = replacewiths[i];

                contentString = contentString.replace ( Replace, With );
            }

            fs.writeFile ( file, contentString, function ( err ) {
                if ( err ) {
                    logErr ( 'Error writing ' + file );
                    logErr ( err );
                } else {
                    log ( 'File ' + file + ' patched successfuly' );
                }
            } ); 
        }
    } );
};

var i, j, replacements, replacewiths;

for ( i = 0; i < filesToPatch.length; i++ ) {
    replacements = [], replacewiths = [];

    if ( _.isArray ( filesToPatch[i].replacements ) ) {
        for ( j = 0; j < filesToPatch[i].replacements.length; j++ ) {
            if ( _.isArray ( filesToPatch[i].replacements[j] ) && ( filesToPatch[i].replacements[j].length > 1 ) ) {
                replacements.push ( filesToPatch[i].replacements[j][0] );
                replacewiths.push ( filesToPatch[i].replacements[j][1] );
            }
        }

        patchFile ( filesToPatch[i].file, replacements, replacewiths );
    }
}
