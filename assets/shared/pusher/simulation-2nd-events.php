<?php
// If debugging is required, enable a JSON contentType, and print_r the output to the screen (as below)
// header('Content-Type: application/json; charset=utf8');
// print_r(json_encode($fixtures));
// print_r(json_encode($leagueTable));
// print_r(json_encode($inSummary));
// print_r(json_encode($matchSummary));
// print_r(json_encode($newsFeed));
// print_r(json_encode($lineUps));
// print_r(json_encode($attack));
// print_r(json_encode($generalPlay));
// print_r(json_encode($distribution));
// print_r(json_encode($defenceAndDiscipline));

require('pusher.php');

// Martin keys
// ----
// $app_id = '61643';
// $app_key = '8c8898a3cfd2738c2673';
// $app_secret = '9e8c4ddc829a6aa88824';

// Adriaan keys
//----
$app_id = '65117'; 
$app_key = '6fdc6505d520d980962e'; 
$app_secret = '81f12bed216e3ecf0512'; 

$pusher = new Pusher($app_key,$app_secret,$app_id);

// Simulate a 2nd pusher event to the Fixtures component
$fixtures = array(
	'fixtures' => array(
		'content' => array(
			array(
				'active' => true,
				'eventType' => 'yellow-card',
				'id' => 3
			)
		)
	)
);

// Simulate a 2nd pusher event to the League position component
$leagueTable = array(
	'leagueTable' => array(
		'metaData' => array(
			'lastUpdated' => '03/03/2014 03:45'
		),		
		'content' => array(
			array(
				'teamName' => 'Chelsea',
				'gamesPlayed' => 40,
				'points' => 89
			),
			array(
				'teamName' => 'Manchester City',
				'gamesPlayed' => 40,
				'points' => 78
			),
			array(
				'teamName' => 'Manchester Utd',
				'gamesPlayed' => 40,
				'points' => 75
			),
			array(
				'teamName' => 'Arsenal',
				'gamesPlayed' => 40,
				'points' => 73
			),
			array(
				'teamName' => 'Tottenham',
				'gamesPlayed' => 40,
				'points' => 72
			),
			array(
				'teamName' => 'Fulham',
				'gamesPlayed' => 40,
				'points' => 70
			),
			array(
				'teamName' => 'Everton',
				'gamesPlayed' => 40,
				'points' => 63
			),
			array(
				'teamName' => 'Liverpool',
				'gamesPlayed' => 40,
				'points' => 61
			),
			array(
				'teamName' => 'West Bromwich Albion',
				'gamesPlayed' => 40,
				'points' => 49
			),
			array(
				'teamName' => 'Swansea',
				'gamesPlayed' => 40,
				'points' => 46
			),
			array(
				'teamName' => 'West Ham United',
				'gamesPlayed' => 40,
				'points' => 46
			),
			array(
				'teamName' => 'Norwich City',
				'gamesPlayed' => 40,
				'points' => 44
			),
			array(
				'teamName' => 'Stoke City',
				'gamesPlayed' => 40,
				'points' => 42
			),
			array(
				'teamName' => 'Southampton',
				'gamesPlayed' => 40,
				'points' => 41
			),
			array(
				'teamName' => 'Aston Villa',
				'gamesPlayed' => 40,
				'points' => 41
			),
			array(
				'teamName' => 'Newcastle',
				'gamesPlayed' => 40,
				'points' => 41
			),
			array(
				'teamName' => 'Sunderland',
				'gamesPlayed' => 40,
				'points' => 39
			),
			array(
				'teamName' => 'Cardiff',
				'gamesPlayed' => 40,
				'points' => 36
			),
			array(
				'teamName' => 'Hull',
				'gamesPlayed' => 40,
				'points' => 28
			),
			array(
				'teamName' => 'Crystal Palace',
				'gamesPlayed' => 40,
				'points' => 25
			)
		)
	)
);

// Simulate a 2nd pusher event to the In Summary component
$inSummary = array(
	'inSummary' => array(
		'content' => array(
			array(
				'heading' => 'Possession',
				'percentages' => true,
				'values' => array(
					'home' => 40,
					'away' => 60
				)
			)
		)
	)
);

// Simulate a 2nd pusher event to the Match Summary component
$matchSummary = array(
	'matchSummary' => array(
		'metaData' => array(
			'kickOff' => 'DD3.MM3.YY3 HH3:MM3',
			'referee' => 'Firstname3 Surname3',
			'attendance' => 'xx,xxx3',
			'location' => 'Location name3'
		),
		'content' => array(
			'homeTeam' => array(
				'goalsScored' => array(
					array(
						'minuteOfGoal' => '30',
						'name' => 'Player name'
					),
					array(
						'minuteOfGoal' => '38',
						'name' => 'Player name'
					)
				)
			),
			'awayTeam' => array(
				'goalsScored' => array(
					array(
						'minuteOfGoal' => '34',
						'name' => 'Player name'
					)
				)
			)
		)
	)
);

// Simulate a 2nd pusher event to the News feed component
$newsFeed = array(
	'newsFeed' => array(
		'content' => array(
			array(
				'id' => 12,
				'time' => '16:12',
				'minute' => '12',
				'source' => array(
					'author' => array(
						'company' =>'Publication 12 name',
						"name" => 'Author 12 name'
					),
					'dataOrigin' => 'escenic',
					'iconSource' => '/assets/images/authors/icon-person.png',
					'type' => 'yellow-card'
				),
				'matchTeams' => array(
					'home' => 'Arsenal',
					'away' => 'Tottenham'
				),
				'heading' => array(
					'text' => 'Minute 12 - Yellow Card'
				),
				'contentTypes' => array(
					array(
						'type' => 'paragraph',
						'text' => 'Minute 12, description 1 lorem Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.'
					)
				)
			)
		)	
	)
);

// Simulate a pusher event to the Line Ups feed component
$lineUps = array(
	'lineUps' => array(
		'content' => array(
			'players' => array(
				array(
					'id' => 2,
					'eventUpdate' => array(
						'goalkeeper',
						'substitute-on',
						'substitute-off',
						'yellow-card',
						'red-card',
						'goal-scorer'
					)
				),
				array(
					'id' => 16,
					'eventUpdate' => array(
						'goalkeeper',
						'substitute-on',
						'substitute-off',
						'yellow-card',
						'red-card',
						'goal-scorer'
					)
				)
			)
		)
	)
);

// Simulate a pusher event to the Attack feed component
$attack = array(
	'attack' => array(
		'content' => array(
			array(
				'heading' => 'Attack statistic 1',
				'percentages' => true,
				'values' => array(
					'home' => 20,
					'away' => 80
				)
			),
			array(
				'heading' => 'Attack statistic 2',
				'percentages' => true,
				'values' => array(
					'home' => 20,
					'away' => 80
				)
			),
			array(
				'heading' => 'Attack statistic 3',
				'percentages' => true,
				'values' => array(
					'home' => 20,
					'away' => 80
				)
			),
			array(
				'heading' => 'Attack statistic 4',
				'percentages' => true,
				'values' => array(
					'home' => 20,
					'away' => 80
				)
			),
			array(
				'heading' => 'Attack statistic 5',
				'percentages' => true,
				'values' => array(
					'home' => 20,
					'away' => 80
				)
			),
			array(
				'heading' => 'Attack statistic 6',
				'percentages' => true,
				'values' => array(
					'home' => 20,
					'away' => 80
				)
			),
			array(
				'heading' => 'Attack statistic 7',
				'percentages' => true,
				'values' => array(
					'home' => 20,
					'away' => 80
				)
			)
		)
	)
);

// Simulate a pusher event to the General Play feed component
$generalPlay = array(
	'generalPlay' => array(
		'content' => array(
			array(
				'heading' => 'General Play statistic 1',
				'percentages' => true,
				'values' => array(
					'home' => 20,
					'away' => 80
				)
			),
			array(
				'heading' => 'General Play statistic 2',
				'percentages' => true,
				'values' => array(
					'home' => 20,
					'away' => 80
				)
			),
			array(
				'heading' => 'General Play statistic 3',
				'percentages' => true,
				'values' => array(
					'home' => 20,
					'away' => 80
				)
			)
		)
	)
);

// Simulate a pusher event to the Distribution feed component
$distribution = array(
	'distribution' => array(
		'content' => array(
			array(
				'heading' => 'Distribution statistic 1',
				'percentages' => true,
				'values' => array(
					'home' => 20,
					'away' => 80
				)
			),
			array(
				'heading' => 'Distribution statistic 2',
				'percentages' => true,
				'values' => array(
					'home' => 20,
					'away' => 80
				)
			),
			array(
				'heading' => 'Distribution statistic 3',
				'percentages' => true,
				'values' => array(
					'home' => 20,
					'away' => 80
				)
			)
		)
	)
);

// Simulate a pusher event to the Defence & Discipline feed component
$defenceAndDiscipline = array(
	'defenceAndDiscipline' => array(
		'content' => array(
			array(
				'heading' => 'Defence & Discipline statistic 1',
				'percentages' => true,
				'values' => array(
					'home' => 20,
					'away' => 80
				)
			),
			array(
				'heading' => 'Defence & Discipline statistic 2',
				'percentages' => true,
				'values' => array(
					'home' => 20,
					'away' => 80
				)
			),
			array(
				'heading' => 'Defence & Discipline statistic 3',
				'percentages' => true,
				'values' => array(
					'home' => 20,
					'away' => 80
				)
			)
		)
	)
);
		
$pusher->trigger('matchCentre.fixtures','broadcast',json_decode(json_encode($fixtures)));
$pusher->trigger('matchCentre.leagueTable','broadcast',json_decode(json_encode($leagueTable)));
$pusher->trigger('matchCentre.695005.inSummary','broadcast',json_decode(json_encode($inSummary)));
$pusher->trigger('matchCentre.695005.matchSummary','broadcast',json_decode(json_encode($matchSummary)));
$pusher->trigger('matchCentre.newsFeed','broadcast',json_decode(json_encode($newsFeed)));
$pusher->trigger('matchCentre.695005.newsFeed','broadcast',json_decode(json_encode($newsFeed)));
$pusher->trigger('matchCentre.695005.lineUps','broadcast',json_decode(json_encode($lineUps)));
$pusher->trigger('matchCentre.695005.attack','broadcast',json_decode(json_encode($attack)));
$pusher->trigger('matchCentre.695005.generalPlay','broadcast',json_decode(json_encode($generalPlay)));
$pusher->trigger('matchCentre.695005.distribution','broadcast',json_decode(json_encode($distribution)));
$pusher->trigger('matchCentre.695005.defenceAndDiscipline','broadcast',json_decode(json_encode($defenceAndDiscipline)));
?>