(function($){
	$.fn.blink = function(options){
		var settings = {			
			blinkPeriod: 1000,
			maxBlinks: undefined,
			onBlink: function(i){},
			onMaxBlinks: function(){},			
			speed: undefined
		};

		if(options){
			$.extend(settings,options);
		}

		var blinkElem = this;
		var on = true;
		var blinkCount = 0;
	
		settings.speed = settings.speed ? settings.speed : settings.blinkPeriod/2;

		/* Perform the fading */
		(function toggleFade(){
			var maxBlinksReached = false;
			
			if(on){
				blinkElem.fadeTo(settings.speed, 0.01);
			}
			else{
				blinkCount++;
				maxBlinksReached = (settings.maxBlinks && (blinkCount >= settings.maxBlinks));
				blinkElem.fadeTo(settings.speed, 1, function(){
					settings.onBlink.call(blinkElem, blinkCount);
					
					if(maxBlinksReached){
						settings.onMaxBlinks.call();
					}
				});
			}

			on = !on;

			if(!maxBlinksReached){
				setTimeout(toggleFade,settings.blinkPeriod/2);
			}
		})();

		return this;
	};
})(jQuery);