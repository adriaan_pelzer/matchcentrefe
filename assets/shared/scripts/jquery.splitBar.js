(function(jQuery){		
	var splitBarConfiguration = {
		easing: 'easeInOutExpo',
		easingRefresh: 'linear',
		timings: {
			animateIntoView: 1500,
			websocketReanimateBar: 250
		}
	}
	
	var methods = {	
		/**
		* Build a split bar graph in response to JSON content
		* @method build
		* @param {BOOLEAN} createKey Whether to generate the key or not
		* @param {OBJECT} data The JSON data for the bar graphs
		* @param {BOOLEAN} initialLoad What type of animation speed to use when animating the bars
		*/		
		build: function(obj){
			$.logEvent('[$.fn.splitBar (build)]: ' + $.logJSONObj(obj));
			
			var selfObj = $(this);
						
			// Build the key
			selfObj
				.append(
					$('<div />')
						.append(
							$('<ul />')
								.append(
									$('<li />')
										.append(
											$('<strong />')
												.text(obj.data.key.homeTeam)
										)
										.attr('class','home-team')
								)
								.append(
									$('<li />')
										.append(
											$('<strong />')
												.text(obj.data.key.awayTeam)
										)
										.attr('class','away-team')
								)
						)
						.attr('class','split-bar-key')
				)			
			
			var splitBarObj;
						
			// Iterate through the JSON, and attach to the contaier DOM element
			$.each(obj.data.content,function(){
				// Work out the correct values/format (% or not) for home and away values
				var barValuesObj = homeAwayValues({
					homeValue: this.values.home,
					awayValue: this.values.away,
					percentages: this.percentages
				});
				
				splitBarObj = $('<div />')
					.append(
						$('<h4 />')
							.text(this.heading)
					)
					.append(
						$('<div />')
							.append(
								$('<strong />')
									.text(this.percentages ? barValuesObj.homeValue + '%' : this.values.home)
							)
							.append(
								$('<ul />')
									.append(
										$('<li />')
											.attr('data-value',barValuesObj.matchedValues ? 50 : barValuesObj.homeValue)
											.html(this.percentages ? barValuesObj.homeValue + '%' : this.values.home)
									)
									.append(
										$('<li />')
											.attr('data-value',barValuesObj.matchedValues ? 50 : barValuesObj.awayValue)
											.html(this.percentages ? barValuesObj.awayValue + '%' : this.values.away)
									)
							)
							.append(
								$('<strong />')
									.text(this.percentages ? barValuesObj.awayValue + '%' : this.values.away)
							)
							.attr('class','outer')
					)
					.attr({
						'class': 'split-bar',
						'data-percentage': this.percentages ? true : false,
						'data-heading': this.heading
					})

				// Add each bar to the container DOM object
				splitBarObj.appendTo(selfObj);
			});
			
			// With all split bars injected, animate them to their final size
			$('.split-bar',selfObj).splitBar('animateIntoView',{initialLoad: obj.initialLoad});
		},
		
		/** 
		* With all split bars injected, animate them to the final size
		* @method animateIntoView
		* @param {BOOLEAN} initialLoad Whether the animation is the first (longer duration) or a refreshed set of data (shorter duration)
		*/
		animateIntoView: function(obj){
			$.logEvent('[$.fn.splitBar (animateIntoView)]: ' + $.logJSONObj(obj));
			
			var homeTeamObj;
			var awayTeamObj;
			var splitBarObj;
			var selfObj = $(this);
			
			return this.each(function(){
				splitBarObj = $(this);
				homeTeamObj = $('LI:first',splitBarObj);
				awayTeamObj = $('LI:last',splitBarObj);
				
				// Ensure that the graph is set to 'loaded'
				if(!selfObj.hasClass('loaded')){
					setTimeout(function(){
						selfObj.addClass('loaded');
					},splitBarConfiguration.timings.animateIntoView);
				}
				
				homeTeamObj.animate({
					width: parseInt(homeTeamObj.attr('data-value')) + '%'
				},{
					duration: obj.initialLoad ? splitBarConfiguration.timings.animateIntoView : splitBarConfiguration.timings.websocketReanimateBar,
					easing: obj.initialLoad ? splitBarConfiguration.easing : splitBarConfiguration.easingRefresh
				});
				
				awayTeamObj.animate({
					width: parseInt(awayTeamObj.attr('data-value')) + '%'
				},{
					duration: obj.initialLoad ? splitBarConfiguration.timings.animateIntoView : splitBarConfiguration.timings.websocketReanimateBar,
					easing: obj.initialLoad ? splitBarConfiguration.easing : splitBarConfiguration.easingRefresh
				});	
			});
		},
	
		/**
		* Refresh a split bar graph in response to JSON content
		* @method refresh
		* @param {OBJECT} data The updated JSON data for the bar graph
		*/
		refresh: function(obj){
			$.logEvent('[$.fn.splitBar (build)]: ' + $.logJSONObj(obj));
			
			var splitBarGraphObj;
			var splitBarObj;
			
			return this.each(function(){
				splitBarGraphObj = $(this);
				
				// Iterate through the In Summary JSON, and update the corresponding split bar with the new data
				$.each(obj.data.content,function(index,dataObj){
					splitBarObj = $('.split-bar[data-heading="' + this.heading + '"]',splitBarGraphObj);

					// Work out the correct values/format (% or not) for home and away values
					var barValuesObj = homeAwayValues({
						homeValue: this.values.home,
						awayValue: this.values.away,
						percentages: this.percentages
					});
					
					// Update the data attributes and associated visual labels, before re-animating to those values
					splitBarObj
						.find('STRONG:first').html(this.percentages ? barValuesObj.homeValue + '%' : this.values.home)
						.end()
						.find('STRONG:last').html(this.percentages ? barValuesObj.awayValue + '%' : this.values.away)
						.end()
						.find('LI:first').attr('data-value',barValuesObj.matchedValues ? 50 : barValuesObj.homeValue).html(this.percentages ? barValuesObj.homeValue + '%' : this.values.home)
						.end()
						.find('LI:last').attr('data-value',barValuesObj.matchedValues ? 50 : barValuesObj.awayValue).html(this.percentages ? barValuesObj.awayValue + '%' : this.values.away)
				});
				
				// With all split bars injected, animate them to the final size
				$('.split-bar',splitBarGraphObj).splitBar('animateIntoView',{initialLoad: false});
			});
		}
	};
	
	// Initialize plugin
	$.fn.splitBar = function(obj){
		// Method calling logic
		if(methods[obj]){
			return methods[obj].apply(this,Array.prototype.slice.call(arguments,1));
		} 
		else if(typeof obj === 'object' || ! obj){
			return methods.init.apply(this,arguments);
		}
	};
	
	/**
	* Work out the correct values/format (% or not) for home and away values
	* @method homeAwayValues
	* @param {INTEGER} homeValue The home team value
	* @param {INTEGER} awayValue The away team value
	* @param {BOOLEAN} percentages Whether the split bar instance requires percentage values to be shown
	* @return {OBJECT}
	*/
	function homeAwayValues(obj){
		var homeValue = obj.homeValue;
		var awayValue = obj.awayValue;
		var totalValue = homeValue + awayValue;	
		var matchedValues = false;
		
		// Work out whether percentages should be used, or original values
		if(!obj.percentages){
			homeValue = Math.round((homeValue/totalValue)*100);
			awayValue = 100-homeValue;
		}
				
		// Handle identical values
		if(obj.homeValue == obj.awayValue){
			matchedValues = true;
		}
		
		var homeAwayObj = {
			homeValue: homeValue,
			awayValue: awayValue,
			matchedValues: matchedValues
		}
		
		return homeAwayObj;
	}
}(jQuery));