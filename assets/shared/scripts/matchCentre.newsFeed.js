/**
* MatchCentre v1.0
* @module matchCentre
*/

var matchCentre = window.matchCentre || {};

/**
* MatchCentre root class
* @class matchCentre
* @namespace matchCentre.newsFeed
*/

matchCentre.newsFeed = (function(){
	var configuration = {
		easingEquation: 'easeInOutExpo', 
		embeddedChartObjs: [],
		embeddedTweets: {
			iframesInitialized: 0,
			intervalObj: null,
			totalTweets: 0
		},
		newsFeedDomObj: null,
		promotedUpdates: ['goal','red-card','statistic','yellow-card'], // The update types which require internal padding eg: Yellow Card
		timings: {
			updateToggleVisibility: 350
		},
		updatesTotalSinceLastAddition: 0
	};
	
	/**
	* Build the league table in response to the initial pulled JSON content
	* @method build
	*/
	function build(){
		$.logEvent('[matchCentre.newsFeed.build]');

		// Create the necessary wrapper DOM elements
		var newsFeedContainerDomObj = $('<div />')
			.append(
				$('<div />')
					.attr('class','inner')
			)
			.attr('id','news-feed')
			
		// Attach the container DOM element for the News Feed to the DOM
		newsFeedContainerDomObj.appendTo($('#content'))

		// Specify globally a pointer to the newly injected League Table DOM element
		configuration.newsFeedDomObj = newsFeedContainerDomObj;
		
		// Capture any errors should the content for the fixtures carousel not be correct
		try{
			if(matchCentre.configuration.initialSiteContent.newsFeed.metaData.hasOwnProperty('countdownTimer')){
				matchCentre.configuration.countdownTimer = matchCentre.configuration.initialSiteContent.newsFeed.metaData.countdownTimer;
								
				matchCentre.configuration.enabled = true;
								
				// Initialize the countdown timer
				matchCentre.core.countdownTimerInit();
			}
		
			// Iterate through the News Feed JSON, and attach to the News Feed DOM element
			$.each(matchCentre.configuration.initialSiteContent.newsFeed.content,function(index,data){		
				// Add the new feed item to the DOM
				injectNewsFeedUpdate({
					data: this,
					existingItem: false,
					fromWebsocket: false,
					newsFeedContainerDomObj: $('>.inner',configuration.newsFeedDomObj)
				});
			});
			
			// Resize all amCharts objects
			matchCentre.newsFeed.amChartsRefresh();
			
			// Set up delegation for the 'New Updates' button and the individual feed toggles
			eventHandlersInit();
			
			// Run a check to monitor embedded Tweet objects, since their (delayed) injection plays with jump tags (from deep-linking Tweets)
			embeddedTweetsDeepLinkingDelay();
			
			if(!matchCentre.configuration.siteInitialized){
				// Notify dispatcher that a component has finished initializing
				matchCentre.dispatcher.initializeComplete({
					finishedId: 'newsFeed'
				});
			}		
		}
		catch(err){			
			$.logEvent('[matchCentre.newsFeed.build]: an error occured populating the News Feed component: ' + $.logJSONObj(err));
			
			if(!matchCentre.configuration.siteInitialized){
				// Notify dispatcher that a component has finished initializing
				matchCentre.dispatcher.initializeComplete({
					finishedId: 'newsFeed'
				});
			}
		}
	}
	
	/**
	* Set up delegation for the 'New Updates' button, the individual feed toggles and the 'Tweet this post' links
	* @method eventHandlersInit
	*/
	function eventHandlersInit(){
		$.logEvent('[matchCentre.newsFeed.eventHandlersInit]');
		
		// Event delegation for the 'New Updates' button
		configuration.newsFeedDomObj
			.delegate('#new-updates','click',function(e){
				e.preventDefault();
				
				// Auto toggle all 'old' news items into a contracted state (specifying that the toggle speed is overwritten to be an immediate snap shut)
				$('.update').filter(function(){
					return !$(this).hasClass('new-and-hidden');
				}).each(function(){
					var toggleDomObj = $('.toggle',this);
					
					// For all currently expanded updates in the feed that are NOT new, toggle them closed
					// Only perform this feature if the news feed is sorted by 'Newest first'
					if(matchCentre.dropdownBar.configuration[matchCentre.configuration.pageType].toggleSwitches.settings['show-newest-first']){
						var isExpanded = toggleDomObj.hasClass('expanded');
						if(isExpanded){
							toggleDomObj.trigger('click',{toggleSpeed: 1});
						}
					}
				});
				
				// In order to show all new updates, remove the 'new-and-hidden' classnames
				$('.update.new-and-hidden',configuration.newsFeedDomObj).removeClass('new-and-hidden');
				
				// Reset the counter used to store how many updates have occured since the 'New Updates' button was last clicked
				configuration.updatesTotalSinceLastAddition = 0;
				
				$('#new-updates',configuration.newsFeedDomObj).remove();	

				if(matchCentre.configuration.advertsEnabled){
					// Refresh the MPU advertising banner
					matchCentre.core.refreshAdvertisingBanner();
				}
				
				if(matchCentre.omniture.configuration.trackingEnabled){
					// Send tracking request to Omniture
					matchCentre.omniture.track({
						additionalProperties: {
							eVar75: 'New updates received in news feed',
							prop71: 'New updates received in news feed'
						},
						trackingType: 'page-view'
					});
				}
			})
			// Event delegation for the individual feed toggles
			.delegate('.toggle','click',function(e,dataObj){
				e.preventDefault();
				
				var linkObj = $(this);
				
				// Don't process the event if an animation is currently taking place
				if(linkObj.hasClass('animating')){
					return false;
				}
				
				var updateDomObj = linkObj.parent();
				var isExpanded = linkObj.next().is(':visible');
				var toggleSpeed = dataObj != undefined ? dataObj.toggleSpeed : configuration.timings.updateToggleVisibility;

				// Handle the fading in/out of the source (left-hand side) elements (all devices except for mobile), match update type/author photo/author name/author publication
				if(matchCentre.configuration.deviceType != 'mobile'){
					$('.source IMG, .source P',updateDomObj)
						.animate({
							opacity: isExpanded ? 0 : 1
						},{
							complete: function(){},
							duration: toggleSpeed
						})		
				}

				// Toggle the inline images hidden before performing a snap toggle, to handle the horizontal snapping which naturally occurs due to negative margins
				if(matchCentre.core.isMobile() && isExpanded){
					linkObj
						.next()
							.find('FIGURE,.inline-image')
								.IF(isExpanded)
									.hide()				
				}
				
				// Perform the toggle animation
				linkObj
					.addClass('animating')
					.next()
						.slideToggle(toggleSpeed,configuration.easingEquation,function(){
							linkObj
								.removeClass('animating contracted expanded')
								.IF(isExpanded)
									.addClass('contracted')
								.ELSE()
									.addClass('expanded')
									.IF(matchCentre.core.isMobile())
										.next()
											.find('FIGURE,.inline-image')
												.show()
									.ENDIF()
								.ENDIF()
								
							// Resize all amCharts objects
							matchCentre.newsFeed.amChartsRefresh();
						});						
			})
			// Event delagation for the 'Tweet this post' links
			.delegate('.tweet-this-post','click',function(e){
				e.preventDefault();
				
				var linkObj = $(this);
				
				window.open('https://twitter.com/intent/tweet?url=' + matchCentre.configuration.baseURL + window.location.pathname + $(location).attr('search') + '%23' + linkObj.parents('.update').attr('data-update-id') + '&text=' + matchCentre.configuration.pageTitle);
			})
	}
	
	/**
	* Add the new feed item to the DOM
	* @method injectNewsFeedUpdate
	* @param {OBJECT} data The JSON data for the single news feed update
	* @param {BOOLEAN} existingItem Whether or not the update is updating an existing item or adding a new item
	* @param {BOOLEAN} fromWebsocket Whether or not the injected item in is response to a websocket update or not
	* @param {OBJECT} newsFeedContainerDomObj The news feed wrapper DOM element
	*/
	function injectNewsFeedUpdate(obj){
		$.logEvent('[matchCentre.newsFeed.injectNewsFeedUpdate]: ' + $.logJSONObj(obj));

		var updateDomObj;
		var updateDataObj;
		var isPromotedType = false;
		updateDataObj = obj.data;
		
		// Check to see whether this update is a promoted type, which requires a slightly different visual treatment
		if($.inArray(updateDataObj.source.type,configuration.promotedUpdates) != -1){
			isPromotedType = true;
		}
		
		if(obj.existingItem){
			// If the update already exists, it needs to be edited
			updateDomObj = $('.update[data-update-id="' + updateDataObj.id + '"]');
			updateDomObj.removeAttr('class data-update-type data-update-id');
			updateDomObj.empty();
		}
		else{
			// If the item doesn't already exist, create it			
			updateDomObj = $('<div />');
		}
		
		var classNameToUse = 'update ' + updateDataObj.source.type;
		classNameToUse += isPromotedType ? ' promoted' : '';
		classNameToUse += obj.fromWebsocket && !obj.existingItem ? ' new-and-hidden' : '';
		classNameToUse += (matchCentre.dropdownBar.configuration[matchCentre.configuration.pageType].toggleSwitches.filterPosts[updateDataObj.source.type] == false) ? ' hidden' : ''; // Ensure that configured to be hidden elements (via toggle switches) are hidden by default
		
		updateDomObj.attr({
				'class': classNameToUse + ' ' + updateDataObj.source.dataOrigin,
				'data-update-id': updateDataObj.id,
				'data-update-type': updateDataObj.source.type
			});
			
		updateDomObj
			// Time/type
			.append(
				$('<p />')
					.append(
						$('<span />')
							.html(updateDataObj.time)
					)
					.append(
						$('<em />')
							.text('Arrow')
					)
					.append(
						$('<strong />')
							.html(updateDataObj.source.type.replace(/-/g,' '))
					)
					.attr('class','time-type')
			)
			// Match lineup
			.append(
				$('<ul />')
					.append(
						$('<li />')
							.append(
								$('<span />')
									.html('Match between: ')
							)
							.append(
								$('<ol />')
									.append(
										$('<li />')
											.html(updateDataObj.matchTeams.home + '&nbsp;vs&nbsp;')
											.prepend(
												$('<span />')
													.html('Home team: ')
											)
									)
									.append(
										$('<li />')
											.html(updateDataObj.matchTeams.away)
											.prepend(
												$('<span />')
													.html('Home team: ')
											)
									)
									.append(
										$('<li />')
											.html('&nbsp;-&nbsp;' + updateDataObj.minute + ' mins')
									)
							)
							.attr('class','match-lineup')
					)
			)
			// Update heading
			.append(
				$('<h2 />')
					.html(updateDataObj.heading.text)
			)
			
			if(updateDataObj.source.dataOrigin == 'escenic'){
				updateDomObj
					// Author (photo + publication + person name)
					.append(
						$('<div />')
							.IF(updateDataObj.source.dataOrigin === 'escenic')
								.append(
									$('<img />')
										.attr({
											'class': 'source-icon',
											src: updateDataObj.source.iconSource
										})
								)
								.append(
									$('<p />')
										.html(updateDataObj.source.author.name)
										.prepend(
											$('<span />')
												.attr('class','by')
												.html('BY: ')
										)
										.append(
											$('<span />')
												.html(updateDataObj.source.author.company)
										)
								)
							.ENDIF()
							.attr('class','source')
						// Expand/contract icon
						.append(
							$('<a />')
								.attr({
									'class': 'toggle expanded',
									href: '#'
								})
								.html('Click here to toggle this news items visibility')
						)
					)
			}
			
		// Build a container to hold all of the content types for this unique update
		var innerDomObj = $('<div />')
			.attr('class','inner');
		
		updateDomObj.append(innerDomObj);
		
		// With the mandatory DOM injection now completed, turn to the dynamic internal content-types, of which there can be an unlimited amount, and of differing types
		var activeContentObj;
		
		if(updateDataObj.contentTypes != undefined){
			$.each(updateDataObj.contentTypes,function(index,value){
				activeContentObj = this;
				
				switch(activeContentObj.type){
					case 'paragraph':
						// Add paragraph to the DOM
						innerDomObj
							.append(
								$('<p />')
									.html(activeContentObj.text)
							)
							
						break;
						case 'image':						
							// Work out the actual height and width of the images to be injected, based on the aspect ratio of the original
							var imageDimensionsObj = calculateImageDimensions({
								originalHeight: activeContentObj.height,
								originalWidth: activeContentObj.width
							});
							
							// Add image to the DOM
							innerDomObj
								.append(
									$('<div />')
										.append(
											$('<img />')
												.attr({
													height: imageDimensionsObj.currentHeight,
													src: activeContentObj.src
												})
										)
										.IF(activeContentObj.hasOwnProperty('watermark'))
											.append(
												$('<p />')
													.html('&copy; ' + activeContentObj.watermark)
											)
										.ENDIF()
										.attr('class','inline-image' + ' ' + imageDimensionsObj.orientation)
								)
								
							break;
						case 'image-with-caption':
							// Work out the actual height and width of the images to be injected, based on the aspect ratio of the original
							var imageDimensionsObj = calculateImageDimensions({
								originalHeight: activeContentObj.height,
								originalWidth: activeContentObj.width
							});
							
							// Add inline image to the DOM
							innerDomObj
								.append(
									$('<figure />')
										.append(
											$('<div />')
												.append(
													$('<img />')
														.attr({
															height: imageDimensionsObj.currentHeight,
															src: activeContentObj.src
														})
												)
												.IF(activeContentObj.hasOwnProperty('watermark'))
													.append(
														$('<p />')
															.html('&copy; ' + activeContentObj.watermark)
													)
												.ENDIF()
												.attr('class','inline-image')
										)
										.append(
											$('<figcaption />')
												.html(activeContentObj.caption)
										)
								)
							
							break;
					case 'pieChart':						
						// Provide the colours for the SVG segments
						$.extend(activeContentObj.data[0],{color: matchCentre.configuration.colors.green, labelColor: matchCentre.configuration.colors.white});
						$.extend(activeContentObj.data[1],{color: matchCentre.configuration.colors.white, labelColor: matchCentre.configuration.colors.black});
						
						// Build the pie chart
						innerDomObj.pieChart('build',{
							data: activeContentObj.data,
							domElementId: updateDataObj.id
						});
						
						break;
					case 'barChart':
						// Provide the colours for the SVG segments
						$.extend(activeContentObj.data[0],{color: matchCentre.configuration.colors.green});
						$.extend(activeContentObj.data[1],{color: matchCentre.configuration.colors.white});
						
						// Build the bar chart
						innerDomObj.barChart('build',{
							data: activeContentObj.data,
							domElementId: updateDataObj.id,
							metaData: {
								xAxisTitle: activeContentObj.xAxisTitle
							}
						});
						
						break;
					case 'tweet': 
						// Increment a reference to however many embedded Tweets there are within the page
						configuration.embeddedTweets.totalTweets++;
					
						// Add embedded tweet to the DOM
						innerDomObj
							.append(
								$('<blockquote />')
									.html('&mdash; ' + activeContentObj.username + '(@' + activeContentObj['screen-name'] + ')')
									.prepend(
										$('<p />')
											.html(activeContentObj.text)
									)
									.append(
										$('<a />')
											.attr('href','https://twitter.com/' + activeContentObj['screen-name'] + '/statuses/' + activeContentObj['tweet-id'])
											.html(activeContentObj.date)
									)
									.attr({
										'class': 'twitter-tweet',
										'data-conversation': 'none',
										'data-link-color': matchCentre.configuration.colors.green, 
										lang: 'en'
									})								
							)
						
							// Add the required Twitter widget JavaScript file to the DOM, as specified within their embed code
							var scriptObj = document.createElement('script');
							scriptObj.type = 'text/javascript';
							scriptObj.src = 'http://platform.twitter.com/widgets.js';
							$('HEAD').append(scriptObj);
							
						break;				
				}
			});
		}
		
		// Add the required Twitter link to each new update
		innerDomObj
			.append(
				$('<p />')
					.append(
						$('<a />')
							.attr('href','#')
							.html('Tweet this post')
							.on('click',function(e){
								var updateTitle = $(this).parents('.update').find('H2').text();
								
								// Send tracking request to Omniture
								matchCentre.omniture.track({
									additionalProperties: {
										dataAction: 'tweet:' + updateTitle,
										dataType: 'txt-tw',
										dataContext: 'mc-news-feed'
									}, 
									callee: this, 
									friendlyTitle: 'Tweet this post: ' + updateTitle,
									trackingType: 'event-click'
								});
							})							
					)
					.attr('class','tweet-this-post')
			)
			
		// Resize all amCharts objects
		amChartsRefresh();
			
		if(!obj.existingItem){
			// Should the new item be added to the top (newest first) or the bottom (oldest first) of the news feed
			obj.newsFeedContainerDomObj
				.IF(matchCentre.dropdownBar.configuration[matchCentre.configuration.pageType].toggleSwitches.settings['show-newest-first'])
					.prepend(updateDomObj)
				.ELSE()
					.append(updateDomObj)
				.ENDIF()
		}
	}
	
	/**
	* Process the Pusher real-time update
	* @method websocketCallback
	* @param {OBJECT} data A JSON update from Pusher
	*/	
	function websocketCallback(obj){
		$.logEvent('[matchCentre.newsFeed.websocketCallback]: ' + $.logJSONObj(obj));
	
		// Capture any errors should the content for the News Feed not be correct
		try{
			var dataObj;
			var existingItem = false;
			
			$.each(obj.data.newsFeed.content,function(index,dataObj){
				dataObj = this;
	
				var checkExistingObj = $('.update[data-update-id="' + dataObj.id + '"]');
				if(checkExistingObj.size() == 1){
					// If the news item already exists
					existingItem = true;
				}
				else{
					// If the news item is a new one
					// Update the counter of updates in the 'New Updates' button (only for news update types which are configured as visible)
					if(matchCentre.dropdownBar.configuration[matchCentre.configuration.pageType].toggleSwitches.filterPosts[dataObj.source.type] != false){
						configuration.updatesTotalSinceLastAddition = configuration.updatesTotalSinceLastAddition+1;

						// Add the button which includes the label 'New Updates - xx' if it's not already visible
						if(configuration.updatesTotalSinceLastAddition == 1){
							var updatesButtonObj = $('<p />')
								.attr({
									'class': 'button',
									id: 'new-updates'
								})
								.text('New Updates - ')
								.append(
									$('<span />')							
								)
								
							updatesButtonObj
								.prependTo(configuration.newsFeedDomObj)
						}
						
						// Update the counter of updates in the 'New Updates' button
						$('#new-updates SPAN').text(configuration.updatesTotalSinceLastAddition);
					}
				}
						
				// Add the new feed item to the DOM
				injectNewsFeedUpdate({
					data: dataObj,
					existingItem: existingItem,
					fromWebsocket: true,
					newsFeedContainerDomObj: $('>.inner',configuration.newsFeedDomObj)
				});
			});
		}
		catch(err){
			$.logEvent('[matchCentre.newsFeed.websocketCallback]: an error occured populating the News Feed component (via Pusher): ' + $.logJSONObj(err));
		}
	}
	
	/**
	* Re-instate the opacity of the .source DOM element(s) to be visible, regardless of toggle state, remove inline image height attributes
	* @method refresh
	*/
	function refresh(){
		$.logEvent('[matchCentre.newsFeed.refresh]');

		// Remove height attributes from all inline images within the news feed, since these are only needed for the initial page build (to handle deeplinking
		$('.inline-image IMG',configuration.newsFeedDomObj)
			.removeAttr('height');
		
		if(matchCentre.core.isMobile()){
			$('.source *',configuration.newsFeedDomObj)
				.css('opacity',1);
		}
	}
		
	/**
	* Resize all amCharts objects
	* @method amChartsRefresh
	*/
	function amChartsRefresh(){
		$.logEvent('[matchCentre.newsFeed.amchartsRefresh]');
		
		// Scale the size of the amCharts objects, to stretch to the fluid/static (desktop only) grid, regardless of device type
		$.each(matchCentre.newsFeed.configuration.embeddedChartObjs,function(index,chartObj){
			chartObj.invalidateSize();
			chartObj.validateData();
			chartObj.validateNow();
		});	
	}

	/**
	* Run a check to monitor embedded Tweet objects, since their (delayed) injection plays with jump tags (from deep-linking Tweets)
	* @method embeddedTweetsDeepLinkingDelay
	*/
	function embeddedTweetsDeepLinkingDelay(){
		$.logEvent('[matchCentre.newsFeed.embeddedTweetsDeepLinkingDelay]');
		
		// If there is no hash value in the URL, do not process this function, as a deep-link has not been requested
		if($.param.fragment()[0] == undefined){
			return;
		}
		
		var iframesInjected = 0;
		var intervalOffset = 100;
		
		if(configuration.embeddedTweets.totalTweets > 0){
			configuration.embeddedTweets.intervalObj = setInterval(function(){
				iframesInjected = $('#news-feed IFRAME').size();
				
				if(iframesInjected == configuration.embeddedTweets.totalTweets){
					configuration.embeddedTweets.iframesInitialized = 0;
					
					// Check to see if an Iframe has been fully initialized (via the presence of the 'visibility:visible' inline CSS property
					$('#news-feed IFRAME').each(function(index){
						if($(this).css('visibility') == 'visible'){
							configuration.embeddedTweets.iframesInitialized++;
						}
					});
					
					// All embedded Tweets now successfully embedded (and have reached their final Iframe heights
					if(configuration.embeddedTweets.iframesInitialized == configuration.embeddedTweets.totalTweets){						
						clearInterval(configuration.embeddedTweets.intervalObj);
						
						// Read the URL hashvalue, to scroll the page straight to the tweeted post
						matchCentre.core.processURLHash();
					}
				}
			},intervalOffset);
		}
	}
	
	/**
	* Work out the actual height and width of the images to be injected, based on the aspect ratio of the original
	* @method calculateImageDimensions
	* @param {INTEGER} originalHeight The height of the original image
	* @param {INTEGER} originalWidth The width of the original image
	* @return {INTEGER} currentHeight The height of the DOM injected image
	* @return {INTEGER} currentWidth The width of the DOM injected image
	* @return {STRING} orientation The orientation of the DOM injected image
	*/
	function calculateImageDimensions(obj){
		$.logEvent('[matchCentre.newsFeed.calculateImageDimensions]: ' + $.logJSONObj(obj));
	
		var imageWidth;
		var imageHeight;
		var orientation;
		var updateWidth = 0;
		
		// Identify what orientation type the image is
		if(obj.originalHeight > obj.originalWidth){
			orientation = 'portrait';
		}
		else{
			if(obj.originalWidth > obj.originalHeight){
				orientation = 'landscape';
			}
		}
		
		var aspectRatio = obj.originalHeight/obj.originalWidth;
		
		switch(matchCentre.configuration.deviceType){
			case 'desktop':
				imageWidth = 615;
				
				// Take into account portrait inline images being 70% width
				if(orientation == 'portrait'){
					imageWidth = Math.floor(imageWidth * 0.7);
				}
				
				break;
			case 'tablet':	
				// Subtract away the left and right gutters (20px each side)
				updateWidth = $(window).width()-40;
				
				// Take into account the 25% left padding
				updateWidth = Math.floor(updateWidth * 0.75);
				imageWidth = updateWidth;
				
				// Take into account portrait inline images being 70% width
				if(orientation == 'portrait'){
					imageWidth = Math.floor(updateWidth * 0.7);
				}
			
				break;
			case 'mobile': 
				// Subtract away the left and right gutters (15px each sode)
				updateWidth = $(window).width()-30;
				
				// Take into account the -6% left margin and 6% right margin to balance things out, so 12% in total
				var negativeIndentInPixels = Math.floor(updateWidth * 0.12);
				
				imageWidth = Math.floor(updateWidth + negativeIndentInPixels);
			
				break;
		}
		
		// Calculate the image height, since both the width and the aspect ratio are now known
		imageHeight = Math.floor(imageWidth * aspectRatio);
	
		var currentImageObj = {
			currentHeight: imageHeight,
			currentWidth: imageWidth,
			orientation: orientation
		}
	
		return currentImageObj;
	}
	
	return {
		amChartsRefresh: amChartsRefresh,
		build: build,
		configuration: configuration,
		refresh: refresh,
		websocketCallback: websocketCallback
	}
}());