/**
* MatchCentre v1.0
* @module matchCentre
*/

var matchCentre = window.matchCentre || {};

/**
* MatchCentre root class
* @class matchCentre
* @namespace matchCentre.websockets
*/
matchCentre.websockets = (function(){
	var configuration = {
		enabled: false,
		monitorEnabled: false,
		pusherObj: null,
		runMultipleInSimulation: false,
		timings: {
			websocketTriggerDelay: 5000,
			websocketMultipleTriggerDelay: 5000
		},
		types: {
			attack: 'local', // footerBar > Stats > Attack
			defenceAndDiscipline: 'local', // footerBar > Stats > Defence & Discipline
			distribution: 'local', // footerBar > Stats > Distribution
			fixtures: 'global',
			generalPlay: 'local', // footerBar > Stats > General Play
			inSummary: 'local',
			leagueTable: 'global',
			lineUps: 'local', // footerBar > Line Ups
			matchSummary: 'local',
			newsFeed: 'local'
		}
	};
			
	/**
	* Only a single connection is required to Pusher, which can then have multiple subscriptions attached to it
	* @method connectToPusher
	*/
	function connectToPusher(){
		$.logEvent('[matchCentre.websockets.connectToPusher]');

		var pusherConfiguration = matchCentre.configuration.initialSiteContent.configuration.pusher;		
		var pusherMode = pusherConfiguration.runLive ? 'live' : 'simulated';
		var apiKey = pusherConfiguration[pusherMode].apiKey;

		// Create the connection to Pusher
		configuration.pusherObj = new Pusher(apiKey);
	}
		
	/**
	* Subscribe to the requested websocket channel, which will broadcast all real-time website updates for that unique component
	* @method channelSubscribe
	* @param {STRING} callbackOrigin The namespaced path of where the callback functionality resides for the unique component
	* @param {STRING} componentId The id of the component which needs to have a websocket event listener created
	*/	
	function channelSubscribe(obj){
		$.logEvent('[matchCentre.websockets.channelSubscribe]: ' + $.logJSONObj(obj));
		
		var channelNamePrefix = matchCentre.configuration.initialSiteContent.configuration.pusher.channelName;
		var channelObj;
		var channelFullName;
				
		// Open a websocket connection to the requested channel
		// Handle the scenario where a local websocket needs to be global since it is within the Match Day Live page 
		if(configuration.types[obj.componentId] == 'local' && !matchCentre.configuration.isMatchDetailPage){
			channelFullName = channelNamePrefix + '.' + obj.componentId;
		}
		else{
			channelFullName = channelNamePrefix + (configuration.types[obj.componentId] == 'local' ? '.' + matchCentre.configuration.fixtureId + '.' : '.') + obj.componentId;
		}
			
		channelObj = configuration.pusherObj.subscribe(channelFullName);

		// Convert a string to a object, representing the path of the namespaced callback
		var callbackPathObj = pathToNamespacedCallback({
			path: obj.callbackOrigin
		});
		
		channelObj.bind('broadcast',function(data){
			// Process the Pusher real-time update
			callbackPathObj.websocketCallback({data: data});
		});
		
		// An update has occured to the websocket channel, reflect that in the websockets monitor
		if(configuration.monitorEnabled){
			monitorUpdate({
				channelFullName: channelFullName,
				updateType: 'subscribe'
			});
		}
	}
		
	/**
	* Unsubscribe from a requested websocket channel
	* @method channelUnsubscribe
	* @param {STRING} channelFullName The websocket channel name which needs to be unsubscribed
	*/
	function channelUnsubscribe(obj){
		$.logEvent('[matchCentre.websockets.channelUnsubscribe]: ' + $.logJSONObj(obj));
		
		configuration.pusherObj.unsubscribe(obj.channelFullName);
		
		// An update has occured to the websocket channel, reflect that in the websockets monitor
		if(configuration.monitorEnabled){
			monitorUpdate({
				channelFullName: obj.channelFullName,
				updateType: 'unsubscribe'
			});
		}
	}
	
	/**
	* Convert a string to a object, representing the path of the namespaced callback
	* @param {STRING} path The path of the callback, in string format
	* @return {OBEJCT} The input string converted to a parsable object
	*/
	function pathToNamespacedCallback(obj){
		$.logEvent('[matchCentre.websockets.pathToNamespacedCallback]: ' + $.logJSONObj(obj));
		
		var rootNodeObj = matchCentre;
		var pathObj = obj.path.split('.');
		
		for(var i=0; i<=pathObj.length-1; i++){
			rootNodeObj = rootNodeObj[pathObj[i]];
		}
		
		return rootNodeObj;
	}
		
	/**
	* An update has occured to the websocket channel, reflect that in the websockets monitor
	* @method monitorUpdate
	* @param {STRING} channelFullName The name of the websocket channel which is being updated
	* @param {STRING} updateType Whether the channel is being subscribed to or unsubscribed from
	*/
	function monitorUpdate(obj){
		$.logEvent('[matchCentre.websockets.monitorUpdate]: ' + $.logJSONObj(obj));
	
		// Create the websockets monitor if it hasn't been
		if($('#websockets-monitor').size() == 0){
			$('<div />')
				.append(
					$('<h4 />')
						.text('Websocket connections')
				)
				.append(
					$('<ul />')
				)
				.attr('id','websockets-monitor')
				.insertBefore($('#outer'))
		}
				
		var monitoredChannelObj = $('#websockets-monitor LI[data-component-id="' + obj.channelFullName + '"]');
		if(monitoredChannelObj.size() == 1){
			monitoredChannelObj.attr('class',obj.updateType == 'subscribe' ? 'open' : 'closed');
		}
		else{
			$('#websockets-monitor UL')
				.append(
					$('<li />')
						.attr({
							'class': obj.updateType == 'subscribe' ? 'open' : 'closed',
							'data-component-id': obj.channelFullName
						})
						.text(obj.channelFullName)
				)
		}
	}
	
	return {
		channelSubscribe: channelSubscribe,
		channelUnsubscribe: channelUnsubscribe,
		configuration: configuration,
		connectToPusher: connectToPusher
	}
}());