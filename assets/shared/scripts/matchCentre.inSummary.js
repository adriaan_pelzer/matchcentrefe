/**
* MatchCentre v1.0
* @module matchCentre
*/

var matchCentre = window.matchCentre || {};

/**
* MatchCentre root class
* @class matchCentre
* @namespace matchCentre.inSummary
*/

matchCentre.inSummary = (function(){
	var configuration = {
		// ...
	};	
			
	/**
	* Build In Summary in response to the initial pulled JSON content
	* @method build
	*/
	function build(){
		$.logEvent('[matchCentre.inSummary.build]');
				
		// Create the necessary wrapper DOM elements
		var inSummaryContainerObj = $('<div />')
			.append(
				$('<h3 />')
					.append(
						$('<strong />')
							.html('In Summary')
					)
			)
			.append(
				$('<div />')
					.attr('class','inner')
			)
			.attr({
				'class': 'in-summary module',
				'data-flyout-title': 'In Summary',
				'data-content-area': 'panel',
				'data-flyout-origin': 'in-summary',
				'data-flyout': true
			})
			
		// Attach the container DOM element for in summary to the DOM	
		$('#panel').append(inSummaryContainerObj);
		
		// Capture any errors should the content for In Summary not be correct
		try{
			// Build the split bar graph
			$('.inner',inSummaryContainerObj).splitBar('build',{
				data: matchCentre.configuration.initialSiteContent['inSummary'],
				initialLoad: true
			});
					
			if(!matchCentre.configuration.siteInitialized){
				// Notify dispatcher that a component has finished initializing
				matchCentre.dispatcher.initializeComplete({
					finishedId: 'inSummary'
				});
			}
		}
		catch(err){			
			$.logEvent('[matchCentre.inSummary.build]: an error occured populating the In Summary component: ' + $.logJSONObj(err));
			
			if(!matchCentre.configuration.siteInitialized){
				// Notify dispatcher that a component has finished initializing
				matchCentre.dispatcher.initializeComplete({
					finishedId: 'inSummary'
				});
			}
		}
	}
	
	/**
	* Process the Pusher real-time update
	* @method websocketCallback
	* @param {OBJECT} data A JSON update from Pusher
	*/	
	function websocketCallback(obj){
		$.logEvent('[matchCentre.inSummary.websocketCallback]: ' + $.logJSONObj(obj));

		var inSummaryDomObj;
		
		// Capture any errors should the content for the In Summary not be correct
		try{
			return $('.in-summary .inner').each(function(){
				inSummaryDomObj = $(this);
				
				// Run the refresh against all matched DOM elements
				inSummaryDomObj.splitBar('refresh',{data: obj.data.inSummary});
			});
		}
		catch(err){
			$.logEvent('[matchCentre.inSummary.websocketCallback]: an error occured populating the In Summary component (via Pusher): ' + $.logJSONObj(err));
		}
	}
	
	return {
		build: build,
		configuration: configuration,
		websocketCallback: websocketCallback
	}
}());