/**
* MatchCentre v1.0
* @module matchCentre
*/

var matchCentre = window.matchCentre || {};

/**
* MatchCentre root class
* @class matchCentre
* @namespace matchCentre.matchSummary
*/

matchCentre.matchSummary = (function(){
	var configuration = {
		shareURLPrefix: 'http://api.addthis.com/oexchange/0.8/forward/'
	};	
			
	/**
	* Build In Summary in response to the initial pulled JSON content
	* @method build
	*/
	function build(){
		$.logEvent('[matchCentre.matchSummary.build]');
		
		var matchObj = matchCentre.configuration.initialSiteContent.matchSummary;
		
		// Capture any errors should the content for the Match Summary not be correct
		try{
			// Create the necessary wrapper DOM elements
			var matchSummaryContainerObj = $('<div />')
				.append(
					$('<p />')
						.append(
							$('<a />')
								.attr({
									'class': 'contracted',
									href: '#'
								})
								.text('Click here to show/hide goals scored')
						)
						.attr('id','toggle-goal-scorers')
				)
				.append(
					$('<ol />')
						.append(
							$('<li />')
								.append(
									$('<div />')
										.append(
											$('<span />')
												.attr('class',matchCentre.core.lowerCaseWithDashes(matchObj.content.homeTeam.name))
												.text('Goal scorers for home team:')
										)
										.append(
											$('<strong />')
												.text(matchObj.content.homeTeam.name)
										)
								)
								.attr('class','home')
						)
						.append(
							$('<li />')
								.text(matchObj.content.homeTeam.goalsScored.length + ' - ' + matchObj.content.awayTeam.goalsScored.length)
								.prepend(								
									$('<strong />')
										.text('Current score: ')
								)
								.attr('class','separator')
						)
						.append(
							$('<li />')
								.append(
									$('<div />')
										.append(
											$('<span />')
												.attr('class',matchCentre.core.lowerCaseWithDashes(matchObj.content.awayTeam.name))
												.text('Goal scorers for away team:')
										)
										.append(
											$('<strong />')
												.text(matchObj.content.awayTeam.name)
										)
								)
								.attr('class','away')
						)
				)
				.append(
					$('<table />')
						.append(
							$('<thead />')
								.append(
									$('<tr />')
										.append(
											$('<th />')
												.text(matchObj.content.homeTeam.name)
										)
										.append(
											$('<th />')
												.html('&nbsp;')
										)
										.append(
											$('<th />')
												.text(matchObj.content.awayTeam.name)
										)
								)
						)
						.append(
							attachGoalsScoredToMatch({
								homeTeamGoalScorers: matchObj.content.homeTeam.goalsScored, 
								awayTeamGoalScorers: matchObj.content.awayTeam.goalsScored
							})
						)
						.attr('border',1)
				)
				.append(
					$('<ul />')
						.append(
							$('<li />')
								.attr('data-meta-type','kick-off')
								.text(matchObj.metaData.kickOff)
								.prepend(
									$('<strong />')
										.text('Ko: ')
								)
						)
						.IF(matchObj.metaData.hasOwnProperty('referee'))
							.append(
								$('<li />')
									.attr('data-meta-type','referee')
									.text(matchObj.metaData.referee)
									.prepend(
										$('<strong />')
											.text('Ref: ')
									)
							)
						.ENDIF()
						.IF(matchObj.metaData.hasOwnProperty('attendance'))
							.append(
								$('<li />')
									.attr('data-meta-type','attendance')
									.text(matchObj.metaData.attendance)
									.prepend(
										$('<strong />')
											.text('Attendance: ')
									)
							)
						.ENDIF()
						.IF(matchObj.metaData.hasOwnProperty('location'))
							.append(
								$('<li />')
									.attr('data-meta-type','location')
									.text(matchObj.metaData.location)
									.prepend(
										$('<strong />')
											.text('At: ')
									)
							)
						.ENDIF()
						.attr('id','summary-statistics')
				)
				.attr('id','match-summary')
				
			// Attach the container DOM element for Match Summary to the DOM	
			matchSummaryContainerObj.insertBefore($('#match-heading'))
			
			configuration.matchSummaryContainerObj = matchSummaryContainerObj;
			
			// Set up handler for the toggle goalscorers button and the social share links
			eventHandlersInit();
						
			if(!matchCentre.configuration.siteInitialized){
				// Notify dispatcher that a component has finished initializing
				matchCentre.dispatcher.initializeComplete({
					finishedId: 'matchSummary'
				});
			}
		}
		catch(err){			
			$.logEvent('[matchCentre.matchSummary.build]: an error occured populating the Match Summary component: ' + $.logJSONObj(err));
			
			if(!matchCentre.configuration.siteInitialized){
				// Notify dispatcher that a component has finished initializing
				matchCentre.dispatcher.initializeComplete({
					finishedId: 'matchSummary'
				});
			}
		}
	}
		
	/**
	* Set up handler for the toggle goalscorers button
	* @method eventHandlersInit
	*/
	function eventHandlersInit(){
		$.logEvent('[matchCentre.matchSummary.eventHandlersInit]');
		
		// Create the event handler for the toggle goalscorers button
		$('#toggle-goal-scorers A').on('click',function(e){
			e.preventDefault();
			
			var tableObj = $('TABLE',configuration.matchSummaryContainerObj);
			$('TABLE',configuration.matchSummaryContainerObj).toggle();
			
			$(this).attr('class',tableObj.is(':visible') ? 'expanded' : 'contracted');
		});
	}
	
	/** 
	* Add an array of events to a single player
	* @method attachEventToPlayer
	* @param {OBJECT} homeTeamGoalScorers A JSON object containing all goal information for the home team
	* @param {OBJECT} awayTeamGoalScorers A JSON object containing all goal information for the away team
	* @return {OBJECT} The complete DOM object to be attached to the current match
	*/	
	function attachGoalsScoredToMatch(obj){
		$.logEvent('[matchCentre.matchSummary.attachGoalsScoredToMatch]: ' + $.logJSONObj(obj));
				
		var maximumGoals = obj.homeTeamGoalScorers.length > obj.awayTeamGoalScorers.length ? obj.homeTeamGoalScorers.length : obj.awayTeamGoalScorers.length;
		var goalsDomObj = $('<tbody />');
				
		// Loop through the maximum goals counter, to create a table row for each goal, which will allow the alternating row pattern to be created
		for(var i=0; i<=maximumGoals-1; i++){
			goalsDomObj
				.append(
					$('<tr />')
						.append(
							$('<td />')
								.attr('class','home')
								.html(obj.homeTeamGoalScorers[i] != undefined ? obj.homeTeamGoalScorers[i].name + ' ' + obj.homeTeamGoalScorers[i].minuteOfGoal + '"' : '&nbsp;')
						)
						.append(
							$('<td />')
								.attr('class','separator')
								.html('&nbsp;')
						)
						.append(
							$('<td />')
								.attr('class','away')
								.html(obj.awayTeamGoalScorers[i] != undefined ? obj.awayTeamGoalScorers[i].name + ' ' + obj.awayTeamGoalScorers[i].minuteOfGoal + '"' : '&nbsp;')
						)
				)
		}
		
		return goalsDomObj;
	}
	
	/**
	* Process the Pusher real-time update
	* @method websocketCallback
	* @param {OBJECT} data A JSON update from Pusher
	*/	
	function websocketCallback(obj){
		$.logEvent('[matchCentre.matchSummary.websocketCallback]: ' + $.logJSONObj(obj));

		// Show the toggle arrow to show/hide the goalscorers
		showGoalScorersToggle();
		
		// Since the websocket update re-defines the entire goalscorers list, any previous scores need to be removed before adding the new updates
		$('TBODY',configuration.matchSummaryContainerObj).remove();
		
		// Capture any errors should the content for the Match Summary not be correct
		try{
			// Update the score
			$('OL LI.separator',configuration.matchSummaryContainerObj).text(obj.data.matchSummary.content.homeTeam.goalsScored.length + ' - ' + obj.data.matchSummary.content.awayTeam.goalsScored.length);
			
			// Dynamically assign the referee name to the match
			addMetaDataToMatch({
				metaDataObj: obj.data.matchSummary.metaData,
				tagLabel: 'Ref',
				tagName: 'referee'
			});
			
			// Dynamically assign attendance to the match
			addMetaDataToMatch({
				metaDataObj: obj.data.matchSummary.metaData,
				tagLabel: 'Attendance',
				tagName: 'attendance'
			});
			
			// Dynamically assign location to the match
			addMetaDataToMatch({
				metaDataObj: obj.data.matchSummary.metaData,
				tagLabel: 'At',
				tagName: 'location'
			});
			
			// Update the goalscorers
			$('TABLE',configuration.matchSummaryContainerObj)
				.append(
					attachGoalsScoredToMatch({
						homeTeamGoalScorers: obj.data.matchSummary.content.homeTeam.goalsScored, 
						awayTeamGoalScorers: obj.data.matchSummary.content.awayTeam.goalsScored
					})
				)				
		}
		catch(err){
			$.logEvent('[matchCentre.matchSummary.websocketCallback]: an error occured populating the Match Summary component (via Pusher): ' + $.logJSONObj(err));
		}
	}

	/**
	* Dynamically assign either the attendance/location/referee name to the match
	* @method addMetaDataToMatch
	* @param {OBJECT} metaDataObj The metaData JSON object
	* @param {STRING} tagLabel The label for the meta data which has been provided 
	* @param {STRING} tagName The meta data which has been provided 
	*/	
	function addMetaDataToMatch(obj){
		$.logEvent('[matchCentre.matchSummary.addMetaDataToMatch]: ' + $.logJSONObj(obj));

		if(obj.metaDataObj.hasOwnProperty(obj.tagName)){
			if($('#summary-statistics LI[data-meta-type=' + obj.tagName + ']').size() == 1){
				$('#summary-statistics LI[data-meta-type=' + obj.tagName + ']')
					.text(obj.metaDataObj[obj.tagName])
					.prepend(
						$('<strong />')
							.text(obj.tagLabel + ': ')
					)
			}
			else{
				$('#summary-statistics')
					.append(
						$('<li />')
							.attr('data-meta-type',obj.tagName)
							.text(obj.metaDataObj[obj.tagName])
							.prepend(
								$('<strong />')
									.text(obj.tagLabel + ': ')
							)
					)
			}
		}
	}
	
	/**
	* Show the toggle arrow to show/hide the goalscorers
	* @method showGoalScorersToggle
	*/
	function showGoalScorersToggle(){
		$.logEvent('[matchCentre.matchSummary.showGoalScorersToggle]');
		
		$('#toggle-goal-scorers').attr('class','visible');
	}
	
	return {
		build: build,
		configuration: configuration,
		websocketCallback: websocketCallback
	}
}());