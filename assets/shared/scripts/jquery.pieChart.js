(function(jQuery){
	var pieChartConfiguration = {
		balloonEnabled: false,
		fontSize: 20,
		hideLabelsPercent: 15,
		labelRadius: -30,
		labelsEnabled: true,		
		outlineThickness: 0,
		radius: '50%',
		startDuration: 0
	}
	
	var methods = {
		/**
		* Build the chart dynamically
		* @method build
		* @param {OBJECT} data The chart data
		* @param {OBJECT} domElementId The unique guid of the update
		*/
		build: function(obj){			
			var selfObj = $(this);

			var chartObj = new AmCharts.AmPieChart();
			chartObj.dataProvider = obj.data;
			chartObj.colorField = 'color';	
			chartObj.titleField = 'labelText';
			chartObj.valueField = 'value';			
			chartObj.balloon.enabled = pieChartConfiguration.balloonEnabled;
			chartObj.fontSize = pieChartConfiguration.fontSize;
			chartObj.labelColorField = 'labelColor';
			chartObj.labelsEnabled = pieChartConfiguration.labelsEnabled;
			chartObj.labelRadius = pieChartConfiguration.labelRadius;
			chartObj.labelText = '[[value]]%';
			chartObj.radius = pieChartConfiguration.radius;
			chartObj.startDuration = pieChartConfiguration.startDuration;
			chartObj.hideLabelsPercent = pieChartConfiguration.hideLabelsPercent;
			
			var chartDomObj = $('<div />')
				.append(
					$('<ul />')
						.append(
							$('<li />')
								.append(
									$('<span />')
										.html(obj.data[1].labelText)
								)
						)
						.append(
							$('<li />')
								.append(
									$('<span />')
										.html(obj.data[0].labelText)
								)
						)
				)
				.append(
					$('<div />')
						.attr({
							'class': 'chart-inner',
							id: 'pie-chart-' + obj.domElementId + '-inner'
						})
				)
				.attr('class','chart');
				
			selfObj.append(chartDomObj);

			// Create the chart
			setTimeout(function(){
				chartObj.write($('.chart-inner',chartDomObj).attr('id'));
				
				// Store a reference to the chart, in order to call re-size functions when switching devices
				matchCentre.newsFeed.configuration.embeddedChartObjs.push(chartObj);
			},1);
		}		
	};
	
	// Initialize plugin
	$.fn.pieChart = function(obj){
		// Method calling logic
		if(methods[obj]){
			return methods[obj].apply(this,Array.prototype.slice.call(arguments,1));
		} 
		else if(typeof obj === 'object' || ! obj){
			return methods.init.apply(this,arguments);
		}
	};
}(jQuery));