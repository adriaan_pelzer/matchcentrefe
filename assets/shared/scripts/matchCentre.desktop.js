/**
* MatchCentre v1.0
* @module matchCentre
*/

var matchCentre = window.matchCentre || {};

/**
* MatchCentre root class
* @class matchCentre
* @namespace matchCentre.desktop
*/

matchCentre.desktop = (function(){
	/**
	* Initialize all desktop functionality
	* @method init
	*/
	function init(){
		$.logEvent('[matchCentre.desktop.init]');	
		
		matchCentre.configuration.deviceType = 'desktop';
	
		// Initialize the countdown timer
		matchCentre.core.countdownTimerInit();
		
		// Initialize the fixtures
		matchCentre.fixtures.init({deviceType: 'desktop'});
		
		// Re-instate the opacity of the .source DOM element(s) to be visible, regardless of toggle state, remove inline image height attributes
		matchCentre.newsFeed.refresh();
		
		// Reset the visibility of the dropdown bar
		matchCentre.dropdownBar.reset();
		
		// Reset the visibility of the footer bar
		matchCentre.footerBar.core.reset();
		
		// Resize all amCharts objects
		matchCentre.newsFeed.amChartsRefresh();
	}
	
	/**
	* Unload all associated event handlers relating to desktop
	* @method unload
	*/
	function unload(){
		$.logEvent('[matchCentre.desktop.unload]');
		
		// Reset the visibility of the dropdown bar
		// matchCentre.dropdownBar.reset();
		
		// Reset the visibility of the footer bar
		matchCentre.footerBar.core.reset();
		
		// Unload the fixtures
		matchCentre.fixtures.unload();
	}
		
	return {
		init: init,
		unload: unload
	}
}());