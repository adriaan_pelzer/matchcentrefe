/**
* MatchCentre v1.0
* @module matchCentre
*/

var matchCentre = window.matchCentre || {};

/**
* MatchCentre root class
* @class matchCentre
* @namespace matchCentre.omniture
*/
matchCentre.omniture = (function(){
	var configuration = {
		additionalPageProperties: {},
		channel: 'match-centre',
		hier1: 'mirror:match-centre',
		prop1: 'match-centre',
		prop5: 'mirror',
		prop11: 'mc-blog',	
		prop16: 'MGN5',
		prop21: 'MGN2',
		server: 'mirror',
		trackingEnabled: true
	}
		
	/** 
	* Send tracking request to Omniture
	* @method track
	* @param {OBJECT} additionalProperties Extra properties required for click events
	* @param {OBJECT} callee The DOM object to exceute the tracking against
	* @param {STRING} friendlyTitle The 'Link name' entry for the link
	* @param {STRING} trackingType 'page-view' || 'event-click'				
	*/	
	function track(obj){
		// Only perform the tracking if it's been enabled
		if(configuration.trackingEnabled){
			$.logEvent('[matchCentre.omniture.track: ' + $.logJSONObj(obj));
			
			try {
				var additionalProperties = {};
				$.extend(additionalProperties,obj.additionalProperties);
				
				switch(obj.trackingType){
					case 'page-view':
						s.pageName = matchCentre.configuration.omniture.pageName;
						s.server = configuration.server;
						s.channel = configuration.channel;
						s.hier1 = configuration.hier1;
						s.prop1 = configuration.prop1;
						s.prop5 = configuration.prop5;
						s.prop11 = configuration.prop11;
						s.prop16 = configuration.prop16;
						s.prop21 = configuration.prop21;
						s.prop64 = matchCentre.configuration.deviceType;
						s.eVar64 = matchCentre.configuration.deviceType;
						
						$.logEvent('[matchCentre.omniture.track (page view, defaults)]: s.pageName=' + s.pageName + ', s.server=' + s.server + ' s.channel=' + s.channel + ', s.hier1=' + s.hier1 + ', s.prop1=' + s.prop1 + ', s.prop5=' + s.prop5 + ', s.prop11=' + s.prop11 + ', s.prop16=' + s.prop16 + ', s.prop21=' + s.prop21 + ', s.prop64=' + s.prop64 + ', s.eVar64=' + s.eVar64);
						
						// Attach all local properties, specific to the unique page
						$.each(additionalProperties,function(keyName,keyValue){
							s[keyName] = keyValue;
							$.logEvent('[matchCentre.omniture.track (page view, custom)]: s.' + keyName + '=' + keyValue);
						});

						// Submit the track event to Omniture
						s.t();
					
						break;
					case 'event-click':						
						s.pageName = matchCentre.configuration.omniture.pageName;
						s.events = 'event29';
						s.prop5 = configuration.prop5;
						s.prop16 = configuration.prop16;
						s.prop21 = configuration.prop21;
						
						s.linkTrackVars = 'pageName,prop5,prop16,prop21,prop71,eVar50,eVar75';
						s.linkTrackEvents = s.events = 'event29';
						s.prop71 = 'lnk|' + additionalProperties.dataContext + '|' + additionalProperties.dataType + '|' + additionalProperties.dataAction;
						s.eVar50 = 'lnk|' + s.pageName;
						s.eVar75 = s.eVar50 + '_' + s.prop71;
						
						$.logEvent('[matchCentre.omniture.track (click event, defaults)]: s.pageName=' + s.pageName + ', s.events=' + s.events + ', s.prop5=' + s.prop5 + ', s.prop16=' + s.prop16 + ', s.prop21=' + s.prop21 + ', s.linkTrackVars=' + s.linkTrackVars + ', s.linkTrackEvents=' + s.linkTrackEvents + ', s.prop71=' + s.prop71 + ', s.eVar50=' + s.eVar50 + ', s.eVar75=' + s.eVar75);
					
						// Retrieve all click-specific properties
						$.each(additionalProperties,function(key,value){
							if(key.substr(0,4) != 'data'){
								s[keyName] = keyValue;
								$.logEvent('[matchCentre.omniture.track (click event, custom)]: s.' + key + '=' + value);
							}
						});
					
						// Submit the track event to Omniture
						s.tl(obj.callee,'o',obj.friendlyTitle);
					
						break;
				}
			}
			catch(err){
				$.logEvent('[matchCentre.omniture.track]: an error occured submitted a page track (pageName=' + matchCentre.configuration.omniture.pageName + '): ' + $.logJSONObj(err));
			}
		}
	}
	
	return {
		configuration: configuration,
		track: track
	}
}());