/**
* matchCentre
* @module matchCentre
*/

var matchCentre = window.matchCentre || {};

/**
* matchCentre dispatcher class
* @class matchCentre
* @namespace dispatcher
*/
matchCentre.dispatcher = (function(){		
	/**
	* Initialize the required functionality for the desired module
	* @method moduleInitialize
	* @param {STRING} type The type of component, mapped to the plugin name used to build/initialize it
	*/
	function moduleInitialize(obj){
		$.logEvent('[matchCentre.dispatcher.moduleInitialize]: ' + $.logJSONObj(obj));
		
		switch(obj.id){
			case 'fixtures':
				matchCentre.fixtures.build();
				break;
			case 'leagueTable':
				matchCentre.leagueTable.build();
				break;
			case 'inSummary':
				matchCentre.inSummary.build();
				break;
			case 'matchSummary':
				matchCentre.matchSummary.build();
				break;				
			case 'newsFeed':
				matchCentre.newsFeed.build();
				break;
			case 'footerBar': 
				matchCentre.footerBar.core.init();
				break;
		}
	}
	
	/**
	* Notify dispatcher that a component has finished initializing
	* @method initializeComplete
	* @param (STRING} finishedId The DOM element id of the module which has finished initializing
	*/
	function initializeComplete(obj){
		$.logEvent('[matchCentre.dispatcher.initializeComplete]: ' + $.logJSONObj(obj));
				
		var loadSequence = matchCentre.configuration.loadSequence;
		var matchedSequencePosition;
				
		for(i=0; i<matchCentre.configuration.loadSequence.length; i++){
			// Locate the component which just finished initializing
			if(matchCentre.configuration.loadSequence[i].id == obj.finishedId){
				matchedSequencePosition = i;
				
				// Update the configuration to confirm that the passed component id has finishined initializing
				matchCentre.configuration.loadSequence[matchedSequencePosition].initialized = true;
				
				break;
			}
		}
		
		// Check to see if there are any more components left to initialize
		if(matchCentre.configuration.loadSequence.length > (matchedSequencePosition+1)){
			var newComponentToInitializeObj = matchCentre.configuration.loadSequence[matchedSequencePosition+1];
		
			// Initialize the required functionality for the desired module
			moduleInitialize(newComponentToInitializeObj);
		}
		else{
			matchCentre.configuration.siteInitialized = true;
			
			// All actions within the load sequence are now complete
			$.logEvent('[matchCentre.dispatcher.initializeComplete]: site dispatcher is complete');

			// Remove the site loader/overlay, used to hide the site content, whilst JSON was being loaded, parsed and processed
			$('#site-loader').fadeOut(matchCentre.configuration.siteLoaderEnabled ? matchCentre.configuration.timings.siteLoaderFadeOut : 1,function(){
				$(this).remove();
			});

			// Run any other functionality which is not part of the sequence loader, but should be executed as the last scripts of initialization
			matchCentre.core.postSequenceFunctions();

			// Only a single connection is required to Pusher, which can then have multiple subscriptions attached to it
			// Always open the connection, regardless of whether websockets are enabled or not. This will enable channels to be dynamically subscribed/unsubscribed, knowing that the connection object is always going to be open
			matchCentre.websockets.connectToPusher();
						
			// Only open websockets if configured to do so AND specified within the toggle switches
			if(matchCentre.websockets.configuration.enabled && matchCentre.dropdownBar.configuration[matchCentre.configuration.pageType].toggleSwitches.settings['auto-updates']){
				// Because the load sequences are different, dependant on whether Match Day Live or a Match Detail page is being viewed, request websockets only for those elements on the page
				$.each(matchCentre.configuration.loadSequence,function(index,componentObj){
					// Do not process any footerBar websockets, since the opening (and closing) of their websockets are triggered by the open/closed state of the footerBar at any given time
					if(componentObj.id != 'footerBar'){
						matchCentre.websockets.channelSubscribe({
							callbackOrigin: componentObj.id, // For all root-namespaced components, the callback origin and the componentId are the same. For footerBar, they are different
							componentId: componentObj.id
						});
					}
				});
				
				// If running in simulation mode, load the broadcasts in a popup window
				setTimeout(function(){
					if(!matchCentre.configuration.initialSiteContent.configuration.pusher.runLive){
						var modalWindow = window.open(matchCentre.configuration.urls.local.pusherSimulation,'pusher','alwaysRaised=yes,height=200,left=0,modal=yes,top=0,width=200',null);
						
						if(matchCentre.websockets.configuration.runMultipleInSimulation){
							// Load the 2nd set of simulated events
							setTimeout(function(){
								modalWindow.location.href = matchCentre.configuration.urls.local.pusher2ndSimulation;
							},matchCentre.websockets.configuration.timings.websocketMultipleTriggerDelay*1);
							
							// Load the 3rd set of simulated events
							setTimeout(function(){
								modalWindow.location.href = matchCentre.configuration.urls.local.pusher3rdSimulation;
							},matchCentre.websockets.configuration.timings.websocketMultipleTriggerDelay*2);
						}
					}
				},matchCentre.websockets.configuration.timings.websocketTriggerDelay);
			}
		}
	}
		
	return {
		initializeComplete: initializeComplete,
		moduleInitialize: moduleInitialize
	}
}());