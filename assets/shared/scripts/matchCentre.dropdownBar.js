/**
* MatchCentre v1.0
* @module matchCentre
*/

var matchCentre = window.matchCentre || {};

/**
* MatchCentre root class
* @class matchCentre
* @namespace matchCentre.dropdownBar
*/

matchCentre.dropdownBar = (function(){
	var configuration = {
		dropdownBarObj: $('#dropdown-bar'),
		expanded: false,
		subGroupExpanded: false,
		timings: {
			toggleHorizontal: 250,
			toggleVertical: 250
		},
		matchDayLive: {
			toggleSwitches: {
				filterPosts: {
					'match-update': true,
					'goal': true,
					'play-by-play': true,
					'at-the-ground': true,
					'home-bias': true,
					'away-bias': true,
					'your-say': true,
					'statistic': true			
				},
				settings: {
					'auto-updates': true,
					'show-newest-first': true
				}
			}
		},
		matchDetail: {
			toggleSwitches: {
				filterPosts: {
					'match-update': true,
					'goal': true,
					'play-by-play': true,
					'at-the-ground': true,
					'home-bias': true,
					'away-bias': true,
					'your-say': true,
					'statistic': true			
				},
				settings: {
					'auto-updates': true,
					'show-newest-first': true
				}
			}
		}
	}

	/**
	* Initialize the dropdown bar 
	* @method init
	*/
	function init(){
		$.logEvent('[matchCentre.dropdownBar.init]');
				
		// Initialize the toggle switches
		toggleSwitchesInit();
		
		// Store the height of the dropdown groups as a data attribute, for later access when toggling
		setOriginalHeights();
		
		// Dynamically build the 'Stats' group (and sub-content)
		createStatsGroup();
		
		// Initialize event handlers for the show/hide
		eventHandlersInit();
	}
	
	/** 
	* If local storage is supported, ensure that the necessary default storage is saved
	* @method localStorageInit
	*/
	function localStorageInit(){
		// Check local storage to see if the necessary data is stored, relating to the current page type
		matchCentre.configuration.localStorageObj = $.localStorage;
		
		if(matchCentre.configuration.localStorageObj.isEmpty('dropdownBar.' + matchCentre.configuration.localStorageNode + '.toggleSwitches')){
			$.logEvent('[matchCentre.dropdownBar.localStorageInit]: setting localstorage for dropdownBar.' + matchCentre.configuration.localStorageNode + '.toggleSwitches');

			// Set the local storage default values
			matchCentre.configuration.localStorageObj.set('dropdownBar.' + matchCentre.configuration.localStorageNode + '.toggleSwitches',configuration[matchCentre.configuration.pageType].toggleSwitches);
		}
		else{
			// Override the pre-defined defaults with the current local storage values, to ensure saved user configurations are used
			configuration[matchCentre.configuration.pageType].toggleSwitches = matchCentre.configuration.localStorageObj.get('dropdownBar.' + matchCentre.configuration.localStorageNode + '.toggleSwitches');
		}		
	}
		
	/**
	* Initialize the toggle switches
	* @method toggleSwitchesInit
	*/
	function toggleSwitchesInit(){
		$.logEvent('[matchCentre.dropdownBar.toggleSwitchesInit]');
				
		var toggleObj;
		var switchObj;

		$(':checkbox',configuration.dropdownBarObj).each(function(){
			toggleObj = $(this);
			
			// Identify whether a 'Filter Posts" or 'Settings' toggle switch has been changed
			var isFilterPost = toggleObj.hasAttr('data-filter-posts');
			var isSettingsPost = toggleObj.hasAttr('data-settings');
			
			// Set the checked state of the checkboxes (Filter Posts) based on the configuration within this namespace
			if(isFilterPost){
				if(configuration[matchCentre.configuration.pageType].toggleSwitches.filterPosts[toggleObj.attr('data-label')]){
					toggleObj.attr('checked','checked');
				}
			}
			
			// Set the checked state of the checkboxes (Settings) based on the configuration within this namespace
			if(isSettingsPost){
				if(configuration[matchCentre.configuration.pageType].toggleSwitches.settings[toggleObj.attr('data-label')]){
					toggleObj.attr('checked','checked');
				}
			}
			
			// Enable the toggle switch functionality
			toggleObj.iphoneStyle({
				checkedLabel: toggleObj.attr('data-checked-label'),
				onChange: function(element,subscribeToChannel){
					switchObj = $(element[0]);
											
					// If a Filter Post toggle switch has been changed, reflect that within the stored configuration, and perform the visual toggle within the news feed
					if(isFilterPost){
						configuration[matchCentre.configuration.pageType].toggleSwitches.filterPosts[switchObj.attr('data-label')] = subscribeToChannel;
												
						// Toggle the hidden state, switch it to the opposite of whatever it currently is
						$('.update[data-update-type="' + switchObj.attr('data-label') + '"]',matchCentre.newsFeed.configuration.newsFeedDomObj)
							.IF(subscribeToChannel)
								.removeClass('hidden')
							.ELSE()
								.addClass('hidden')
							.ENDIF()
						
						if(switchObj.attr('data-label') == 'statistic' && subscribeToChannel){
							// Resize all amCharts objects	
							matchCentre.newsFeed.amChartsRefresh();
						}
		
						// Send tracking request to Omniture
						matchCentre.omniture.track({
							additionalProperties: {
								dataAction: switchObj.attr('data-label') + ':' + (subscribeToChannel ? 'on' : 'off'),
								dataType: 'switch:' + switchObj.attr('data-label'),
								dataContext: 'mc-filter-bar'
							}, 
							callee: this, 
							friendlyTitle: 'Filter (' + switchObj.attr('data-label') + '):' + (subscribeToChannel ? 'on' : 'off'),
							trackingType: 'event-click'
						});
					}
					else if(isSettingsPost){
						configuration[matchCentre.configuration.pageType].toggleSwitches.settings[switchObj.attr('data-label')] = subscribeToChannel;
						
						switch(switchObj.attr('data-label')){
							case 'auto-updates':								
								try{
									// Loop through all open websockets defined in the load sequence (except footer bar), in order to turn them on/off (footer bar websockets are closed when this button is changeable, so would always already be unsubscribed)
									$.each(matchCentre.configuration.loadSequence,function(index,componentObj){
										if(componentObj.id != 'footerBar'){
											if(subscribeToChannel){
												// Subscribe to the requested websocket channel, which will broadcast all real-time website updates for that unique component												
												$.logEvent('[matchCentre.dropdownBar.toggleSwitchesInit]: channels to subscribe to: ' + componentObj.id);
												
												matchCentre.websockets.channelSubscribe({
													callbackOrigin: componentObj.id,
													componentId: componentObj.id
												});
											}
											else{												
												var channelNamePrefix = matchCentre.configuration.initialSiteContent.configuration.pusher.channelName;
												var channelFullName;

												if(matchCentre.websockets.configuration.types[componentObj.id] == 'local' && !matchCentre.configuration.isMatchDetailPage){
													channelFullName = channelNamePrefix + '.' + componentObj.id;
												}
												else{
													channelFullName = channelNamePrefix + (matchCentre.websockets.configuration.types[componentObj.id] == 'local' ? '.' + matchCentre.configuration.fixtureId + '.' : '.') + componentObj.id;
												}
												
												// Unsubscribe from the requested websocket channel
												$.logEvent('[matchCentre.dropdownBar.toggleSwitchesInit]: channels to unsubscribe from: ' + channelFullName);

												matchCentre.websockets.channelUnsubscribe({
													channelFullName: channelFullName
												});
											}
										}
									});
								}
								catch(err){
									$.logEvent('[matchCentre.dropdownBar.toggleSwitchesInit]: an error occured toggle the \'Auto updates\' button: ' + err);
								}
								
								break;
							case 'show-newest-first':
								// Reorder the news feed to be the reverse of what it currently is
								reorderNewsFeed();
								
								break;
						}
						
						// Send tracking request to Omniture
						matchCentre.omniture.track({
							additionalProperties: {
								dataAction: switchObj.attr('data-label') + ':' + (subscribeToChannel ? 'on' : 'off'),
								dataType: 'switch:' + switchObj.attr('data-label'),
								dataContext: 'mc-filter-bar'
							}, 
							callee: this, 
							friendlyTitle: 'Settings (' + switchObj.attr('data-label') + '):' + (subscribeToChannel ? 'on' : 'off'),
							trackingType: 'event-click'
						});
					}
					
					// Reflect the change also in local storage, for return page visits
					if(matchCentre.configuration.supportsLocalStorage){
						matchCentre.configuration.localStorageObj.set('dropdownBar.' + matchCentre.configuration.localStorageNode + '.toggleSwitches',configuration[matchCentre.configuration.pageType].toggleSwitches);
					}
				},
				uncheckedLabel: toggleObj.attr('data-unchecked-label')
			});
		});
	}
	
	/**
	* Reorder the news feed to be the reverse of what it currently is
	* @method reorderNewsFeed
	*/
	function reorderNewsFeed(obj){
		$.logEvent('[matchCentre.dropdownBar.reorderNewsFeed]');

		var currentUpdatesTotal = $('.update',matchCentre.newsFeed.configuration.newsFeedDomObj).size();
		
		for(var i=0; i<=currentUpdatesTotal-1; i++){
			$('.update:eq(' + i + ')',matchCentre.newsFeed.configuration.newsFeedDomObj)
				.prependTo($('>.inner',matchCentre.newsFeed.configuration.newsFeedDomObj))
		}
	}
	
	/**
	* Store the height of the dropdown groups as a data attribute, for later access when toggling
	* @method setOriginalHeights
	*/
	function setOriginalHeights(){
		$.logEvent('[matchCentre.dropdownBar.setOriginalHeights]');
		
		var flyoutContentObj;
		var internalScrollerObj;
		
		$('.flyout-content',configuration.dropdownBarObj).each(function(){
			flyoutContentObj = $(this);
			
			// Determine whether the dropdown bar flyout-content wrapper DOM elements require internal scrolling or not
			internalScrollerObj = scrollerDimensions({
				dropdownContentObj: flyoutContentObj,
				originalHeight: flyoutContentObj.height()
			});
			
			if(!flyoutContentObj.hasAttr('data-original-height')){
				flyoutContentObj
					.attr({
						'data-original-height': flyoutContentObj.height(),
						'data-scroll-height': internalScrollerObj.scrollHeight,
						'data-scrolling-required': internalScrollerObj.scrollingRequired
					})
					.css({
						left: '',
						top: ''
					})
					.height(0)
					.addClass('loaded')
			}
		});
	}
	
	/**
	* Dynamically build the 'Stats' group (and sub-content)
	* @method createStatsGroup
	*/
	function createStatsGroup(){
		$.logEvent('[matchCentre.dropdownBar.createStatsGroup]');
		
		var listItems = '';
		var menuObj;
		var statsGroupObj;
						
		statsGroupObj = $('<div />')
			.append(
				$('<ul />')
					.append(function(){
						$('[data-flyout=true]').each(function(index){
							menuObj = $(this);
							
							listItems += '<li><a href="#" data-from-footer-bar="true" data-flyout-title="' + menuObj.attr('data-flyout-title') + '" data-content="' + menuObj.attr('data-flyout-origin') + '" data-content-area="' + menuObj.attr('data-content-area') + '">' + menuObj.attr('data-flyout-title') + '<span>Click here to view ' + menuObj.attr('data-flyout-title') + '</span></a></li>';
							
							return listItems;
						})
						
						return listItems;
					})
					.attr('class','triggers')
			)
			.append(
				$('<a />')
					.attr({
						'class': 'button',
						href: '#'
					})
					.text('Close')
			)
			.attr({
				'class': 'flyout-content',
				id: 'container-stats'
			})
			.insertAfter($('#container-filter-posts'))
			
			// Determine whether the dropdown bar flyout-content wrapper DOM elements require internal scrolling or not
			var internalScrollerObj = scrollerDimensions({
				dropdownContentObj: statsGroupObj,
				originalHeight: statsGroupObj.height()
			});
			
		// Since the items has now been injected, the data attribute (data-original-height) can now be added as the height is retrievable
		statsGroupObj
			.attr({
				'data-original-height': $('#container-stats').height(),
				'data-scroll-height': internalScrollerObj.scrollHeight,
				'data-scrolling-required': internalScrollerObj.scrollingRequired
			})
			.height(0)
			.addClass('loaded')
	}
	
	/**
	* Initialize event handlers for the show/hide
	* @method eventHandlersInit
	*/
	function eventHandlersInit(){
		$.logEvent('[matchCentre.dropdownBar.eventHandlersInit]');
		
		var activeSiblings;
		var requestedGroupObj;
		
		$('>UL LI',configuration.dropdownBarObj).on('click',function(e){					
			e.preventDefault();
			
			// Scroll the window to the top
			$.scrollTo({
				left: 0,
				top: 0
			},1);
			
			// If default gesture scrolling is disabled, re-enable it
			if(!matchCentre.configuration.defaultGestureScrollingEnabled){
				// Enable (gesture-based) page scrolling
				matchCentre.core.enableDefaultGestureScrolling();
			}
			
			// Don't process the handler if the selected element is already active
			if($(this).hasClass('active')){
				// Hide any previously open flyouts
				resetSiblingsVisibility();

				if(matchCentre.footerBar.core.configuration.isVisible){
					// Show/hide the footer bar
					matchCentre.footerBar.core.showHideFooterBar({mode: 'hide'});
				}
			}
			else{
				// Hide any 'Stats' sub-groups (if visible, only possible for either tablet or mobile, NOT desktop)
				if(configuration.subGroupExpanded){
					$('.triggers .module',configuration.dropdownBarObj).remove();
					configuration.subGroupExpanded = false;
				}
							
				activeSiblings = false;
				requestedGroupObj = $('#container-' + $(this).attr('id'));

				// Before showing the visible group, ensure that any ones currently active are reset to hidden
				requestedGroupObj.siblings('DIV').each(function(){
					if($(this).height() > 0){
						activeSiblings = true;
					}
				});
				
				if(activeSiblings){
					// Hide any previously open flyouts
					resetSiblingsVisibility();
					
					setTimeout(function(){
						// Show the selected group
						toggleGroupVisibility({
							requestedGroupObj: requestedGroupObj
						});
					},configuration.timings.toggleVertical);
				}
				else{
					// Show the selected group
					toggleGroupVisibility({
						requestedGroupObj: requestedGroupObj
					},configuration.timings.toggleVertical);
				}
			}
		});
		
		// Initialize handlers for each of the 'Close' buttons
		$('.flyout-content>.button').on('click',function(e){
			e.preventDefault();

			// Hide any previously open flyouts
			resetSiblingsVisibility();
			
			if(matchCentre.footerBar.core.configuration.isVisible){					
				// Show/hide the footer bar
				matchCentre.footerBar.core.showHideFooterBar({mode: 'hide'});
			}
		});
		
		// Initialize handlers for the sub-links (Stats only)
		var triggerObj;
		var triggerNodeName;
		var fromFooterBar;
				
		$('UL.triggers>LI>A',configuration.dropdownBarObj).on('click',function(e){
			e.preventDefault();
			
			triggerObj = $(this);
			triggerNodeName = e.target.nodeName.toUpperCase();
			fromFooterBar = triggerObj.attr('data-content-area') == 'footer-bar';
						
			if((triggerNodeName == 'LI' || triggerNodeName == 'A') && triggerObj.hasAttr('data-content')){
				// Scroll the window to the top
				$.scrollTo({
					left: 0,
					top: 0
				},0);

				triggerObj
					.parent()
						.append(
							$('<div />')
								.attr('class',triggerObj.attr('data-content') + ' module')
								.IF(fromFooterBar)
									/* When automatically showing the footer bar as well as the dropdown bar */
									.append(
										$('<div />')
											.attr('class','inner')
											.html($('.' + triggerObj.attr('data-content') + ' .jspPane').html())
									)
									.prepend(
										$('<h3 />')
											.append(
												$('<strong />')
													.text(triggerObj.attr('data-flyout-title'))
											)
									)
									/* When showing just the dropdown bar, without interactivity from the footer bar */
									// .html($('.' + triggerObj.attr('data-content')).html())
								.ELSE()
									.html($('.' + triggerObj.attr('data-content')).html())
								.ENDIF()
								.prepend(
									$('<a />')
										.attr('class','close')
										.text('Close')
								)
						)
						
				// With the content now moved to the dropdown bar, animate it into view				
				var subGroupInnerHeight;
				var subGroupObj = $('.module',configuration.dropdownBarObj);
				var subGroupInnerObj = $('.inner',subGroupObj);
				var innerOffsetTop = Math.floor(subGroupInnerObj.offset().top);
				var additionalInnerPadding = parseInt(subGroupInnerObj.css('padding-top')) + parseInt(subGroupInnerObj.css('padding-bottom'));

				var windowHeight = window.innerHeight;
				var thresholdHeight = windowHeight - innerOffsetTop - additionalInnerPadding;
				
				subGroupInnerHeight = Math.floor(subGroupInnerObj.height());
				
				$.logEvent('[matchCentre.dropdownBar.eventHandlers]: subGroupInnerHeight=' + subGroupInnerHeight + ', innerOffsetTop=' + innerOffsetTop + ', window height: ' + windowHeight + ', additionalInnerPadding: ' + additionalInnerPadding + ', thresholdHeight: ' + thresholdHeight);
					
				// Set the height for all inner DOM elements to the threshold height
				subGroupInnerObj.css('height',thresholdHeight);
				
				// Disable (gesture-based) page scrolling except for internal scrollers (iScroll)
				matchCentre.core.disableDefaultGestureScrolling();
				
				// Work out whether the flyouts inner DOM elements need scrolling attached to them
				if(subGroupInnerHeight > thresholdHeight){
					// Add an internal <div> and attach internal padding values to that, since iScroll does not take into account the presence of paddings or margins when calculating the height of it's scroller
					subGroupInnerObj
						.attr({
							id: 'internal-scroller'
						})
						.wrapInner(
							$('<div />')
								.attr('class','internal-scroller')
								.css({
									paddingTop: '15px'
								})
						);
						
					// Activate the internal scroller
					var myScroll = new IScroll('#internal-scroller');
				}
								
				// Perform left to right animation
				subGroupObj.css({
					left: 0,
					WebkitTransition: 'left ' + configuration.timings.toggleHorizontal + 'ms ease-in-out',
					transition: 'left ' + configuration.timings.toggleHorizontal + 'ms ease-in-out'
				});
				
				// Ensure that a flag is set, to identify that a sub-group is currently in an expanded state
				setTimeout(function(){
					configuration.subGroupExpanded = true;
				},configuration.timings.toggleHorizontal);
				
				// Initialize handlers for the sub-group content close button
				// Delegate this event, since the sub groups are injected post initial page load
				$('UL.triggers').delegate('.close','click',function(e){
					e.preventDefault();	

					// Ensure that iScroll doesn't show CSS3 "tramlines" when hiding the horizontal 'Stats' flyout: perform this BEFORE the flyout starts to move off to the left of the screen
					subGroupInnerObj
						.children(':first')
							.removeAttr('style');
					
					// Perform right to left animation
					$('.module',configuration.dropdownBarObj).css({
						left: '-100%',
						WebkitTransition: 'left ' + configuration.timings.toggleHorizontal + 'ms ease-in-out',
						transition: 'left ' + configuration.timings.toggleHorizontal + 'ms ease-in-out'
					});
					
					// Remove the injected flyout content
					setTimeout(function(){
						$('.module',configuration.dropdownBarObj).remove();
						configuration.subGroupExpanded = false;
						
						// Enable (gesture-based) page scrolling
						matchCentre.core.enableDefaultGestureScrolling();
					},configuration.timings.toggleHorizontal);
				});
			}
		});
	}
		
	/**
	* Hide any previously open flyouts
	* @method resetSiblingsVisibility
	*/
	function resetSiblingsVisibility(){
		$.logEvent('[matchCentre.dropdownBar.resetSiblingsVisibility]');
		
		var selfObj = $(this);
		
		$('.flyout-content').filter(function(){
			return $(this).height() > 0;
		}).css({
			height: 0,
			WebkitTransition: 'height ' + configuration.timings.toggleVertical + 'ms ease-in-out',
			overflow: 'hidden',
			transition: 'height ' + configuration.timings.toggleVertical + 'ms ease-in-out'
		});
				
		setTimeout(function(){
			// Remove all active classes
			$('>UL LI',configuration.dropdownBarObj).removeClass('active');		
			
			configuration.expanded = false;
		},configuration.timings.toggleVertical);
	}
	
	/**
	* Show the selected group
	* @method toggleGroupVisibility
	* @param {OBJECT} requestedGroupObj The DOM element which is to be expanded
	*/
	function toggleGroupVisibility(obj){
		$.logEvent('[matchCentre.dropdownBar.toggleGroupVisibility]: ' + $.logJSONObj(obj));
		
		var requestedHeight = obj.requestedGroupObj.attr('data-original-height');
		
		// Check to see whether the fixtures (carousel) is currently expanded. If it is, close it
		if(matchCentre.fixtures.configuration.expanded){
			$('#fixtures')
				.css({
					height: 0,
					WebkitTransition: 'height ' + matchCentre.fixtures.configuration.timings.expandContract + 'ms ease-in-out',
					transition: 'height ' + matchCentre.fixtures.configuration.timings.expandContract + 'ms ease-in-out'
				})
				
			// Update boolean flag, representing that the fixtures (carousel) has been closed
			matchCentre.fixtures.configuration.expanded = false;
		}
		
		var activeTriggerId = obj.requestedGroupObj.attr('id').replace('container-','');
		
		// Process the 'Stats' dropdown group differently to the other links, since it's inner content is derived from the footer bar component
		if(activeTriggerId == 'stats'){
			$('>UL LI#stats',configuration.dropdownBarObj).addClass('loading');
			
			// Show/hide the footer bar
			matchCentre.footerBar.core.showHideFooterBar({mode: 'show'});
		}
		else{
			// Expanded the group for the selected trigger item
			obj.requestedGroupObj.css({
				height: requestedHeight,
				WebkitTransition: 'height ' + configuration.timings.toggleVertical + 'ms ease-in-out',
				transition: 'height ' + configuration.timings.toggleVertical + 'ms ease-in-out'
			});
			
			setTimeout(function(){
				// To allow the component moving in from the side to overflow correctly, add overflow visibility
				obj.requestedGroupObj.css('overflow','visible');
			
				// Add the active state to the selected item, and remove from all siblings
				$('>UL LI#' + activeTriggerId,configuration.dropdownBarObj)
					.addClass('active')
					.siblings()
						.removeClass('active');
			
				configuration.expanded = true;
			},configuration.timings.toggleVertical);
		}
	}
	
	/**
	* Determine whether the dropdown bar flyout-content wrapper DOM elements require internal scrolling or not
	* @method scrollerDimensions
	* @param {OBJECT} dropdownContentObj The container DOM object which contains the content for potential scrolling 
	* @param {INTEGER} originalHeight The natural height of the dropdown bar flyout content DOM object
	* @return {INTEGER} scrollHeight After how many pixels of a flyout contents depth internal scrolling should be activated
	* @return {BOOLEAN} scrollingRequired Whether or not internal scrolling should be activated or not
	*/
	function scrollerDimensions(obj){
		$.logEvent('[matchCentre.dropdownBar.scrollerDimensions]: ' + $.logJSONObj(obj));
		
		var dropdownBarHeight = matchCentre.dropdownBar.configuration.dropdownBarObj.outerHeight(true)-1; // -1 takes account of the bottom border
		var contentOffsetTop = Math.ceil($('#content').offset().top) + dropdownBarHeight;
		var viewportHeight = $(window).height();
		var scrollHeight = viewportHeight-contentOffsetTop;
		
		var dimensions = {
			scrollHeight: scrollHeight,
			scrollingRequired: obj.originalHeight > scrollHeight
		}
		
		return dimensions;
	}
	
	/**
	* Reset the visibility of the dropdown bar
	* @method reset
	*/
	function reset(obj){	
		$.logEvent('[matchCentre.dropdownBar.reset]');
		
		// Hide any previously open flyouts
		resetSiblingsVisibility();
	}
	
	return {
		configuration: configuration,
		init: init,
		localStorageInit: localStorageInit,
		reset: reset
	}
}());