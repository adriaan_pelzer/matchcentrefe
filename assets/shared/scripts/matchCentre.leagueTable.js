/**
* MatchCentre v1.0
* @module matchCentre
*/

var matchCentre = window.matchCentre || {};

/**
* MatchCentre root class
* @class matchCentre
* @namespace matchCentre.leagueTable
*/

matchCentre.leagueTable = (function(){
	var configuration = {
		easing: 'easeInOutExpo',
		hidePositionWhileReordering: false,
		rowHeight: 25,
		timings: {
			moveToNewPosition: 1000
		}
	};
		
	/**
	* Build the league table in response to the initial pulled JSON content
	* @method build
	*/
	function build(){
		$.logEvent('[matchCentre.leagueTable.build]');
		
		// Create the necessary wrapper DOM elements
		var leagueTableContainerObj = $('<div />')
			.append(
				$('<h3 />')
					.append(
						$('<strong />')
							.html('Live League Table')
					)
			)
			.append(
				$('<div />')
					.append(
						$('<ul />')
							.append(
								$('<li />')
									.text('Pld')
									.prepend(
										$('<strong />')
											.html('Games played: ')
									)
							)
							.append(
								$('<li />')
									.text('Pts')
									.prepend(
										$('<strong />')
											.html('Points: ')
									)
							)
							.attr('class','legend')
					)
					.append(
						$('<ol />')
					)
					.attr('class','inner')
			)
			.attr({
				'class': 'league-table module',
				'data-flyout-title': 'Live League Table',
				'data-content-area': 'panel',
				'data-flyout-origin': 'league-table',
				'data-flyout': true
			})
			
		// Attach the container DOM element for fixtures to the DOM	
		$('#panel').append(leagueTableContainerObj);
		
		var teamObj;			

		// Capture any error should the content for the League Table not be correct
		try{
			// Iterate through the Fixtures JSON, and attach to the fixtures DOM element
			$.each(matchCentre.configuration.initialSiteContent.leagueTable.content,function(index,data){
				teamObj = $('<li />')
					.IF(this.id == matchCentre.configuration.homeTeamId || this.id == matchCentre.configuration.awayTeamId)
						.attr('class','active')
					.ENDIF()
					.css('top',index*configuration.rowHeight)
					.append(
						$('<strong />')
							.text((index+1) + '.')
					)
					.append(
						$('<h4 />')
							.text(this.teamName)
					)
					.append(
						$('<ul />')
							.append(
								$('<li />')
									.text(this.gamesPlayed)
									.prepend(
										$('<span />')
											.text('Played: ')
									)
							)
							.append(
								$('<li />')
									.text(this.points)
									.prepend(
										$('<span />')
											.text('Points: ')
									)
							)
					)
				
				teamObj.appendTo($('.inner OL',leagueTableContainerObj));
			});
			
			// Add the footer
			$('.inner',leagueTableContainerObj)
				.append(
					$('<div />')
						.append(
							$('<p />')
								.html('Last updated ' + matchCentre.configuration.initialSiteContent.leagueTable.metaData.lastUpdated)
						)
						.attr('class','footer')
				)
			
			// Specify globally a pointer to the newly injected League Table element
			// Reference it as a string, and not an object, to ensure the object can be accessed since it will be injected at a later date
			configuration.leagueTableObj = '.league-table';
			
			if(!matchCentre.configuration.siteInitialized){
				// Notify dispatcher that a component has finished initializing
				matchCentre.dispatcher.initializeComplete({
					finishedId: 'leagueTable'
				});
			}
		}
		catch(err){
			$.logEvent('[matchCentre.leagueTable.build]: an error occured populating the League Table component: ' + $.logJSONObj(err));
			
			if(!matchCentre.configuration.siteInitialized){
				// Notify dispatcher that a component has finished initializing
				matchCentre.dispatcher.initializeComplete({
					finishedId: 'leagueTable'
				});
			}
		}
	}
	
	/**
	* Process the Pusher real-time update
	* @method websocketCallback
	* @param {OBJECT} data A JSON update from Pusher
	*/	
	function websocketCallback(obj){
		$.logEvent('[matchCentre.leagueTable.websocketCallback]: ' + $.logJSONObj(obj));
		
		// Capture any errors should the content for the League Table not be correct
		try{
			var newLeagueTableObj = obj.data.leagueTable.content;		
			var leagueTableDomObj;
			
			$('.league-table').each(function(){
				leagueTableDomObj = $(this);

				// Loop through each team, and re-order in the league table
				$.each(newLeagueTableObj,function(index,value){
					if(configuration.hidePositionWhileReordering){
						$('OL>LI>STRONG',leagueTableDomObj).animate({opacity:0},1);
					}

					$('OL>LI>H4:contains(' + newLeagueTableObj[index].teamName + ')',leagueTableDomObj)
						.parent()
						.animate({
							top: index*configuration.rowHeight
						},{
							duration: configuration.timings.moveToNewPosition,
							easing: configuration.easing
						});
				});
				
				// Update the position, the games played and the points scored
				refreshData({
					data: newLeagueTableObj,
					leagueTableDomObj: leagueTableDomObj
				});
				
				// Update the meta data in the footer of the league table, since it's been updated
				$('.footer P',leagueTableDomObj).text('Last updated: ' + obj.data.leagueTable.metaData.lastUpdated);
			});
		}
		catch(err){
			$.logEvent('[matchCentre.leagueTable.websocketCallback]: an error occured populating the League Table component (via Pusher): ' + $.logJSONObj(err));
		}
	}
	
	/**
	* Update the position, the games played and the points scored
	* @method refreshData
	& @param {OBJECT} data The data for the table content refresh
	* @param {OBJECT} leagueTableDomObj The current league table DOM object	
	*/
	function refreshData(obj){
		$.logEvent('[matchCentre.leagueTable.refreshData]: ' + $.logJSONObj(obj));
		
		setTimeout(function(){
			$.each(obj.data,function(index,value){
				$('OL>LI>H4:contains(' + obj.data[index].teamName + ')',obj.leagueTableDomObj)
					.next()
						.find('LI:last')
							.text(obj.data[index].points)
					.prev()
						.text(obj.data[index].gamesPlayed)
						.prepend(
							$('<span />')
								.text('Played: ')
						)
					.parent()
					.prev()
					.prev()
						.text(index+1 + '.')
						.IF(configuration.hidePositionWhileReordering)
							.animate({opacity:1},1)
						.ENDIF()
			});
		},configuration.timings.moveToNewPosition);
	}
	
	return {
		build: build,
		configuration: configuration,
		websocketCallback: websocketCallback
	}
}());