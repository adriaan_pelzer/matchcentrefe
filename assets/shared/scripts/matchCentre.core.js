/**
* MatchCentre v1.0
* @module matchCentre
*/

var matchCentre = window.matchCentre || {};
matchCentre.configuration = {
	advertsEnabled: false,
	awayTeamId: null,
	awayTeamName: null,
	baseURL: null,
	colors: { // Used to pass into the SVG graphing. Specified here so that the API doesn't need to send presentational information
		black: '#000000',
		green: '#3c7f18',
		white: '#ffffff'
	},
	countdownTimer: { 
		days: null,
		enabled: false,
		hours: null,
		minutes: null,
		seconds: null
	},
	debugLogging: false,
	defaultGestureScrollingEnabled: true,
	fixtureId: null, 
	homeTeamId: null,
	homeTeamName: null,
	initialSiteContent: {},
	isMatchDetailPage: false, 
	loadSequence: null,
	loadSequences: {
		matchDayLive: [
			{id: 'fixtures', initialized: false},
			{id: 'leagueTable', initialized: false},
			{id: 'newsFeed', initialized: false}
		],
		matchDetail: [
			{id: 'fixtures', initialized: false},
			{id: 'leagueTable', initialized: false},
			{id: 'inSummary', initialized: false},
			{id: 'matchSummary', initialized: false},
			{id: 'newsFeed', initialized: false},
			{id: 'footerBar', initialized: false}
		]
	},
	localStorageNode: null,
	localStorageObj: null, 
	locked: false,
	omniture: {
		pageName: null,
		prop2: null,
		prop15: null,
		prop30: null
	},
	pageTitle: 'Mirror Match Centre',
	pageType: 'matchDayLive',
	pinTableToViewportTop: 122,
	runLocalDataFeeds: false,
	runMultipleWebsockets: true,
	shareThisUrlPrefix: 'http://api.addthis.com/oexchange/0.8/forward/',
	siteInitialized: false,
	siteLoaderEnabled: true,
	supportsLocalStorage: false,
	timings: {
		hideAddressBar: 201,
		siteLoaderFadeOut: 500
	},
	urls: { 
		live: {
			initialSiteContent: 'http://pulveriser-api-prod-m5pkaaqsig.elasticbeanstalk.com/matchCentre/main/'
		},
		local: {
			initialSiteContent: '/assets/shared/json/initialSiteContent.json',
			pusherSimulation: '/assets/shared/pusher/simulation.php',
			pusher2ndSimulation: '/assets/shared/pusher/simulation-2nd-events.php',
			pusher3rdSimulation: '/assets/shared/pusher/simulation-3rd-events.php'
		}	
	}
};

/**
* MatchCentre root class
* @class matchCentre
* @namespace core
*/
matchCentre.core = (function(){
	/** 
	* Initialize Match Centre
	* @method init
	* @param {INTEGER} awayTeamId The unique id of the away team (match detail page only)
	* @param {STRING} awayTeamName The name of the away team (match detail page only)
	* @param {STRING} baseURL The fully qualified base URL of where the site is running from
	* @param {BOOLEAN} debugLogging Whether debug logging is enabled or not
	* @param {STRING} deviceType Either 'mobile' || 'tablet' || 'desktop'
	* @param {INTEGER} fixtureId The unique id of the fixture (match detail page only)
	* @param {INTEGER} homeTeamId The unique id of the home team (match detail page only)
	* @param {STRING} homeTeamName The name of the home team (match detail page only)
	* @param {BOOLEAN} isMatchDetailPage Whether the page is a match detail page or not	
	* @param {STRING} omniture.pageName The page name value required for Omniture
	* @param {STRING} omniture.prop2 The prop2 value required for Omniture
	* @param {STRING} omniture.prop15 The prop15 value required for Omniture
	* @param {STRING} omniture.prop30 The prop30 value required for Omniture
	* @param {BOOLEAN} runLocalDataFeeds Whether local JSON files should be used for the feed updates	
	*/
	function init(obj){		
		// Enable debug logging if provided as part of initialization
		if(obj.debugLogging || matchCentre.configuration.debugLogging){
			matchCentre.configuration.debugLogging = true;
			
			$('BODY').addClass('debug');
			
			// DOM inject the debug logger
			createDebugLogger();
		}
		
		$.logEvent('[matchCentre.core.init]: ' + $.logJSONObj(obj));
		
		// Override the necessity to load site data via local JSON files, if an override has been provided
		if(obj.runLocalDataFeeds){
			matchCentre.configuration.runLocalDataFeeds = true;
			
			// Generate the debug mode dimensions layer
			generateModeDimensions();			
		}
		else{
			// If the site is running as 'live', always enable websockets, regardless of how it's been configured
			matchCentre.websockets.configuration.enabled = true;
		}
		
		// If the page type is a match detail page, store a reference to this, as it changes the load sequence to use
		if(obj.isMatchDetailPage){
			matchCentre.configuration.isMatchDetailPage = true;
			matchCentre.configuration.pageType = 'matchDetail';
			
			$('BODY').addClass('match-detail');
		}
		else{
			// For the all matches page, remove the footer bar from the DOM
			$('#footer-bar').hide();
		}
								
		// Update the global site configuration, taking values as specified within the HTML source-code
		matchCentre.configuration.deviceType = obj.deviceType;
		matchCentre.configuration.baseURL = obj.baseURL;
		matchCentre.configuration.fixtureId = obj.fixtureId;
		matchCentre.configuration.homeTeamId = obj.homeTeamId;
		matchCentre.configuration.homeTeamName = obj.homeTeamName;
		matchCentre.configuration.awayTeamId = obj.awayTeamId;
		matchCentre.configuration.awayTeamName = obj.awayTeamName;
		matchCentre.configuration.omniture.pageName = obj.omniture.pageName;
		matchCentre.configuration.omniture.prop2 = obj.omniture.prop2;

		// Only match detail pages pass this prop value for Omniture tracking
		if(obj.omniture.hasOwnProperty('prop15')){
			matchCentre.configuration.omniture.prop15 = obj.omniture.prop15;
		}
		
		matchCentre.configuration.omniture.prop30 = obj.omniture.prop30;
		matchCentre.configuration.pageTitle = $('TITLE').text();
			
		// Ensure that the dimensions/mode layer are updated on resize
		modeDimensionsRefresh();
		
		// Set up handlers for the social share links
		socialIconsInit();
				
		// Check to see whether local storage is available within the current browser
		if(supportsLocalStorage()){
			matchCentre.configuration.supportsLocalStorage = true;
			
			// If local storage is supported, define storage key names for subsequent usage
			localStorageInit();
		}
		
		// Pull initial site content from the API || local file system
		pullInitialSiteContent();
	};
	
	/**
	* Initialize the countdown timer
	* @method countdownTimerInit
	*/
	function countdownTimerInit(){
		$.logEvent('[matchCentre.core.countdownTimerInit]');
				
		if(!matchCentre.configuration.enabled){
			return;
		}
				
		if($('#countdown-timer').size() == 0){
			$('<div />')
				.append(
					$('<h4 />')
						.text('Countdown until kickoff')
				)
				.append(
					$('<div />')
						.attr('class','timer')
				)
				.attr('id','countdown-timer')
				.insertAfter($('#match-heading'));
		}
		else{
			$('#countdown-timer .timer').empty();
		}
		
		// Initialize the countdown timer
		$('#countdown-timer .timer').countdown({
			callback: function(days,hours,minutes,seconds){
				if(days === 0 && hours === 0 && minutes === 0 && seconds === 0){
					$('#countdown-timer').remove();
					matchCentre.configuration.enabled = false;
				}
				else{
					// Update the stored timers
					matchCentre.configuration.countdownTimer.days = days;
					matchCentre.configuration.countdownTimer.hours = hours;
					matchCentre.configuration.countdownTimer.minutes = minutes;
					matchCentre.configuration.countdownTimer.seconds = seconds;
				}
			},
			device: matchCentre.configuration.deviceType,
			duration: 360,
			timestamp:{
				days: matchCentre.configuration.countdownTimer.days,
				hours: matchCentre.configuration.countdownTimer.hours,
				minutes: matchCentre.configuration.countdownTimer.minutes,
				seconds: matchCentre.configuration.countdownTimer.seconds
			}			
		});
	}
	
	/**
	* Generate the debug mode dimensions layout
	* @method generateModeDimensions
	*/
	function generateModeDimensions(){
		$.logEvent('[matchCentre.core.generateModeDimensions]');
		
		$('<ul />')
			.append(
				$('<li />')
					.append(
						$('<strong />')
							.text('Mode: ')
					)
					.append(
						$('<span />')
							.text('Desktop')
					)
					.attr('id','mode')
			)
			.append(
				$('<li />')
					.append(
						$('<strong />')
							.text('Dimensions: ')
					)
					.append(
						$('<span />')
							.text('1920x1080')
					)
					.attr('id','dimensions')
			)
			.attr('id','mode-dimensions')
			.insertAfter($('#site-loader'));
	}
	
	/**
	* If local storage is supported, define storage key names for subsequent usage
	* @method localStorageInit
	*/
	function localStorageInit(){
		$.logEvent('[matchCentre.core.localStorageInit]');
				
		matchCentre.configuration.localStorageNode = matchCentre.configuration.pageType;
		
		// The local storage node is different for a match detail page, as it includes a suffix of the unique fixture id
		if(matchCentre.configuration.isMatchDetailPage){
			matchCentre.configuration.localStorageNode = 'matchDetail-' + matchCentre.configuration.fixtureId;			
		}
					
		// If local storage is supported, ensure that the necessary default storage is saved
		matchCentre.dropdownBar.localStorageInit();
	}
	
	/**
	* Initialize functionality for the active device
	* Unload all functionality for the non-active device
	* @method deviceInit
	* @param {String} deviceType 'mobile' || 'tablet' || 'desktop'
	*/
	function deviceInit(obj){
		$.logEvent('[matchCentre.core.deviceInit]: ' + $.logJSONObj(obj));

		// Upon every device switch, scroll the page to the top, and remove the 'pinnedToTop' classname
		$(window).scrollTo(0);
		$('BODY')
			.removeClass('pinnedToTop')
			.find('#header')
				.css('left',0);
				
		switch(obj.deviceType){
			case 'mobile':
				matchCentre.desktop.unload();
				matchCentre.tablet.unload();
				matchCentre.mobile.init();
				break;
			case 'tablet':
				matchCentre.desktop.unload();
				matchCentre.mobile.unload();
				matchCentre.tablet.init();
				break;
			case 'desktop':
				matchCentre.mobile.unload();
				matchCentre.tablet.unload();
				matchCentre.desktop.init();
				break;
		}
	}
	
	/**
	* Pull initial site content from the API || local file system
	* @method pullInitialSiteContent
	*/
	function pullInitialSiteContent(){
		$.logEvent('[matchCentre.core.pullInitialSiteContent]: local or live data? ' + (matchCentre.configuration.runLocalDataFeeds ? 'local' : 'live'));
		
		var fixtureId = matchCentre.configuration.fixtureId != null ? '/' + matchCentre.configuration.fixtureId : '';
		var dataType = 'json';
		var jsonUrl = matchCentre.configuration.urls.live.initialSiteContent + fixtureId;
				
		// Override the (live) defaults if local data is being used for simulation
		if(matchCentre.configuration.runLocalDataFeeds){
			jsonUrl = matchCentre.configuration.urls.local.initialSiteContent;
		}
		
		$.ajax({
			dataType: dataType,
			success: function(data){
				// Store namespaced references to retrieved data, whilst the dispatcher executes
				matchCentre.configuration.initialSiteContent = data.matchCentre;

				// Set the required load sequence, depending on the page type ('matchDayLive' OR 'matchDetail')
				matchCentre.configuration.loadSequence = matchCentre.configuration.loadSequences[matchCentre.configuration.isMatchDetailPage ? 'matchDetail' : 'matchDayLive'];
				
				// Once the content is retrieved, initialize the dispatcher, to sequentially load all DOM injection
				matchCentre.dispatcher.moduleInitialize({
					id: matchCentre.configuration.loadSequence[0].id
				});
			},
			type: 'get',
			url: jsonUrl
		});
	}
	
	/**
	* All functionality to load once the correct CSS and JavaScript has been attached to the DOM (via adaptJS)
	* @method postCSSLoadInit
	*/
	function postCSSLoadInit(){
		$.logEvent('[matchCentre.core.postCSSLoadInit]');
		
		// Initialize the dropdown bar
		matchCentre.dropdownBar.init();
		
		// Read the URL hashvalue, to scroll the page straight to the tweeted post
		if(matchCentre.newsFeed.configuration.embeddedTweetsTotal == 0){
			processURLHash();
		}
	}
	
	/**
	* Run any other functionality which is not part of the sequence loader, but should be executed as the last scripts of initialization
	* @method postSequenceFunctions
	*/
	function postSequenceFunctions(){
		$.logEvent('[matchCentre.core.postSequenceFunctions]');

		var additionalProperties = {
			pageName: matchCentre.configuration.omniture.pageName,
			prop2: matchCentre.configuration.omniture.prop2,
			prop30: matchCentre.configuration.omniture.prop30		
		};
		
		// Match detail pages also send an additional prop15 property
		if(matchCentre.configuration.omniture.prop15 != null){
			additionalProperties.prop15 = matchCentre.configuration.omniture.prop15;
		}

		// Send tracking request to Omniture			
		matchCentre.omniture.track({
			additionalProperties: additionalProperties, 
			trackingType: 'page-view'
		});
	}
		
	/**
	* Read the URL hashvalue, to scroll the page straight to the tweeted post
	* @method processURLHash
	*/
	function processURLHash(){
		$.logEvent('[matchCentre.core.processURLHash]');
		
		var hashValue = $.param.fragment()[0];
		
		if(hashValue != undefined){
			var matchingDomElement = $('#news-feed .update[data-update-id=' + hashValue + ']');
			
			// Scroll the page to the top of the associated news item
			if(matchingDomElement.size() == 1){
				// Take account of the header being fixed for both the desktop and tablet experience
				var offset = Math.floor(matchingDomElement.offset().top - (isDesktop() || isTablet() ? $('#fixtures').height() : 0));

				$.scrollTo({
					top: offset,
					left: 0
				},1);	
			}
		}		
	}
	
	/**
	* Initialize MPU advert
	* @method advertInit
	*/
	function advertInit(){
		$.logEvent('[matchCentre.core.advertInit]');
		
		$('#panel')
			.prepend(
				$('<div />')
					.attr({
						'class': 'module',
						id: 'mpu-advert'
					})
			)
	}
	
	/**
	* Set up handlers for the social share links
	* @method socialIconsInit
	*/
	function socialIconsInit(){
		$.logEvent('[matchCentre.core.socialIconsInit]');	
		// Create the event handler for the social share links
		var shareURLPrefix = '';
		
		$('#share-icons LI').each(function(){			
			switch($(this).attr('class')){
				case 'facebook':
					shareURLPrefix = 'facebook';					
					break;
				case 'twitter':
					shareURLPrefix = 'twitter';					
					break;
				case 'google':
					shareURLPrefix = 'google_plusone_share';					
					break;
			}
			
			// Attach the dynamic URL to the social page links
			$('>A',this).attr({
				href: matchCentre.configuration.shareThisUrlPrefix + shareURLPrefix + '/offer?title=' + matchCentre.configuration.pageTitle + '&url=' + matchCentre.configuration.baseURL + '/' + $.retrievePageURL() + $(location).attr('search'),
				target: '_blank'
			}).parent().on('click',function(e){				
				/*
				// Omniture seems to attach tracking to native <a> links, so there is no need to add an additional click handler to the social links
				// If it were to be needed, the code below would handle that functionality
				var dataType;
				var friendlyTitle = 'Match Centre Blog Top Link Click';
				
				switch($(this).attr('class')){
					case 'facebook':
						dataType = 'ico-fb';						
						break;						
					case 'twitter':
						dataType = 'ico-tw';						
						break;						
					case 'google':
						dataType = 'ico-g';						
						break;
				}
				
				/*
				// Send tracking request to Omniture
				matchCentre.omniture.track({
					additionalProperties: {
						dataAction: 'share:mirrorfootball',
						dataType: 'ico-fb',
						dataContext: 'mc-blog-top'
					}, 
					callee: this, 
					friendlyTitle: friendlyTitle,
					trackingType: 'event-click'
				});
				*/
			});
		});	
	}	
	
	/**
	* Refresh the MPU advertising banner
	* @method refreshAdvertisingBanner
	*/
	function refreshAdvertisingBanner(){
		$.logEvent('[matchCentre.core.refreshAdvertisingBanner]');
		
		googletag.pubads().refresh([matchCentre.configuration.mpuAdvert]);
	}
	
	/**
	* Disable (gesture-based) page scrolling except for internal scrollers (iScroll)
	* @method disableGestureScrolling
	*/
	function disableDefaultGestureScrolling(){
		$.logEvent('[matchCentre.core.disableDefaultGestureScrolling]');
		
		matchCentre.configuration.defaultGestureScrollingEnabled = false;
		
		$('BODY').on('touchmove',function(e){
			if($(e.target).hasParent('.internal-scroller') == 0){
				e.preventDefault();
			}
		});
	}
	
	/**
	* Enable (gesture-based) page scrolling
	* @method enableDefaultGestureScrolling
	*/
	function enableDefaultGestureScrolling(){
		$.logEvent('[matchCentre.core.enableDefaultGestureScrolling]');
		
		matchCentre.configuration.defaultGestureScrollingEnabled = true;
		
		$('BODY').off('touchmove');
	}

	/**
	* Check to see whether local storage is available within the current browser
	* @method supportsLocalStorage
	* @return {BOLLEAN} Whether or not local storage is supported or not
	*/
	function supportsLocalStorage(){
		try {
			$.logEvent('[matchCentre.core.supportsLocalStorage]: supported');
			
			return 'localStorage' in window && window['localStorage'] !== null;
		}
		catch(err){
			return false;
		}
	}	
	
	/**
	* DOM inject the debug logger
	* @method createDebugLogger
	*/
	function createDebugLogger(){		
		$('BODY')
			.prepend(
				$('<div />')
					.append(
						$('<h4 />')
							.text('Output log event')
							.append(
								$('<a />')
									.attr('href','#')
									.on('click',function(e){
										e.preventDefault();
										
										$('#logger .inner')
											.empty()
									})
									.text('(clear)')
							)
							.append(
								$('<a />')
									.attr('href','#')
									.on('click',function(e){
										e.preventDefault();
										
										$('#logger .inner')
											.css('height','100px')
									})
									.text('(regular size)')
							)
							.append(
								$('<a />')
									.attr('href','#')
									.on('click',function(e){
										e.preventDefault();
										
										$('#logger .inner')
											.css('height','400px')
									})
									.text('(small expand)')
							)
					)
					.append(
						$('<div />')
							.attr('class','inner')
					)
					.attr('id','logger')
			)
			
		$.logEvent('[matchCentre.core.createDebugLogger]');
	}
	
	/**
	* Ensure that the dimensions/mode layer are updated on resize
	* @method modeDimensionsRefresh
	*/
	function modeDimensionsRefresh(){	
		// Which mode does the current resolution fall into
		var currentWidth = $(window).width();
		var whichMode = null;
		
		if($.numberInRange({number:currentWidth, lower:0, upper: 767})){
			whichMode = 'mobile';
		}
		else if($.numberInRange({number:currentWidth, lower:768, upper: 1139})){
			whichMode = 'tablet';
		}
		else if($.numberInRange({number:currentWidth, lower:1140, upper: 9999})){
			whichMode = 'desktop';
		}
		
		$('#mode-dimensions #mode SPAN').html(whichMode);
		$('#mode-dimensions #dimensions SPAN').html($(window).width() + 'x' + $(window).height());
	}

	/**
	* Capitalize a sentence from xxx to Xxx
	* @method capitalizeSentence
	* @param {STRING} stringToFormat The string to capitalize
	* @return {STRING}
	*/
	function capitalizeSentence(stringToFormat){
		$.logEvent('[matchCentre.core.capitalizeSentence]');
		
		return stringToFormat.substr(0,1).toUpperCase() + stringToFormat.substr(1,stringToFormat.length-1).toLowerCase();
	}
		
	/**
	* Convert input to xxx-xxx-xxx
	* @method lowerCaseWithDashes
	* @param {STRING} stringToFormat The string to convert to lowercase and add replace spaces with dashes
	* @return {STRING}
	*/	
	function lowerCaseWithDashes(stringToFormat){
		$.logEvent('[matchCentre.core.lowerCaseWithDashes]');
		
		return stringToFormat.toLowerCase().replace(/ /g,'-');
	}

	/**
	* Is a number positive or not?
	* @method isPositiveNumber
	* @param {INTEGER} number The number to check whether it is positive or not
	* @return {INTEGER}
	*/
	function isPositiveNumber(number){		
		var isPositive = number > 0;
		
		$.logEvent('[matchCentre.core.isPositiveNumber]: ' + number + ' (' + isPositive + ')');
		
		return isPositive;
	}
	
	/**
	* Check to see whether the site is rendering in mobile mode
	* @method isMobile
	* @return {BOOLEAN}
	*/
	function isMobile(){
		return matchCentre.configuration.deviceType == 'mobile';
	}
	
	/**
	* Check to see whether the site is rendering in either tablet mode
	* @method isTablet
	* @return {BOOLEAN}
	*/
	function isTablet(){
		return matchCentre.configuration.deviceType == 'tablet';
	}
	
	/**
	* Check to see whether the site is rendering in desktop mode
	* @method isDesktop
	* @return {BOOLEAN}
	*/
	function isDesktop(){
		return matchCentre.configuration.deviceType == 'desktop';
	}
	
	/**
	* Lock/unlock the site
	* @method siteLock
	* @param {STRING} Method 'lock' or 'unlock'
	*/
	function siteLock(obj){
		$.logEvent('[matchCentre.core.siteLock]: ' + $.logJSONObj(obj));

		switch(obj.method){
			case 'lock':
				matchCentre.configuration.locked = true;
				$('BODY').addClass('locked');
				break;
			case 'unlock':
				matchCentre.configuration.locked = false;
				$('BODY').removeClass('locked');
				break;
		}
	}
		
	/**
	* Check to see if the site is currently in a locked/disabled state
	* @method isSiteLocked
	* @return {BOOLEAN}
	*/
	function isSiteLocked(){
		return $('BODY').hasClass('locked');
	}	
	
	return {
		advertInit: advertInit,
		capitalizeSentence: capitalizeSentence,
		countdownTimerInit: countdownTimerInit,
		deviceInit: deviceInit,
		disableDefaultGestureScrolling: disableDefaultGestureScrolling,
		enableDefaultGestureScrolling: enableDefaultGestureScrolling,
		init: init,
		isDesktop: isDesktop,
		isMobile: isMobile,
		isPositiveNumber: isPositiveNumber,
		isSiteLocked: isSiteLocked,
		isTablet: isTablet,
		lowerCaseWithDashes: lowerCaseWithDashes,
		modeDimensionsRefresh: modeDimensionsRefresh,
		postCSSLoadInit: postCSSLoadInit,
		postSequenceFunctions: postSequenceFunctions,
		processURLHash: processURLHash,
		refreshAdvertisingBanner: refreshAdvertisingBanner,
		siteLock: siteLock
	}
}());

// Window events
$(window).on({
	load: function(){
		// All functionality to load once the correct CSS and JanvaScript has been attached to the DOM (via adaptJS)
		matchCentre.core.postCSSLoadInit();
	},
	scroll: function(){
		if(!matchCentre.core.isMobile()){
			// Work out whether to pin the table of results to the top of the screen or not
			if($(window).scrollTop() > matchCentre.configuration.pinTableToViewportTop){
				$('BODY').addClass('pinnedToTop');
								
				$('#header').css({
					left: $('#page').offset().left,
					width: $('#page').width()
				});
			}
			else{
				$('BODY').removeClass('pinnedToTop');
				
				$('#header').css({
					left: 0,
					width: 'auto'
				});
			}
		}
	},
	resize: function(){
		// Reset the fixtures back to its initial load state
		matchCentre.fixtures.reset({deviceType: matchCentre.configuration.deviceType});
			
		if(matchCentre.core.isTablet()){			
			// Resize the fixtures (inner match) widths, for tablet devices
			matchCentre.fixtures.resize({deviceType: matchCentre.configuration.deviceType});
		}
		else if(matchCentre.core.isMobile()){
			// Resize the fixtures (inner match) widths, for mobile devices
			matchCentre.fixtures.resize({deviceType: 'mobile'});
		}
		
		// Ensure that the dimensions/mode layer are updated on resize
		matchCentre.core.modeDimensionsRefresh();
	}
});

// jQuery extensions
$.extend({
	/**
	* Logging, based on whether it has been configured to log or not
	* @method logEvent
	* @param {STRING} Event The event to log
	*/
	logEvent: function(event){
		if(matchCentre.configuration.debugLogging){
			$('#logger .inner')
				.prepend(
					$('<p />')
						.html(($('#logger .inner P').size()+1) + '. ' + event)
				)
		}
	},
	
	/**
	* Loop through an object
	* @method logJSONObj
	* @param {OBJECT} obj A variable JSON object to output to the console
	*/
	logJSONObj: function(obj){
		var debugJSON = '';
		var i;
		
		for(i in obj){
			if(obj.hasOwnProperty(i)){
				debugJSON += i + '=' + obj[i] + ', ';	
			}
		}
		return debugJSON.length > 0 ? debugJSON.substr(0,debugJSON.length-2) : '[empty parameter object]';
	},
	
	/**
	* Is a number between 2 numbers?
	* @method numberInRange
	* @param {INTEGER} number The number to check whether it is within the lower and upper bounds
	* @param {INTEGER} lower The lower number to check against
	* @param {INTEGER} upper The upper number to check against
	*/	
	numberInRange: function(obj){		
		return obj.number >= obj.lower && obj.number <= obj.upper;
	},
	
	/**
	* Retrieve just the page URL from the complete URL
	* @method retrievePageURL
	* @return {STRING} The page URL
	*/
	retrievePageURL: function(){
		return top.location.href.split('?')[0].split('/').slice(-1);
	}
});

$.fn.extend({
	IF: function(expr){
		return this.pushStack((this._ELSE = !($.isFunction(expr) ? expr.apply(this) : expr))? [] : this, 'IF', expr);
	},
	
	ELSE: function(expr){
		var $set = this.end();
		return $set.pushStack(((!$set._ELSE) || ($set._ELSE = ((typeof(expr) !== 'undefined') && (!($.isFunction(expr) ? expr.apply($set) : expr)))))? [] : $set, 'ELSE', expr);
	},

	ENDIF: function(){
		return this.end();
	},
	
	hasAttr: function(name){  
		return this.attr(name) !== undefined;
	},
	
	hasParent: function(filterCriteria){
		return this.parents(filterCriteria).length;
	}
});