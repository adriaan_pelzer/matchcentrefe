(function(jQuery){
	var barChartConfiguration = {
		axisTitleColor: '#fff',
		balloonEnabled: false,
		fillAlpha: 1,
		gridAlpha: 0.5,
		gridColor: '#fff',
		gridPosition: 'start',
		lineAlpha: 0,
		textColor: '#fff'
	}

	var methods = {		
		/**
		* Build the chart dynamically
		* @method build
		* @param {OBJECT} data The chart data
		* @param {OBJECT} domElementId The unique guid of the update
		* @param {OBJECT} metaData Other data required by the chart, but outside of the actual chart (numeric) data
		*/
		build: function(obj){
			var selfObj = $(this);
						
			// Serial chart
			var chartObj = new AmCharts.AmSerialChart();
			chartObj.dataProvider = obj.data;
			chartObj.categoryField = 'labelText';
			chartObj.rotate = true;
			chartObj.balloon.enabled = barChartConfiguration.balloonEnabled;

			// y-Axis
			var yAxis = chartObj.categoryAxis;
			yAxis.titleColor = barChartConfiguration.axisTitleColor;
			yAxis.color = barChartConfiguration.textColor;
			yAxis.labelRotation = 90;
			yAxis.axisColor = barChartConfiguration.gridColor;
			yAxis.gridAlpha = barChartConfiguration.gridAlpha;
			yAxis.gridColor = barChartConfiguration.gridColor;			
			yAxis.gridPosition = barChartConfiguration.gridPosition;
			yAxis.labelsEnabled = false;
			yAxis.tickLength = 0;

			// x-Axis
			var xAxis = new AmCharts.ValueAxis();
			xAxis.title = obj.metaData.xAxisTitle;
			xAxis.titleColor = barChartConfiguration.axisTitleColor;
			xAxis.color = barChartConfiguration.textColor;
			xAxis.axisColor = barChartConfiguration.gridColor;
			xAxis.gridAlpha = barChartConfiguration.gridAlpha;
			xAxis.gridColor = barChartConfiguration.gridColor;
			xAxis.maximum = 100;			
			xAxis.minimum = 0;

			chartObj.addValueAxis(xAxis);

			// Graph-specific settings
			var graphObj = new AmCharts.AmGraph();
			graphObj.colorField = 'color';
			graphObj.valueField = 'value';
			graphObj.balloonText = '[[category]]: [[value]]';
			graphObj.type = 'column';
			graphObj.lineAlpha = barChartConfiguration.lineAlpha;
			graphObj.fillAlphas = barChartConfiguration.fillAlpha;
			
			// Add the graph to the chart object
			chartObj.addGraph(graphObj);		
			
			var chartDomObj = $('<div />')
				.append(
					$('<ul />')
						.append(
							$('<li />')
								.append(
									$('<span />')
										.html(obj.data[0].labelText)
								)
						)
						.append(
							$('<li />')
								.append(
									$('<span />')
										.html(obj.data[1].labelText)
								)
						)
				)
				.append(
					$('<div />')
						.attr({
							'class': 'chart-inner',
							id: 'bar-chart-' + obj.domElementId + '-inner'
						})
				)
				.attr('class','chart');
				
			selfObj.append(chartDomObj);
			
			// Create the chart
			setTimeout(function(){				
				chartObj.write($('.chart-inner',chartDomObj).attr('id'));
				
				// Store a reference to the chart, in order to call re-size functions when switching devices
				matchCentre.newsFeed.configuration.embeddedChartObjs.push(chartObj);
			},1);
		}		
	};
	
	// Initialize plugin
	$.fn.barChart = function(obj){
		// Method calling logic
		if(methods[obj]){
			return methods[obj].apply(this,Array.prototype.slice.call(arguments,1));
		} 
		else if(typeof obj === 'object' || ! obj){
			return methods.init.apply(this,arguments);
		}
	};
}(jQuery));