	/**
* MatchCentre v1.0
* @module matchCentre
*/

var matchCentre = window.matchCentre || {};

/**
* MatchCentre root class
* @class matchCentre
* @namespace matchCentre.footerBar.core
*/

matchCentre.footerBar = {
	matchPreview: {},
	matchStats: {}
};

matchCentre.footerBar.core = (function(){
	var configuration = {		
		bottomSpacerForTabContent: 20,
		expandCollapseEasing: 'easeInOutExpo', 
		expandedHeightInPercentage: 40, 
		expandedHeightInPixels: null, 
		footerBarDomObj: $('#footer-bar'),
		isVisible: false, 
		jsonContent: null,
		loadSequence: [
			{id: 'footerBar.lineUps', initialized: false},
			// {id: 'footerBar.matchPreview.last6Matches', initialized: false},
			// {id: 'footerBar.matchPreview.previousMeetings', initialized: false},
			// {id: 'footerBar.matchPreview.topScorers', initialized: false},
			// {id: 'footerBar.matchPreview.next3Matches', initialized: false},
			{id: 'footerBar.matchStats.attack', initialized: false},
			{id: 'footerBar.matchStats.generalPlay', initialized: false},
			{id: 'footerBar.matchStats.distribution', initialized: false},
			{id: 'footerBar.matchStats.defenceAndDiscipline', initialized: false},
			{id: 'footerBar.facts', initialized: false}
		],
		selectedIndex: null,
		showLockingOverlay: true,
		tabTriggersObj: null,
		tabType: null,
		timings: {
			fadeInOutTabContent: 250, 
			loadTimeDeferal: 1,
			showHide: 1000 
		},
		toggleWrapperDomObj: null,
		topSpacerForSecondaryTabBlock: 45, 
		urls: {
			live: 'http://54.217.155.2:443/matchCentre/footerBar',
			local: '/assets/shared/json/footerBar.json'
		},
		websocketCallbackPaths: {
			attack: 'footerBar.matchStats.attack',
			defenceAndDiscipline: 'footerBar.matchStats.defenceAndDiscipline',
			distribution: 'footerBar.matchStats.distribution',
			generalPlay: 'footerBar.matchStats.generalPlay',
			lineUps: 'footerBar.lineUps'
		},
		websocketChannels: ['lineUps','attack','generalPlay','distribution','defenceAndDiscipline']
	};
	
	/**
	* Initialize the Footer Bar
	* @method init
	*/
	function init(){
		$.logEvent('[matchCentre.footerBar.core.init]');
		
		// Flatten the tabs into their actual layout, with tab event handlers assigned
		initializeTabs();
	}
	
	/**
	* Initialize the required functionality for the desired module
	* @method moduleInitialize
	* @param {STRING} id The type of component, mapped to the plugin name used to build/initialize it
	*/
	function moduleInitialize(obj){
		$.logEvent('[matchCentre.footerBar.core.moduleInitialize]: ' + $.logJSONObj(obj));
		
		switch(obj.id){
			case 'footerBar.lineUps':
				matchCentre.footerBar.lineUps.build();
				break;
			case 'footerBar.matchPreview.last6Matches':
				matchCentre.footerBar.matchPreview.last6Matches.build();
				break;
			case 'footerBar.matchPreview.previousMeetings':
				matchCentre.footerBar.matchPreview.previousMeetings.build();
				break;
			case 'footerBar.matchPreview.topScorers':
				matchCentre.footerBar.matchPreview.topScorers.build();
				break;
			case 'footerBar.matchPreview.next3Matches':
				matchCentre.footerBar.matchPreview.next3Matches.build();
				break;
			case 'footerBar.matchStats.attack':
				matchCentre.footerBar.matchStats.attack.build();
				break;
			case 'footerBar.matchStats.generalPlay':
				matchCentre.footerBar.matchStats.generalPlay.build();
				break;
			case 'footerBar.matchStats.distribution':
				matchCentre.footerBar.matchStats.distribution.build();
				break;
			case 'footerBar.matchStats.defenceAndDiscipline':
				matchCentre.footerBar.matchStats.defenceAndDiscipline.build();
				break;
			case 'footerBar.facts':
				matchCentre.footerBar.facts.build();
				break;
		}
	}	
	
	/**
	* Notify dispatcher that a component has finished initializing
	* @method initializeComplete
	* @param (STRING} finishedId The DOM element id of the module which has finished initializing
	*/
	function initializeComplete(obj){
		$.logEvent('[matchCentre.footerBar.core.initializeComplete]: ' + $.logJSONObj(obj));
		
		var matchedSequencePosition;
				
		for(i=0; i<configuration.loadSequence.length; i++){
			// Locate the component which just finished initializing
			if(configuration.loadSequence[i].id == obj.finishedId){
				matchedSequencePosition = i;
				
				// Update the configuration to confirm that the passed component id has finishined initializing
				configuration.loadSequence[matchedSequencePosition].initialized = true;
				
				break;
			}
		}
		
		// Check to see if there are any more components left to initialize
		if(configuration.loadSequence.length > (matchedSequencePosition+1)){
			var newComponentToInitializeObj = configuration.loadSequence[matchedSequencePosition+1];

			setTimeout(function(){
				// Initialize the required functionality for the desired module
				moduleInitialize(newComponentToInitializeObj);			
			},configuration.timings.loadTimeDeferal);
		}
		else{		
			// All actions within the load sequence are now complete, notify the global site dispatcher that the footerBar is now complete
			$.logEvent('[matchCentre.footerBar.core.initializeComplete]: footerBar dispatcher is complete');

			// Re-attach the selectivizr plugin to the DOM (IE8 and lower) upon each opening of the footer
			if(document.addEventListener === undefined){
				$('#selectivizr').remove();
				
				$('<script />')
					.attr({
						id: 'selectivizr',
						src: '/assets/shared/scripts/selectivizr-min.js'
					})
					.appendTo($('HEAD'))
			}
			
			// Only expand the footer bar if the site is being viewed in desktop mode
			// Work out what percentage of the screen to show, and convert that to pixels
			configuration.expandedHeightInPixels = Math.floor(($(window).height()/100) * configuration.expandedHeightInPercentage);

			// Set the heights for the root tabs (Line Ups, Facts)
			$('#footer-bar-container')
				.children('DIV')
					.css('height',configuration.expandedHeightInPixels-configuration.bottomSpacerForTabContent)
					.children('DIV .inner')
						.css('height',configuration.expandedHeightInPixels-(configuration.bottomSpacerForTabContent*2))
						.jScrollPane({
							verticalGutter: 20
						});
				
			// Set the heights for the tab internal tabs (Match Preview, Match Stats)
			$('#footer-bar-container .tabs-internal')
				.css('height',configuration.expandedHeightInPixels-(configuration.bottomSpacerForTabContent*2))
				.children('.tab-content')
					.css('height',configuration.expandedHeightInPixels-(configuration.bottomSpacerForTabContent*2)-configuration.topSpacerForSecondaryTabBlock)
					.children('.inner')
						.css('height',configuration.expandedHeightInPixels-(configuration.bottomSpacerForTabContent*2)-configuration.topSpacerForSecondaryTabBlock-20)
						.jScrollPane({
							verticalGutter: 20
						});
							
			// Toggle the visibility of the tab content blocks (on the root node of the tabs), based on the element clicked by the user
			toggleTabContentVisibility();
				
			// Switch the active state of the selected/unselected tab trigger elements, so that the loading icon is removed and the active style is applied
			$('.tab-triggers:eq(0) LI:eq(' + configuration.selectedIndex + ')',configuration.footerBarDomObj)
				.attr('class','active');

			// Bring the footer bar inner content into view
			$('#footer-bar-container')
				.animate({
					height: configuration.expandedHeightInPixels
				},{
					complete: function(){
						configuration.isVisible = true;
													
						// Fade in the "Close" button
						$('.close',configuration.footerBarDomObj).fadeIn(configuration.timings.showHide);
					},
					duration: configuration.timings.showHide,
					easing: configuration.expandCollapseEasing
				});
			
			// For both tablet and mobile devices, once the footer bar is initialized, allow the 'Stats' dropdown bar group to expand open
			if(matchCentre.core.isTablet() || matchCentre.core.isMobile()){
				var containerStatsObj = $('#container-stats');
					
				containerStatsObj.css({
						height: parseInt(containerStatsObj.attr('data-original-height')),
						WebkitTransition: 'height ' + matchCentre.dropdownBar.configuration.timings.toggleVertical + 'ms ease-in-out',
						transition: 'height ' + matchCentre.dropdownBar.configuration.timings.toggleVertical + 'ms ease-in-out'
					});
					
				setTimeout(function(){
					// To allow the component moving in from the side to overflow correctly, add overflow visibility
					containerStatsObj.css('overflow','visible');
				
					// Add the active state to the selected item, and remove from all siblings
					$('>UL LI#stats',matchCentre.dropdownBar.configuration.dropdownBarObj)
						.removeClass('loading')
						.addClass('active')
						.siblings()
							.removeClass('active');
					
					matchCentre.dropdownBar.configuration.expanded = true;
				},matchCentre.dropdownBar.configuration.timings.toggleVertical);
			}
			
			// Activate all websocket channels within the footer bar
			activateWebsockets();
			
			// Whilst the footer bar is being expanded, fade in the footer bar overlay
			if(configuration.showLockingOverlay){
				$('<div />')
					.attr('id','footer-bar-overlay')
					.prependTo($('BODY'))
						.fadeIn(configuration.timings.showHide);
			}
		}
	}
		
	/**
	* Flatten the tabs into their actual layout, with tab event handlers assigned
	* @method initializeTabs
	*/
	function initializeTabs(){
		$.logEvent('[matchCentre.footerBar.core.initializeTabs]');
		
		var linkObj;
		
		$('.tab-triggers LI A',configuration.footerBarDomObj).on('click',function(e){
			e.preventDefault();

			linkObj = $(this);
			configuration.tabTriggersObj = linkObj.parents('.tab-triggers');
			
			// Don't process the handler event if the current link is already active
			if(linkObj.parent().hasClass('active')){
				return;
			}
						
			// Show the requested tab and hide all other siblings
			configuration.selectedIndex = $('LI',configuration.tabTriggersObj).index(linkObj.parent());
			
			// Store the tab type
			configuration.tabType = $('LI:eq(' + configuration.selectedIndex + ')',configuration.tabTriggersObj).hasParent('#footer-bar-container') == 1 ? 'internal' : 'root';
			
			// If the footer bar is currently visible, toggle the visibility of the tab content blocks
			if(configuration.isVisible){
				// Switch the active state of the selected/unselected tab trigger elements
				$('LI:eq(' + configuration.selectedIndex + ')',configuration.tabTriggersObj)
					.attr('class','active')
					.siblings()
						.removeClass('active');
						
				// Toggle the visibility of the tab content blocks
				toggleTabContentVisibility();
			}
			else{
				var activeTabObj = $('LI:eq(' + configuration.selectedIndex + ')',configuration.tabTriggersObj);
				var tabTitle = activeTabObj.text();
				var tabTrackingTitle = tabTitle.toLowerCase().replace(/ /g,'-');
	
				// Send tracking request to Omniture
				matchCentre.omniture.track({
					additionalProperties: {
						dataAction: 'show:' + tabTrackingTitle,
						dataType: 'tab:' + tabTrackingTitle,
						dataContext: 'mc-footer-bar'
					}, 
					callee: this, 
					friendlyTitle: 'Footer bar tab click ' + tabTitle,
					trackingType: 'event-click'
				});

				// Add the loading icon to the clicked root tab
				activeTabObj.attr('class','loading')
					
				// Show/hide the footer bar
				showHideFooterBar({mode: 'show'});
			}
		});
		
		// Initialize the close functionality for the main footer bar
		$('.close',configuration.footerBarDomObj).on('click',function(e){
			e.preventDefault();
			
			// Show/hide the footer bar
			showHideFooterBar({
				immediateClose: false,
				mode: 'hide'
			});
		});
		
		// Notify the main site dispatcher that the footer bar has finished initializing
		matchCentre.dispatcher.initializeComplete({finishedId: 'footerBar'});
	}
	
	/**
	* Toggle the visibility of the tab content blocks
	* @method toggleTabContentVisibility
	* @param {OBJECT} initialToggle Whether or not this toggle was part of the initial show/hide animation. If it is, it needs to run an immediate fadeOut effect
	*/
	function toggleTabContentVisibility(obj){
		$.logEvent('[matchCentre.footerBar.core.toggleTabContentVisibility]');
	
		var internalTabsObj;
		var tabsContentDomObj = $('#footer-bar-container>.tab-content:eq(' + configuration.selectedIndex + ')');
		var activeTabId = null;
		
		switch(configuration.tabType){
			case 'root':
				$('#footer-bar-container',configuration.footerBarDomObj)
					.children('.tab-content:eq(' + configuration.selectedIndex + ')')
						.css('zIndex',1)
						.show()
					.siblings('.tab-content')
						.css('zIndex',2)
						.fadeOut(!configuration.isVisible ? 0 : configuration.timings.fadeInOutTabContent)
					.end();

				// Check to see whether the selected root tab element contains internal tabs inside it
				internalTabsObj = $('>.tabs-internal',tabsContentDomObj);
				var hasInternalTabs = internalTabsObj.size() == 1;
		
				// For a trigger called within an internal tab block, ensure that if there is no current selection, the first tab is auto-selected
				if(hasInternalTabs){
					// If there are currently no internal tabs selected, auto-highlight the first
					if($('LI.active',internalTabsObj).size() == 0){
						
						internalTabsObj
							// Highlight the first element in the internal tab triggers
							.find('.tab-triggers LI:eq(0)')							
								.addClass('active')
							.end()
							.children('.tab-content:eq(0)')
								.show()
								.siblings('.tab-content')
									.hide();
					}
				}
	
				break;
			case 'internal':
				internalTabsObj = configuration.tabTriggersObj.parent();
			
				var tabsInternalContentDomObj;
				tabsInternalContentDomObj = internalTabsObj.find('.tab-content:eq(' + configuration.selectedIndex + ')');
			
				tabsInternalContentDomObj
					.css('zIndex',1)
					.show()
					.siblings('.tab-content')
						.css('zIndex',2)
						.fadeOut(!configuration.isVisible ? 0 : configuration.timings.fadeInOutTabContent);
						
				break;
		}
	}
		
	/**
	* Activate all websocket channels within the footer bar
	* @method activateWebsockets
	*/
	function activateWebsockets(){
		$.logEvent('[matchCentre.footerBar.core.activateWebsockets]');

		// Only subscribe to websockets if configured to do so
		if(matchCentre.websockets.configuration.enabled && matchCentre.dropdownBar.configuration[matchCentre.configuration.pageType].toggleSwitches.settings['auto-updates']){
			$.each(configuration.websocketChannels,function(index,channelName){
				matchCentre.websockets.channelSubscribe({
					callbackOrigin: configuration.websocketCallbackPaths[channelName], // For all root-namespaced components, the callback origin and the componentId are the same. For footerBar, they are different
					componentId: channelName
				})
			});	
		}
	}
			
	/**
	* Unsubscribe all websocket channels from the footer bar
	* @method resetWebsocketListeners
	*/
	function resetWebsockets(){
		$.logEvent('[matchCentre.footerBar.core.resetWebsockets]');
		
		// Only unsubscribe from websockets if they're configured as active, otherwise they wouldn't have been subscribed to on opening the footer bar
		if(matchCentre.websockets.configuration.enabled && matchCentre.dropdownBar.configuration[matchCentre.configuration.pageType].toggleSwitches.settings['auto-updates']){
			$.each(configuration.websocketChannels,function(index,channelName){
				matchCentre.websockets.channelUnsubscribe({
					channelFullName: matchCentre.configuration.initialSiteContent.configuration.pusher.channelName + '.' + matchCentre.configuration.fixtureId + '.' + channelName
				});
			});
		}
	}
		
	/**
	* Show/hide the footer bar
	* @method showHideFooterBar
	* @param {BOOLEAN} immediateClose Whether to hide the footer bar immediately, rather than animate it over a longer time
	* @param {STRING} mode Whether to show or hide the footer bar	
	*/
	function showHideFooterBar(obj){
		$.logEvent('[matchCentre.footerBar.core.showHideFooterBar]: ' + $.logJSONObj(obj));
						
		switch(obj.mode){
			case 'show':				
				// Retrieve the JSON to populate the footer bar
				$.ajax({
					dataType: 'json',
					success: function(data){
						// Store namespaced references to retrieved data, whilst the dispatcher executes
						configuration.jsonContent = data.matchCentre.footerBar;
						
						// Once the content is retrieved, initialize the footerBar dispatcher, to sequentially load all DOM injection
						moduleInitialize({
							id: configuration.loadSequence[0].id
						});
					},
					type: 'get',
					url: matchCentre.configuration.runLocalDataFeeds ? configuration.urls.local : configuration.urls.live + '/' + matchCentre.configuration.fixtureId
				});
				
				break;
			case 'hide':
				// Unsubscribe all websocket channels from the footer bar
				if(configuration.isVisible){
					resetWebsockets();
				}

				var toggleSpeed = obj.immediateClose ? 1 : configuration.timings.showHide;
			
				// Hide the footer bar from view
				$('#footer-bar-container')
					.animate({
						height: 0
					},{
						complete: function(){
							configuration.isVisible = false;
							
							// Remove all active states in the first tab triggers set of links
							$('.tab-triggers:first LI',configuration.footerBarDomObj).removeClass('active');
							
							// Unload footer bar
							unload();
						},
						duration: toggleSpeed,
						easing: configuration.expandCollapseEasing
					});
					
				// Whilst the footer bar is being contracted, fade out the footer bar overlay
				if(configuration.showLockingOverlay){
					$('#footer-bar-overlay')
						.fadeOut(configuration.timings.showHide,function(){
							$(this).remove();
						});
				}
					
				// Fade out the "Close" button
				$('.close',configuration.footerBarDomObj).fadeOut(configuration.timings.showHide);
				
				break;
		}
	}
	
	/**
	* Unload footer bar
	* @method unload
	*/
	function unload(){
		$.logEvent('[matchCentre.footerBar.core.unload]');
		
		// Remove all inline calculated heights for the purpose of vertical scrolling within the footer bar
		removeCalculatedHeights();
		
		// Reset the viewstate of the tab content container DOM elements, despite the fact that their inner content is removed and rebuilt upon every expand of the footer bar
		$('.tabs-internal')
			.find('.tab-triggers>LI')
				.removeClass('active')
			.end()
			.removeAttr('style')
			.children('.tab-content')
				.removeAttr('style')
				.empty()
				.parents('.tab-content')
					.removeAttr('style')
					.siblings('DIV[data-flyout]')
						.removeAttr('style')
						.empty();
	}
		
	/**
	* Reset the visibility of the footer bar
	* @method reset
	*/
	function reset(obj){
		$.logEvent('[matchCentre.footerBar.core.reset]');

		if(configuration.isVisible){
			// Show/hide the footer bar
			showHideFooterBar({
				immediateClose: true,
				mode: 'hide'
			});
			
			// Don't wait for the animation of the footer bar closing before setting its closed state to true. This will help ensure this reset function isn't executed for both the inactive devices resetting
			configuration.isVisible = false;
		}
	}
	
	/**
	* Remove all inline calculated heights for the purpose of vertical scrolling within the footer bar (desktop treatment only)
	* @method removeCalculatedHeights
	*/
	function removeCalculatedHeights(){
		$.logEvent('[matchCentre.footerBar.core.removeCalculatedHeights]');
		
		$('.inner',configuration.footerBarDomObj).css('height','');
	}	
	
	return {
		configuration: configuration,
		init: init,
		initializeComplete: initializeComplete,
		reset: reset,
		showHideFooterBar: showHideFooterBar,
		unload: unload
	}
}());

/**
* @namespace matchCentre.footerBar.lineUps
*/

matchCentre.footerBar.lineUps = (function(){
	var configuration = {};
	
	/**
	* Build Footer Bar (Line Ups) in response to the initial pulled JSON content
	* @method build
	*/
	function build(){
		$.logEvent('[matchCentre.footerBar.lineUps.build]');

		// Store locally the relevant JSON content from the retrieved footerBar JSON
		var jsonData = matchCentre.footerBar.core.configuration.jsonContent.lineUps;
		
		var wrapperDomObj = $('.line-ups',matchCentre.footerBar.core.configuration.footerDomObj);
		
		// Capture any error should the content for the Line Ups not be correct
		try{
			wrapperDomObj
				.empty()
				.append(
					$('<h3 />')
						.append(
							$('<strong />')
								.text(wrapperDomObj.attr('data-flyout-title'))
						)
				)
				.append(
					$('<div />')
						.append(
							$('<ul />')
								.append(
									$('<li />')
										.append(
											$('<div />')
												.append(
													$('<h4 />')
														.attr('class',matchCentre.core.lowerCaseWithDashes(jsonData.content.homeTeam.name))
														.text('Line Ups for:')
												)
												.append(
													$('<strong />')
														.text(jsonData.content.homeTeam.name)
												)
												.attr('class','badges-header')
										)
										.append(function(){
											var startingDomObj = $('<ol />');
											
											// Loop through all starting players
											$.each(jsonData.content.homeTeam.players.starting,function(index,playerObj){
												startingDomObj
													.append(
														$('<li />')
															.append(
																$('<span />')
																	.text(playerObj.name)
															)
															.IF(playerObj.eventUpdate.length > 0)
																.append(attachEventToPlayer({events: playerObj.eventUpdate}))
															.ENDIF()
															.attr('data-player-id',playerObj.id)
													)
											});
											
											return startingDomObj;
										})
										.append(
											$('<h4 />')
												.attr('class','substitutions')
												.text('Substitutions')
										)
										.append(function(){
											var substitutionsDomObj = $('<ol />');
											
											// Loop through all substitutions
											$.each(jsonData.content.homeTeam.players.substitutions,function(index,playerObj){
												substitutionsDomObj
													.append(
														$('<li />')
															.append(
																$('<span />')
																	.text(playerObj.name)
															)
															.IF(playerObj.eventUpdate.length > 0)
																.append(attachEventToPlayer({events: playerObj.eventUpdate}))
															.ENDIF()
															.attr('data-player-id',playerObj.id)
													)
											});
											
											return substitutionsDomObj;
										})
										.attr('class','home')
								)
								.append(
									$('<li />')
										.append(
											$('<div />')
												.append(
													$('<h4 />')
														.attr('class',matchCentre.core.lowerCaseWithDashes(jsonData.content.awayTeam.name))
														.text('Line Ups for:')
												)
												.append(
													$('<strong />')
														.text(jsonData.content.awayTeam.name)
												)
												.attr('class','badges-header')
										)
										.append(function(){
											var startingDomObj = $('<ol />');
											
											// Loop through all starting players
											$.each(jsonData.content.awayTeam.players.starting,function(index,playerObj){
												startingDomObj
													.append(
														$('<li />')
															.append(
																$('<span />')
																	.text(playerObj.name)
															)
															.IF(playerObj.eventUpdate.length > 0)
																.append(attachEventToPlayer({events: playerObj.eventUpdate}))
															.ENDIF()
															.attr('data-player-id',playerObj.id)
													)
											});
											
											return startingDomObj;
										})
										.append(
											$('<h4 />')
												.attr('class','substitutions')
												.text('Substitutions')
										)
										.append(function(){
											var substitutionsDomObj = $('<ol />');
											
											// Loop through all substitutions
											$.each(jsonData.content.awayTeam.players.substitutions,function(index,playerObj){
												substitutionsDomObj
													.append(
														$('<li />')
															.append(
																$('<span />')
																	.text(playerObj.name)
															)
															.IF(playerObj.eventUpdate.length > 0)
																.append(attachEventToPlayer({events: playerObj.eventUpdate}))
															.ENDIF()
															.attr('data-player-id',playerObj.id)
													)
											});
											
											return substitutionsDomObj;
										})
										.attr('class','away')
								)
						)
						.attr('class','inner')
				)
			
			// Notify dispatcher that a component has finished initializing
			matchCentre.footerBar.core.initializeComplete({
				finishedId: 'footerBar.lineUps'
			});
		}
		catch(err){
			$.logEvent('[matchCentre.footerBar.lineUps.build]: an error occured populating the Line Ups component: ' + $.logJSONObj(err));
			
			// Notify dispatcher that a component has finished initializing
			matchCentre.footerBar.core.initializeComplete({
				finishedId: 'footerBar.lineUps'
			});
		}
	}
	
	/** 
	* Add an array of events to a single player
	* @method attachEventToPlayer
	* @param {ARRAY} events The list of events which belong to a player
	* @return {OBJECT} The DOM object to be attached to the current player
	*/
	function attachEventToPlayer(obj){
		$.logEvent('[matchCentre.footerBar.lineUps.attachEventToPlayer]: ' + $.logJSONObj(obj));
		
		var playerEventsDomObj = $('<ul />');
		
		$.each(obj.events,function(index,eventName){
			playerEventsDomObj
				.append(
					$('<li />')
						.attr('class',eventName)
						.text(matchCentre.core.capitalizeSentence(eventName))
				)
		});
		
		return playerEventsDomObj;
	}
	
	/**
	* Process the Pusher real-time update
	* @method websocketCallback
	* @param {OBJECT} data A JSON update from Pusher
	*/	
	function websocketCallback(obj){
		$.logEvent('[matchCentre.footerBar.lineUps.websocketCallback]: ' + $.logJSONObj(obj));
			
		// Capture any errors should the content for Line Ups not be correct
		try{
			var dataObj;
			
			$.each(obj.data.lineUps.content.players,function(index,dataObj){
				dataObj = this;
				
				$('.line-ups LI[data-player-id=' + dataObj.id + ']')
					.find('UL')
						.remove()
					.end()
					.append(attachEventToPlayer({events: dataObj.eventUpdate}))
			});
		}
		catch(err){
			$.logEvent('[matchCentre.footerBar.lineUps.websocketCallback]: an error occured populating the Line Ups (footerBar) component (via Pusher): ' + $.logJSONObj(err));
		}
	}
	
	return {
		build: build,
		configuration: configuration,
		websocketCallback: websocketCallback
	}	
}());

/**
* @namespace matchCentre.footerBar.matchPreview.last6Matches
*/

matchCentre.footerBar.matchPreview.last6Matches = (function(){
	var configuration = {};
	
	/**
	* Build Footer Bar (Match Preview/Last 6 Matches) in response to the initial pulled JSON content
	* @method build
	*/
	function build(){
		$.logEvent('[matchCentre.footerBar.matchPreview.last6Matches.build]');

		// Store locally the relevant JSON content from the retrieved footerBar JSON
		var jsonData = matchCentre.footerBar.core.configuration.jsonContent.matchPreview.last6Matches;

		var wrapperDomObj = $('.last-6-matches',matchCentre.footerBar.core.configuration.footerDomObj);

		wrapperDomObj
			.empty()
			.append(
				$('<h3 />')
					.append(
						$('<strong />')
							.text(wrapperDomObj.attr('data-flyout-title'))
					)
			)
			.append(
				$('<div />')
					.append(
						$('<div />')
							.append(
								$('<div />')
									.append(
										$('<h4 />')	
											.attr('class',matchCentre.core.lowerCaseWithDashes(jsonData.metaData.homeTeamName))
											.text('Last matches played for: ')
									)
									.append(
										$('<strong />')
											.text(jsonData.metaData.homeTeamName)
									)
									.attr('class','badges-header')
							)
							.append(
								$('<ol />')
									.append(function(){
										var matchesObj = $(this);
							
										$.each(jsonData.content.homeTeamMatches,function(index,matchObj){
											var matchDomObj = $('<li />');
																						
											// Add the date of the match
											var matchDateObj = $('<span />').text(matchObj.date);
											matchDomObj.prepend(matchDateObj);
											
											// Add the match details, score, goalscorers
											$('<table />')
												.append(
													$('<thead />')
														.append(
															$('<tr />')
																.append($('<th />').text('Home team'))
																.append($('<th />').text('Score'))
																.append($('<th />').text('Away team'))
														)
												)
												.append(
													$('<tbody />')
														.append(
															$('<tr />')
																.append(
																	$('<td />')
																		.text(matchObj.home.teamName)
																		.IF(matchObj.home.goalScorers.length > 0)
																			.append(attachGoalScorersToMatch({goalScorers: matchObj.home.goalScorers}))
																		.ENDIF()
																)
																.append(
																	$('<td />')
																		.text(matchObj.score)
																)
																.append(
																	$('<td />')
																		.text(matchObj.away.teamName)
																		.IF(matchObj.home.goalScorers.length > 0)
																			.append(attachGoalScorersToMatch({goalScorers: matchObj.away.goalScorers}))
																		.ENDIF()
																)
														)
												)
												.attr('border','1')
												.insertAfter(matchDateObj);
												
											// Attach each individual match to the overall homeTeamMatches DOM object
											matchesObj.append(matchDomObj);
										})
										
										return matchesObj;
									})
									.attr('class','match-listing')
							)
							.attr('class','home')
					)
					.append(
						$('<div />')
							.append(
								$('<div />')
									.append(
										$('<h4 />')	
											.attr('class',matchCentre.core.lowerCaseWithDashes(jsonData.metaData.awayTeamName))
											.text('Last matches played for: ')
									)
									.append(
										$('<strong />')
											.text(jsonData.metaData.awayTeamName)
									)
									.attr('class','badges-header')
							)
							.append(
								$('<ol />')
									.append(function(){
										var matchesObj = $(this);
							
										$.each(jsonData.content.awayTeamMatches,function(index,matchObj){
											var matchDomObj = $('<li />');
																						
											// Add the date of the match
											var matchDateObj = $('<span />').text(matchObj.date);
											matchDomObj.prepend(matchDateObj);
											
											// Add the match details, score, goalscorers
											$('<table />')
												.append(
													$('<thead />')
														.append(
															$('<tr />')
																.append($('<th />').text('Home team'))
																.append($('<th />').text('Score'))
																.append($('<th />').text('Away team'))
														)
												)
												.append(
													$('<tbody />')
														.append(
															$('<tr />')
																.append(
																	$('<td />')
																		.text(matchObj.home.teamName)
																		.IF(matchObj.home.goalScorers.length > 0)
																			.append(attachGoalScorersToMatch({goalScorers: matchObj.home.goalScorers}))
																		.ENDIF()
																)
																.append(
																	$('<td />')
																		.text(matchObj.score)
																)
																.append(
																	$('<td />')
																		.text(matchObj.away.teamName)
																		.IF(matchObj.away.goalScorers.length > 0)
																			.append(attachGoalScorersToMatch({goalScorers: matchObj.away.goalScorers}))
																		.ENDIF()
																)
														)
												)
												.attr('border','1')
												.insertAfter(matchDateObj);
												
											// Attach each individual match to the overall homeTeamMatches DOM object
											matchesObj.append(matchDomObj);
										})
										
										return matchesObj;
									})
									.attr('class','match-listing')
							)
							.attr('class','away')
					)
					.attr('class','inner')
			)

		// Notify dispatcher that a component has finished initializing
		matchCentre.footerBar.core.initializeComplete({
			finishedId: 'footerBar.matchPreview.last6Matches'
		});
	}
	
	/** 
	* Add an array of events to a single player
	* @method attachGoalScorersToMatch
	* @param {OBJECT} goalScorers A JSON object containing all goal information
	* @return {OBJECT} The complete DOM object to be attached to the current match
	*/
	function attachGoalScorersToMatch(obj){
		$.logEvent('[matchCentre.footerBar.matchPreview.last6Matches.attachGoalScorersToMatch]: ' + $.logJSONObj(obj));
		
		var goalsDomObj = $('<ul />');
		
		$.each(obj.goalScorers,function(index,goalScorerObj){
			goalsDomObj
				.append(
					$('<li />')
						.text(goalScorerObj.name + ' ' + goalScorerObj.minuteOfGoal + '"')
				)
		});
		
		return goalsDomObj;
	}	
	
	return {
		build: build,
		configuration: configuration
	}	
}());

/**
* @namespace matchCentre.footerBar.matchPreview.previousMeetings
*/

matchCentre.footerBar.matchPreview.previousMeetings = (function(){
	var configuration = {};
	
	/**
	* Build Footer Bar (Match Preview/Previous Meetings) in response to the initial pulled JSON content
	* @method build
	*/
	function build(){
		$.logEvent('[matchCentre.footerBar.matchPreview.previousMeetings.build]');

		// Store locally the relevant JSON content from the retrieved footerBar JSON
		var jsonData = matchCentre.footerBar.core.configuration.jsonContent.matchPreview.previousMeetings;

		var wrapperDomObj = $('.previous-meetings',matchCentre.footerBar.core.configuration.footerDomObj);

		wrapperDomObj
			.empty()
			.append(
				$('<h3 />')
					.append(
						$('<strong />')
							.text(wrapperDomObj.attr('data-flyout-title'))
					)
			)			
			.append(
				$('<div />')
					.append(
						$('<div />')
							.append(
								$('<ul />')
									.append(
										$('<li />')
											.append(
												$('<strong />')
													.attr('class','team-badges ' + matchCentre.core.lowerCaseWithDashes(jsonData.metaData.homeTeamName)).text(jsonData.metaData.homeTeamName)
											)
									)
									.append(
										$('<li />')
											.append(
												$('<strong />')
													.attr('class','team-badges ' + matchCentre.core.lowerCaseWithDashes(jsonData.metaData.awayTeamName)).text(jsonData.metaData.awayTeamName)
											)
									)
							)
							.attr('class','badges-header')
					)
					.append(
						$('<ol />')
							.append(function(){
								var matchesObj = $(this);
					
								$.each(jsonData.content,function(index,matchObj){
									var matchDomObj = $('<li />');
									
									// Add the date of the match
									var matchDateObj = $('<span />').text(matchObj.date);
									matchDomObj.prepend(matchDateObj);
									
									// Add the match details, score, goalscorers
									$('<table />')
										.append(
											$('<thead />')
												.append(
													$('<tr />')
														.append($('<th />').text('Home team'))
														.append($('<th />').text('Score'))
														.append($('<th />').text('Away team'))
												)
										)
										.append(
											$('<tbody />')
												.append(
													$('<tr />')
														.append(
															$('<td />')
																.text(jsonData.metaData.homeTeamName)
																.IF(matchObj.home.goalScorers.length > 0)
																	.append(attachGoalScorersToMatch({goalScorers: matchObj.home.goalScorers}))
																.ENDIF()
														)
														.append(
															$('<td />')
																.text(matchObj.score)
														)
														.append(
															$('<td />')
																.text(jsonData.metaData.awayTeamName)
																.IF(matchObj.away.goalScorers.length > 0)
																	.append(attachGoalScorersToMatch({goalScorers: matchObj.away.goalScorers}))
																.ENDIF()
														)
												)
										)
										.attr('border','1')
										.insertAfter(matchDateObj);
										
									// Attach each individual match to the overall homeTeamMatches DOM object
									matchesObj.append(matchDomObj);
								})
								
								return matchesObj;
							})
							.attr('class','match-listing')
					)
					.attr('class','inner')
			)
		
		// Notify dispatcher that a component has finished initializing
		matchCentre.footerBar.core.initializeComplete({
			finishedId: 'footerBar.matchPreview.previousMeetings'
		});
	}
	
	/** 
	* Add an array of events to a single player
	* @method attachEventToPlayer
	* @param {OBJECT} goalScorers A JSON object containing all goal information
	* @return {OBJECT} The complete DOM object to be attached to the current match
	*/
	function attachGoalScorersToMatch(obj){
		$.logEvent('[matchCentre.footerBar.matchPreview.previousMeetings.attachGoalScorersToMatch]: ' + $.logJSONObj(obj));
		
		var goalsDomObj = $('<ul />');
		
		$.each(obj.goalScorers,function(index,goalScorerObj){
			goalsDomObj
				.append(
					$('<li />')
						.text(goalScorerObj.name + ' ' + goalScorerObj.minuteOfGoal + '"')
				)
		});
		
		return goalsDomObj;
	}
	
	return {
		build: build,
		configuration: configuration
	}	
}());

/**
* @namespace matchCentre.footerBar.matchPreview.topScorers
*/

matchCentre.footerBar.matchPreview.topScorers = (function(){
	var configuration = {};
	
	/**
	* Build Footer Bar (Match Preview/Top Scorers) in response to the initial pulled JSON content
	* @method build
	*/
	function build(){
		$.logEvent('[matchCentre.footerBar.matchPreview.topScorers.build]');

		// Store locally the relevant JSON content from the retrieved footerBar JSON
		var jsonData = matchCentre.footerBar.core.configuration.jsonContent.matchPreview.topScorers;
		
		var wrapperDomObj = $('.top-scorers',matchCentre.footerBar.core.configuration.footerDomObj);
		
		wrapperDomObj
			.empty()
			.append(
				$('<h3 />')
					.append(
						$('<strong />')
							.text(wrapperDomObj.attr('data-flyout-title'))
					)
			)
			.append(
				$('<div />')
					.append(
						$('<div />')
							.append(
								$('<div />')
									.append(
										$('<h4 />')	
											.attr('class',matchCentre.core.lowerCaseWithDashes(jsonData.metaData.homeTeamName))
											.text('Top scorers for: ')
									)
									.append(
										$('<strong />')
											.text(jsonData.metaData.homeTeamName)
									)
									.attr('class','badges-header')
							)
							.append(
								$('<table />')
									.append(
										$('<thead />')
											.append(
												$('<tr />')
													.append($('<th />').text('Player'))
													.append($('<th />').text('Goals'))
											)
									)
									.append(
										$('<tbody />')
											.append(function(){
												var teamObj = $(this);
												
												$.each(jsonData.content.homeTeam,function(index,playerObj){
													var playerDomObj = $('<tr />');
													playerDomObj
														.append(
															$('<td />')
																.text(playerObj.playerName)
														)
														.append(
															$('<td />')
																.text(playerObj.goalsTotal)
														)
														
													playerDomObj.appendTo(teamObj);
												})
												
												return teamObj;
											})
									)
									.attr('border',1)
							)
							.attr('class','home')
					)
					.append(
						$('<div />')
							.append(
								$('<div />')
									.append(
										$('<h4 />')	
											.attr('class',matchCentre.core.lowerCaseWithDashes(jsonData.metaData.awayTeamName))
											.text('Top scorers for: ')
									)
									.append(
										$('<strong />')
											.text(jsonData.metaData.awayTeamName)
									)
									.attr('class','badges-header')
							)
							.append(
								$('<table />')
									.append(
										$('<thead />')
											.append(
												$('<tr />')
													.append($('<th />').text('Player'))
													.append($('<th />').text('Goals'))
											)
									)
									.append(
										$('<tbody />')
											.append(function(){
												var teamObj = $(this);
												
												$.each(jsonData.content.awayTeam,function(index,playerObj){
													var playerDomObj = $('<tr />');
													playerDomObj
														.append(
															$('<td />')
																.text(playerObj.playerName)
														)
														.append(
															$('<td />')
																.text(playerObj.goalsTotal)
														)
														
													playerDomObj.appendTo(teamObj);
												})
												
												return teamObj;
											})
									)
									.attr('border',1)
							)
							.attr('class','away')
					)
					.attr('class','inner')
			)
		
		// Notify dispatcher that a component has finished initializing
		matchCentre.footerBar.core.initializeComplete({
			finishedId: 'footerBar.matchPreview.topScorers'
		});
	}
	
	return {
		build: build,
		configuration: configuration
	}	
}());

/**
* @namespace matchCentre.footerBar.matchPreview.next3Matches
*/

matchCentre.footerBar.matchPreview.next3Matches = (function(){
	var configuration = {};
	
	/**
	* Build Footer Bar (Match Preview/Next 3 Matches) in response to the initial pulled JSON content
	* @method build
	*/
	function build(){
		$.logEvent('[matchCentre.footerBar.matchPreview.next3Matches.build]');

		// Store locally the relevant JSON content from the retrieved footerBar JSON
		var jsonData = matchCentre.footerBar.core.configuration.jsonContent.matchPreview.next3Matches;

		var wrapperDomObj = $('.next-3-matches',matchCentre.footerBar.core.configuration.footerDomObj);

		wrapperDomObj
			.empty()
			.append(
				$('<h3 />')
					.append(
						$('<strong />')
							.text(wrapperDomObj.attr('data-flyout-title'))
					)
			)
			.append(
				$('<div />')
					.append(
						$('<div />')
							.append(
								$('<div />')
									.append(
										$('<h4 />')	
											.attr('class',matchCentre.core.lowerCaseWithDashes(jsonData.metaData.homeTeamName))
											.text('Next 3 matches for: ')
									)
									.append(
										$('<strong />')
											.text(jsonData.metaData.homeTeamName)
									)
									.attr('class','badges-header')
							)
							.append(
								$('<ol />')
									.append(function(){
										var matchesObj = $(this);
							
										$.each(jsonData.content.homeTeamMatches,function(index,matchObj){
											var matchDomObj = $('<li />');
											
											// Add the date of the match
											var matchDateObj = $('<span />').text(matchObj.date);
											matchDomObj.prepend(matchDateObj);
											
											// Add the match details
											$('<table />')
												.append(
													$('<thead />')
														.append(
															$('<tr />')
																.append($('<th />').text('Home team'))
																.append($('<th />').text('vs'))
																.append($('<th />').text('Away team'))
														)
												)
												.append(
													$('<tbody />')
														.append(
															$('<tr />')
																.append(
																	$('<td />')
																		.text(matchObj.home.teamName)
																)
																.append(
																	$('<td />')
																		.text('vs')
																)
																.append(
																	$('<td />')
																		.text(matchObj.away.teamName)
																)
														)
												)
												.attr('border','1')
												.insertAfter(matchDateObj);
												
											// Attach each individual match to the overall homeTeamMatches DOM object
											matchesObj.append(matchDomObj);
										})
										
										return matchesObj;
									})
									.attr('class','match-listing')
							)
							.attr('class','home')
					)
					.append(
						$('<div />')
							.append(
								$('<div />')
									.append(
										$('<h4 />')	
											.attr('class',matchCentre.core.lowerCaseWithDashes(jsonData.metaData.awayTeamName))
											.text('Next 3 matches for: ')
									)
									.append(
										$('<strong />')
											.text(jsonData.metaData.awayTeamName)
									)
									.attr('class','badges-header')
							)
							.append(
								$('<ol />')
									.append(function(){
										var matchesObj = $(this);
							
										$.each(jsonData.content.awayTeamMatches,function(index,matchObj){
											var matchDomObj = $('<li />');
											
											// Add the date of the match
											var matchDateObj = $('<span />').text(matchObj.date);
											matchDomObj.prepend(matchDateObj);
											
											// Add the match details, score, goalscorers
											$('<table />')
												.append(
													$('<thead />')
														.append(
															$('<tr />')
																.append($('<th />').text('Home team'))
																.append($('<th />').text('vs'))
																.append($('<th />').text('Away team'))
														)
												)
												.append(
													$('<tbody />')
														.append(
															$('<tr />')
																.append(
																	$('<td />')
																		.text(matchObj.home.teamName)
																)
																.append(
																	$('<td />')
																		.text('vs')
																)
																.append(
																	$('<td />')
																		.text(matchObj.away.teamName)
																)
														)
												)
												.attr('border','1')
												.insertAfter(matchDateObj);
												
											// Attach each individual match to the overall homeTeamMatches DOM object
											matchesObj.append(matchDomObj);
										})
										
										return matchesObj;
									})
									.attr('class','match-listing')
							)
							.attr('class','away')
					)
					.attr('class','inner')
			)
		
		// Notify dispatcher that a component has finished initializing
		matchCentre.footerBar.core.initializeComplete({
			finishedId: 'footerBar.matchPreview.next3Matches'
		});
	}
		
	return {
		build: build,
		configuration: configuration
	}	
}());

/**
* @namespace matchCentre.footerBar.matchStats.attack
*/

matchCentre.footerBar.matchStats.attack = (function(){
	var configuration = {};
	
	/**
	* Build Footer Bar (Match Stats/Attack) in response to the initial pulled JSON content
	* @method build
	*/
	function build(){
		$.logEvent('[matchCentre.footerBar.matchStats.attack.build]');

		// Store locally the relevant JSON content from the retrieved footerBar JSON
		var jsonData = matchCentre.footerBar.core.configuration.jsonContent.matchStats.attack;
		
		var wrapperDomObj = $('.attack',matchCentre.footerBar.core.configuration.footerDomObj);

		// Capture any errors should the content for the League Table not be correct
		try{
			wrapperDomObj
				.empty()
				.append(
					$('<h3 />')
						.append(
							$('<strong />')
								.text(wrapperDomObj.attr('data-flyout-title'))
						)
				)
				.append(
					$('<div />')
						.attr('class','inner')
				)
				
			// Build the split bar graph
			$('.inner',wrapperDomObj).splitBar('build',{
				data: jsonData,
				initialLoad: false
			});
			
			// Notify dispatcher that a component has finished initializing
			matchCentre.footerBar.core.initializeComplete({
				finishedId: 'footerBar.matchStats.attack'
			});
		}
		catch(err){
			$.logEvent('[matchCentre.footerBar.matchStats.attack.build]: an error occured populating the Attack component: ' + $.logJSONObj(err));
			
			// Notify dispatcher that a component has finished initializing
			matchCentre.footerBar.core.initializeComplete({
				finishedId: 'footerBar.matchStats.attack'
			});
		}
	}
	
	/**
	* Process the Pusher real-time update
	* @method websocketCallback
	* @param {OBJECT} data A JSON update from Pusher
	*/	
	function websocketCallback(obj){
		$.logEvent('[matchCentre.footerBar.matchStats.attack.websocketCallback]: ' + $.logJSONObj(obj));
				
		// Capture any errors should the content for Match Stats > Attack not be correct
		try{
			var dataObj = obj.data.attack;
			
			// Refresh a split bar graph in response to JSON content
			$('.attack .inner').splitBar('refresh',{
				data: dataObj
			});
		}
		catch(err){
			$.logEvent('[matchCentre.footerBar.matchStats.attack.websocketCallback]: an error occured populating the Attack (footerBar) component (via Pusher): ' + $.logJSONObj(err));
		}
	}
	
	return {
		build: build,
		configuration: configuration,
		websocketCallback: websocketCallback
	}	
}());

/**
* @namespace matchCentre.footerBar.matchStats.generalPlay
*/

matchCentre.footerBar.matchStats.generalPlay = (function(){
	var configuration = {};
	
	/**
	* Build Footer Bar (Match Stats/General Play) in response to the initial pulled JSON content
	* @method build
	*/
	function build(){
		$.logEvent('[matchCentre.footerBar.matchStats.generalPlay.build]');

		// Store locally the relevant JSON content from the retrieved footerBar JSON
		var jsonData = matchCentre.footerBar.core.configuration.jsonContent.matchStats.generalPlay;
		
		var wrapperDomObj = $('.general-play',matchCentre.footerBar.core.configuration.footerDomObj);

		try{
			wrapperDomObj
				.empty()
				.append(
					$('<h3 />')
						.append(
							$('<strong />')
								.text(wrapperDomObj.attr('data-flyout-title'))
						)
				)
				.append(
					$('<div />')
						.attr('class','inner')
				)
				
			// Build the split bar graph
			$('.inner',wrapperDomObj).splitBar('build',{
				data: jsonData,
				initialLoad: false
			});
			
			// Notify dispatcher that a component has finished initializing
			matchCentre.footerBar.core.initializeComplete({
				finishedId: 'footerBar.matchStats.generalPlay'
			});
		}
		catch(err){
			$.logEvent('[matchCentre.footerBar.matchStats.generalPlay.build]: an error occured populating the General Play component: ' + $.logJSONObj(err));
			
			// Notify dispatcher that a component has finished initializing
			matchCentre.footerBar.core.initializeComplete({
				finishedId: 'footerBar.matchStats.generalPlay'
			});
		}
	}
	
	/**
	* Process the Pusher real-time update
	* @method websocketCallback
	* @param {OBJECT} data A JSON update from Pusher
	*/	
	function websocketCallback(obj){
		$.logEvent('[matchCentre.footerBar.matchStats.generalPlay.websocketCallback]: ' + $.logJSONObj(obj));
				
		// Capture any errors should the content for Match Stats > General Play not be correct
		try{
			var dataObj = obj.data.generalPlay;
			
			// Refresh a split bar graph in response to JSON content
			$('.general-play .inner').splitBar('refresh',{
				data: dataObj
			});
		}
		catch(err){
			$.logEvent('[matchCentre.footerBar.matchStats.generalPlay.websocketCallback]: an error occured populating the General Play (footerBar) component (via Pusher): ' + $.logJSONObj(err));
		}
	}
	
	return {
		build: build,
		configuration: configuration,
		websocketCallback: websocketCallback
	}	
}());

/**
* @namespace matchCentre.footerBar.matchStats.distribution
*/

matchCentre.footerBar.matchStats.distribution = (function(){
	var configuration = {};
	
	/**
	* Build Footer Bar (Match Stats/Distribution) in response to the initial pulled JSON content
	* @method build
	*/
	function build(){
		$.logEvent('[matchCentre.footerBar.matchStats.distribution.build]');

		// Store locally the relevant JSON content from the retrieved footerBar JSON
		var jsonData = matchCentre.footerBar.core.configuration.jsonContent.matchStats.distribution;
		
		var wrapperDomObj = $('.distribution',matchCentre.footerBar.core.configuration.footerDomObj);

		try{
			wrapperDomObj
				.empty()
				.append(
					$('<h3 />')
						.append(
							$('<strong />')
								.text(wrapperDomObj.attr('data-flyout-title'))
						)
				)
				.append(
					$('<div />')
						.attr('class','inner')
				)
				
			// Build the split bar graph
			$('.inner',wrapperDomObj).splitBar('build',{
				data: jsonData,
				initialLoad: false
			});
			
			// Notify dispatcher that a component has finished initializing
			matchCentre.footerBar.core.initializeComplete({
				finishedId: 'footerBar.matchStats.distribution'
			});
		}
		catch(err){
			$.logEvent('[matchCentre.footerBar.matchStats.distribution.build]: an error occured populating the Distribution component: ' + $.logJSONObj(err));
			
			// Notify dispatcher that a component has finished initializing
			matchCentre.footerBar.core.initializeComplete({
				finishedId: 'footerBar.matchStats.distribution'
			});
		}
	}
	
	/**
	* Process the Pusher real-time update
	* @method websocketCallback
	* @param {OBJECT} data A JSON update from Pusher
	*/	
	function websocketCallback(obj){
		$.logEvent('[matchCentre.footerBar.matchStats.distribution.websocketCallback]: ' + $.logJSONObj(obj));

		// Capture any errors should the content for Match Stats > Distribution not be correct
		try{
			var dataObj = obj.data.distribution;
			
			// Refresh a split bar graph in response to JSON content
			$('.distribution .inner').splitBar('refresh',{
				data: dataObj
			});
		}
		catch(err){
			$.logEvent('[matchCentre.footerBar.matchStats.distribution.websocketCallback]: an error occured populating the Distribution (footerBar) component (via Pusher): ' + $.logJSONObj(err));
		}
	}
	
	return {
		build: build,
		configuration: configuration,
		websocketCallback: websocketCallback
	}	
}());

/**
* @namespace matchCentre.footerBar.matchStats.defenceAndDiscipline
*/

matchCentre.footerBar.matchStats.defenceAndDiscipline = (function(){
	var configuration = {};
	
	/**
	* Build Footer Bar (Match Stats/Defence & Discipline) in response to the initial pulled JSON content
	* @method build
	*/
	function build(){
		$.logEvent('[matchCentre.footerBar.matchStats.defenceAndDiscipline.build]');

		// Store locally the relevant JSON content from the retrieved footerBar JSON
		var jsonData = matchCentre.footerBar.core.configuration.jsonContent.matchStats.defenceAndDiscipline;
		
		var wrapperDomObj = $('.defence-and-discipline',matchCentre.footerBar.core.configuration.footerDomObj);

		try{
			wrapperDomObj
				.empty()
				.append(
					$('<h3 />')
						.append(
							$('<strong />')
								.text(wrapperDomObj.attr('data-flyout-title'))
						)
				)
				.append(
					$('<div />')
						.attr('class','inner')
				)
				
			// Build the split bar graph
			$('.inner',wrapperDomObj).splitBar('build',{
				data: jsonData,
				initialLoad: false
			});
			
			// Notify dispatcher that a component has finished initializing
			matchCentre.footerBar.core.initializeComplete({
				finishedId: 'footerBar.matchStats.defenceAndDiscipline'
			});
		}
		catch(err){
			$.logEvent('[matchCentre.footerBar.matchStats.defenceAndDiscipline.build]: an error occured populating the Defence & Discipline component: ' + $.logJSONObj(err));
			
			// Notify dispatcher that a component has finished initializing
			matchCentre.footerBar.core.initializeComplete({
				finishedId: 'footerBar.matchStats.defenceAndDiscipline'
			});
		}
	}
	
	/**
	* Process the Pusher real-time update
	* @method websocketCallback
	* @param {OBJECT} data A JSON update from Pusher
	*/	
	function websocketCallback(obj){
		$.logEvent('[matchCentre.footerBar.matchStats.defenceAndDiscipline.websocketCallback]: ' + $.logJSONObj(obj));
				
		// Capture any errors should the content for Match Stats > Defence & Discipline not be correct
		try{
			var dataObj = obj.data.defenceAndDiscipline;
			
			// Refresh a split bar graph in response to JSON content
			$('.defence-and-discipline .inner').splitBar('refresh',{
				data: dataObj
			});
		}
		catch(err){
			$.logEvent('[matchCentre.footerBar.matchStats.defenceAndDiscipline.websocketCallback]: an error occured populating the Defence & Discipline (footerBar) component (via Pusher): ' + $.logJSONObj(err));
		}
	}
	
	return {
		build: build,
		configuration: configuration,
		websocketCallback: websocketCallback
	}	
}());

/**
* @namespace matchCentre.footerBar.facts
*/

matchCentre.footerBar.facts = (function(){
	var configuration = {};
	
	/**
	* Build Footer Bar (Facts) in response to the initial pulled JSON content
	* @method build
	*/
	function build(){
		$.logEvent('[matchCentre.footerBar.facts.build]');

		// Store locally the relevant JSON content from the retrieved footerBar JSON
		var jsonData = matchCentre.footerBar.core.configuration.jsonContent.facts;
		
		var wrapperDomObj = $('.facts',matchCentre.footerBar.core.configuration.footerDomObj);

		try{
			wrapperDomObj
				.empty()
				.append(
					$('<h3 />')
						.append(
							$('<strong />')
								.text(wrapperDomObj.attr('data-flyout-title'))
						)
				)
				.append(
					$('<div />')
						.append(
							$('<ul />')
								.append(function(){
									var listItems = '';
									
									$.each(jsonData.content,function(index,factObj){
										listItems += '<li>' + factObj.fact + '</li>';
									});
									
									return listItems;
								})
						)
						.attr('class','inner')
				)
			
			// Notify dispatcher that a component has finished initializing
			matchCentre.footerBar.core.initializeComplete({
				finishedId: 'footerBar.facts'
			});
		}
		catch(err){
			$.logEvent('[matchCentre.footerBar.facts.build]: an error occured populating the Facts component: ' + $.logJSONObj(err));
			
			// Notify dispatcher that a component has finished initializing
			matchCentre.footerBar.core.initializeComplete({
				finishedId: 'footerBar.facts'
			});
		}
	}
	
	return {
		build: build,
		configuration: configuration
	}	
}());