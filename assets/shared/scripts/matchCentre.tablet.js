/**
* MatchCentre v1.0
* @module matchCentre
*/

var matchCentre = window.matchCentre || {};

/**
* MatchCentre root class
* @class matchCentre
* @namespace matchCentre.tablet
*/

matchCentre.tablet = (function(){
	/**
	* Initialize all tablet functionality
	* @method init
	*/
	function init(){
		$.logEvent('[matchCentre.tablet.init]');
		
		matchCentre.configuration.deviceType = 'tablet';
		
		// Initialize the countdown timer
		matchCentre.core.countdownTimerInit();
		
		// Initialize the fixtures
		matchCentre.fixtures.init({deviceType: 'tablet'});
		
		// Re-instate the opacity of the .source DOM element(s) to be visible, regardless of toggle state, remove inline image height attributes
		matchCentre.newsFeed.refresh();

		// Reset the visibility of the dropdown bar
		matchCentre.dropdownBar.reset();
		
		// Reset the visibility of the footer bar
		matchCentre.footerBar.core.reset();
		
		// Resize all amCharts objects
		matchCentre.newsFeed.amChartsRefresh();
	}
	
	/**
	* Unload all associated event handlers relating to tablet
	* @method unload
	*/
	function unload(){
		$.logEvent('[matchCentre.tablet.unload]');

		// Re-instate the DOM position of the fixtures trigger for tablet
		$('#fixtures-trigger').insertBefore($('#fixtures'));
		
		// Unload the fixtures
		matchCentre.fixtures.unload();
	}
		
	return {
		init: init,
		unload: unload
	}
}());