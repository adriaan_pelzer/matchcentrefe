/**
* MatchCentre v1.0
* @module matchCentre
*/

var matchCentre = window.matchCentre || {};

/**
* MatchCentre root class
* @class matchCentre
* @namespace matchCentre.mobile
*/

matchCentre.mobile = (function(){
	var configuration = {
		timings: {
			hideAddressBar: 201
		}
	};
		
	/**
	* Initialize all mobile functionality
	* @method init
	*/
	function init(){
		$.logEvent('[matchCentre.mobile.init]');	
		
		matchCentre.configuration.deviceType = 'mobile';
		
		// Initialize the countdown timer
		matchCentre.core.countdownTimerInit();
		
		// Mobile requires the match statistics teams to have data attributes, and the HTML team names removed (for presentational reasons)
		// matchSummaryReArrange();

		// Toggle the visibility of the goal scorers	
		// toggleGoalScorers();
		
		// Initialize the fixtures
		matchCentre.fixtures.init({deviceType: 'mobile'});
		
		// Re-instate the opacity of the .source DOM element(s) to be visible, regardless of toggle state, remove inline image height attributes
		matchCentre.newsFeed.refresh();
		
		// Reset the visibility of the dropdown bar
		matchCentre.dropdownBar.reset();
		
		// Reset the visibility of the footer bar
		matchCentre.footerBar.core.reset();
		
		// Resize all amCharts objects
		matchCentre.newsFeed.amChartsRefresh();
	}
	
	/**
	* Mobile require the match statistics teams to have data attributes, and the HTML team names removed (for presentational reasons)
	* @method matchSummaryReArrange
	*/
	function matchSummaryReArrange(){
		$.logEvent('[matchCentre.mobile.matchSummaryReArrange]');
		
		var homeTeamName = $('#match-summary .home:eq(0) SPAN').html();
		var awayTeamName = $('#match-summary .away:eq(0) SPAN').html();
		
		$('#match-summary .home:eq(0) SPAN')
			.attr('data-clubname',homeTeamName)
			.html('')
		.parent()			
			.siblings('.away:eq(0)')
				.children('SPAN')
					.attr('data-clubname',awayTeamName)
					.html('')
	}
	
	/**
	* Re-instate the club team names since mobile is being left
	* @method restoreMatchSummary
	*/
	function restoreMatchSummary(){
		$.logEvent('[matchCentre.mobile.restoreMatchSummary]');
		
		var homeTeamName = $('#match-summary .home:eq(0) SPAN').attr('data-clubname');
		var awayTeamName = $('#match-summary .away:eq(0) SPAN').attr('data-clubname');
		
		$('#match-summary')
			.find('.home:eq(0) SPAN')
				.html(homeTeamName)
			.end()
			.find('.away:eq(0) SPAN')
				.html(awayTeamName)
			.end()
			.find('TBODY')
				.show();
	}
	
	/**
	* Toggle the visibility of the goal scorers
	* @method toggleGoalScorers
	*/
	function toggleGoalScorers(){
		$.logEvent('[matchCentre.mobile.toggleGoalScorers]');
	
		var linkObj;
		var toggleTableObj = $('#match-summary TBODY');
		
		$('#toggle-goal-scorers A').on('click',function(e){
			e.preventDefault();
			
			linkObj = $(this);			
			toggleTableObj
				.toggle(1,function(){
					linkObj.attr('class',toggleTableObj.is(':visible') ? 'expanded' : 'contracted')
				})
		});
	}
	
	/**
	* Unload all associated event handlers relating to mobile
	* @method unload
	*/
	function unload(){
		$.logEvent('[matchCentre.mobile.unload]');
		
		// Re-instate the club team names since mobile is being left
		// restoreMatchSummary();
		
		// Reset the visibility of the dropdown bar
		// matchCentre.dropdownBar.reset();		
		
		// Remove the handler for toggling goal scorers
		// $('#toggle-goal-scorers A').off('click');
	}
		
	return {
		configuration: configuration,
		init: init,
		unload: unload
	}
}());