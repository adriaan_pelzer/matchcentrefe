/**
* MatchCentre v1.0
* @module matchCentre
*/

var matchCentre = window.matchCentre || {};

/**
* MatchCentre root class
* @class matchCentre
* @namespace matchCentre.fixtures
*/

matchCentre.fixtures = (function(){
	var configuration = {
		centralizeOffsets: {
			enabled: true,
			desktop: {
				1: 3,
				2: 2,
				3: 2,
				4: 1,
				5: 1,
				6: 1,
				7: 1
			},
			tablet: {
				1: 2,
				2: 1,
				3: 1
			}
		},
		easingEquation: 'easeInOutQuad', 
		expanded: false, 
		fixturesObj: null,
		iScrollObj: null, 
		matchesInViewport: { 
			mobile: 5,
			tablet: 5,
			desktop: 7
		}, 
		matchWidth: null, 
		timings: {
			animateToCentre: 750,
			animateToNewMatch: 250, 
			centralizeMatches: 1,
			expandContract: 250,
			mobileHeightTimeout: 500 			
		},
		totalMatches: 0, 
		updates: {
			blinks: 6,
			blinkPeriod: 500,
			fadeInOut: 350
		}
	};
		
	/**
	* Build the fixtures in response to the initial pulled JSON content
	* @method build
	*/
	function build(){
		$.logEvent('[matchCentre.fixtures.build]');
		
		// Create the necessary wrapper DOM elements
		var fixturesContainerObj = $('<div />')
			.append(
				$('<ul />')
					.append(
						$('<li />')
							.append(
								$('<a />')
									.attr({
										'class': 'previous',
										href: '#'
									})
									.html('Previous')
							)
					)
					.append(
						$('<li />')
							.append(
								$('<a />')
									.attr({
										'class': 'next',
										href: '#'
									})
									.html('Next')
							)
					)
			)
			.append(
				$('<div />')
					.append(
						$('<div />')
							.append(
								$('<p />')
									.append(
										$('<a />')
											.attr('href',$('#view-all-live-games').attr('href'))
											.html('All games')
											.append(
												$('<span />')
													.html('Click here to view Match Day Live')
											)
									)
									.attr('class','inline-link')
							)
							.attr('class','inner')
					)
					.attr('class','outer')
			)
			.attr({
				'data-selected-index': 0,
				id: 'fixtures'
			})
			
		// Attach the container DOM element for fixtures to the DOM	
		fixturesContainerObj.appendTo($('#header'))
		
		var matchObj;
		
		// Capture any errors should the content for the Fixtures not be correct
		try{
			// Iterate through the Fixtures JSON, and attach to the fixtures DOM element
			$.each(matchCentre.configuration.initialSiteContent.fixtures.content,function(index,data){
				configuration.totalMatches++;
				
				matchObj = $('<div />')
					.append(
						$('<h3 />')
							.html(this.homeTeam.longName + ' vs ' + this.awayTeam.longName)
					)
					.append(
						$('<table />')
							.IF(matchCentre.configuration.isMatchDetailPage)
								.IF(this.id == matchCentre.configuration.fixtureId)
									.attr('class','active')	
								.ENDIF()
							.ELSE()
								.IF(this.active)
									.attr('class','active')
								.ENDIF()
							.ENDIF()
							.append(
								$('<thead />')
									.append(
										$('<tr />')
											.append(
												$('<th />')
													.attr('colspan',2)
													.html(this.active ? this.status : this.status == 'Full Time' ? this.status : this.startTime)
											)
									)
							)
							.append(
								$('<tbody />')
									.append(
										$('<tr />')
											.append(
												$('<th />')
													.html(this.homeTeam.shortName)
											)
											.append(
												$('<td />')
													.attr('data-club',matchCentre.core.lowerCaseWithDashes(this.homeTeam.shortName))
													.html(this.homeTeam.score)
											)
									)
									.append(
										$('<tr />')
											.append(
												$('<th />')
													.html(this.awayTeam.shortName)
													.append(
														$('<a />')
															.attr('href',this.url)
															.html('View match details')
													)
											)
											.append(
												$('<td />')
													.attr('data-club',matchCentre.core.lowerCaseWithDashes(this.awayTeam.shortName))
													.html(this.awayTeam.score)
											)
									)
							)
							.attr('border',1)
					)
					.attr({
						'class': 'match',
						'data-match-id': this.id
					})
					
				matchObj.appendTo($('.inner',fixturesContainerObj));
			});
			
			// Add the Beta feedback link
			$('.inner',fixturesContainerObj)
				.append(
					$('<p />')
						.append(
							$('<a />')
								.attr('href','#')
								.html('Send us some feedback')
								.append(
									$('<span />')
										.html('Like what you see? Think we can do better? Send us some feedback >')
								)
						)
						.attr('class','inline-link')
				)
			
			// With all matches attached to the Fixtures DOM element, add the final 'Mirror.co.uk' suffix link
			$('.inner',fixturesContainerObj)
				.append(
					$('<p />')
						.append(
							$('<a />')
								.attr('href','#')
								.html('Mirror.co.uk')
								.append(
									$('<span />')
										.html('Click here to view Mirror.co.uk')
								)
						)
						.attr('class','inline-link')
				)
			
			// Specify globally a pointer to the newly injected Fixtures DOM element
			configuration.fixturesObj = fixturesContainerObj;
			
			// Initialize fixtures
			init({deviceType: matchCentre.configuration.deviceType});
		}
		catch(err){			
			$.logEvent('[matchCentre.fixtures.build]: an error occured populating the Fixtures component: ' + $.logJSONObj(err));
			
			if(!matchCentre.configuration.siteInitialized){
				// Notify dispatcher that a component has finished initializing
				matchCentre.dispatcher.initializeComplete({
					finishedId: 'fixtures'
				});
			}
		}
	}
	
	/**
	* Initialize fixtures
	* @method init
	* @param {STRING} deviceType 'mobile' || 'tablet' || 'desktop'
	*/
	function init(obj){
		$.logEvent('[matchCentre.fixtures.init]: ' + $.logJSONObj(obj));
		
		// Update the fixtures trigger label (only mobile sees this, as it's used to expand the carousel)
		$('#fixtures-trigger SPAN')
			.IF(matchCentre.configuration.isMatchDetailPage)
				.text(matchCentre.configuration.homeTeamName + ' vs ' + matchCentre.configuration.awayTeamName)
			.ELSE()
				.text('All games')
			.ENDIF()
							
		// Don't initialize fixtures functionality for the mobile render mode
		if(obj.deviceType != 'mobile'){
			// Re-instate height of the fixtures (for everything other than mobile)
			configuration.fixturesObj.css('height','auto');
			
			// Add a data attribute to the fixtures, defining how many scores it contains
			configuration.fixturesObj.attr('data-scores-contained',$('.match',configuration.fixturesObj).size());
			
			// Change the dimensions of the .match elements within the fixtures for tablet
			if(obj.deviceType == 'tablet'){
				resize({deviceType: obj.deviceType});
			}
			else{
				// The default width for the fixed-width desktop layout
				configuration.matchWidth = 156;
				
				// Update the width of the fixtures inner upon initialization
				$('.inner',configuration.fixturesObj).css('width',parseInt(configuration.fixturesObj.attr('data-scores-contained')) * configuration.matchWidth);
			}

			$.logEvent('[matchCentre.fixtures.init]: Page width=' + Math.floor($('#wrapper').width()) + ', innerWidth=' + $('.inner',configuration.fixturesObj).width() + ', matchWidth=' + configuration.matchWidth + ', isTablet=' + matchCentre.core.isTablet());
			
			// Add a data attribute to the fixtures, defining the overhang (in order to ascertain when the next arrow should be disabled)
			configuration.fixturesObj.attr('data-overhang',parseInt(configuration.fixturesObj.attr('data-scores-contained')) - configuration.matchesInViewport[obj.deviceType]);
									
			// Initialize handlers for previous and next arrows
			previousNextInit();
			
			// For non-mobile fixtures, centralize the matches in the best way possible
			if(configuration.centralizeOffsets.enabled){
				centralizeMatches();
			}
		}
		else{
			$('.inner',configuration.fixturesObj).css('width','100%');
			
			if(!configuration.fixturesObj.hasAttr('data-mobile-height')){
				// For CSS3 transitions to occur, the height for the transition is required, create it as a data attribute here, if not already in the DOM
				configuration.fixturesObj.addClass('offscreen');
				
				setTimeout(function(){
					configuration.fixturesObj
						.attr({
							'data-mobile-height': parseInt($('.outer',configuration.fixturesObj).height()),
							'data-original-dom-object': true
						})
						.css('height',0)
						.removeClass('offscreen')
				},configuration.timings.mobileHeightTimeout);
			}
			else{
				configuration.fixturesObj.css('height',0);
			}
		
			// Initialize the handler for showing the mobile treatment of the current scores
			mobileHandlersInit();
		}
		
		// Make sure that when clicking (anywhere) on a match, the match detail page URL is loaded
		$('.match TBODY',configuration.fixturesObj).on('click',function(){		
			top.location.href = $('A',this).attr('href');
		});

		if(!matchCentre.configuration.siteInitialized){
			// Notify dispatcher that a component has finished initializing
			matchCentre.dispatcher.initializeComplete({
				finishedId: 'fixtures'
			});
		}
	}
	
	/**
	* For mobile fixtures, centralize the matches in the best way possible
	* @method centralizeMatchesMobile
	*/
	function centralizeMatchesMobile(){
		$.logEvent('[matchCentre.fixtures.centralizeMatchesMobile]');
		
		var fixtureListCentrePoint = configuration.fixturesObj.height()/2;
		var activeMatchesObj = [];
		var middleActiveMatch;
		var centralMatchObj;
		var positionFromTopOfFixtures;
		
		$('.match',configuration.fixturesObj).each(function(index){
			if($('>TABLE',this).hasClass('active')){				
				activeMatchesObj.push(index);
			}
		});
				
		var centralPosition;
		var centralMatchIndex;
		
		if(activeMatchesObj.length > 1){
			// Handle even numbers of active matches differently to odd numbers of active matches
			if(activeMatchesObj.length%2 == 0){
				centralMatchIndex = activeMatchesObj[activeMatchesObj.length/2]-1;
				centralMatchObj = $('.match:eq(' + centralMatchIndex + ')',configuration.fixturesObj);
				positionFromTopOfFixtures = Math.floor(centralMatchObj.offset().top - configuration.fixturesObj.offset().top);
				positionFromTopOfFixtures += centralMatchObj.height();
			}
			else{
				centralPosition = Math.ceil(activeMatchesObj.length/2);
				centralMatchIndex = activeMatchesObj[centralPosition]-1;
				centralMatchObj = $('.match:eq(' + centralMatchIndex + ')',configuration.fixturesObj);		
				positionFromTopOfFixtures = Math.floor(centralMatchObj.offset().top - configuration.fixturesObj.offset().top);
				positionFromTopOfFixtures += centralMatchObj.height()/2;
			}
		}
		else{
			centralMatchObj = $('.match:eq(' + activeMatchesObj[0] + ')',configuration.fixturesObj);
			positionFromTopOfFixtures = Math.floor(centralMatchObj.offset().top - configuration.fixturesObj.offset().top);
			positionFromTopOfFixtures += centralMatchObj.height()/2;							
		}
		
		var centralizeOffsetPosition = 0;
		
		$.logEvent('[matchCentre.fixtures.centralizeMatchesMobile]: positionFromTopOfFixtures: ' + fixtureListCentrePoint);
		
		// Calculate the centralized offset position to send iScroll to
		if(positionFromTopOfFixtures > fixtureListCentrePoint){
			centralizeOffsetPosition = fixtureListCentrePoint - positionFromTopOfFixtures;
		}
		else{
			centralizeOffsetPosition = fixtureListCentrePoint;
		}
		
		$.logEvent('[matchCentre.fixtures.centralizeMatchesMobile]: centralizeOffsetPosition: ' + centralizeOffsetPosition);
		
		// Scroll the scrollable internal DOM element to the centre of the active matches
		// Only centralize the match if a negative centralize offset is found
		if(centralizeOffsetPosition < 0){
			configuration.iScrollObj.scrollTo(0,centralizeOffsetPosition,configuration.timings.animateToCentre,IScroll.utils.ease.bounce);
			// Possible easing equations: quadratic || circular || back || bounce || elastic
		}
		
	}
	
	/**
	* For non-mobile fixtures, centralize the matches in the best way possible
	* @method centralizeMatches
	*/
	function centralizeMatches(){
		$.logEvent('[matchCentre.fixtures.centralizeMatches');
		
		// Before centralizing, ensure that there are enough matches in order for it to be calculated
		if(configuration.totalMatches <= configuration.matchesInViewport[matchCentre.configuration.deviceType]){
			// Update the state of the previous/next handlers based on the data-selected-index attribute of the fixtures
			updatePreviousNext({initial: true});
			
			return false;
		}
		
		var activeMatches = 0;
		var activeMatchesObj = [];
		var firstActiveMatch;
		var matchOffsetIndex = 0;
		
		$('.match',configuration.fixturesObj).each(function(index){
			if($('>TABLE',this).hasClass('active')){
				activeMatches++;
				
				activeMatchesObj.push(index);
			}
		});
		
		firstActiveMatch = activeMatchesObj[0];
				
		// Work out which match to offset the scroll position to, in order to best centralize the active matches
		if(activeMatches > 0){
			var matchesOverhang = matchCentre.configuration.deviceType == 'desktop' ? 3 : 5;
			
			if(configuration.centralizeOffsets[matchCentre.configuration.deviceType].hasOwnProperty(activeMatches)){
				matchOffsetIndex = firstActiveMatch-configuration.centralizeOffsets[matchCentre.configuration.deviceType][activeMatches]
				
				if(matchOffsetIndex < 0){
					matchOffsetIndex = 0;
				}
			}
			else{
				matchOffsetIndex = firstActiveMatch;
			}
			
			// Handle the scenario where the active set of matches are towards the end of the carousel
			if(matchOffsetIndex > (configuration.totalMatches-configuration.matchesInViewport[matchCentre.configuration.deviceType])){
				matchOffsetIndex = configuration.totalMatches-configuration.matchesInViewport[matchCentre.configuration.deviceType];
			}
		}

		$.logEvent('[matchCenter.fixtures.centralizeMatches: matchesOverhang: ' + matchesOverhang + ', activeMatches: ' + activeMatches + ', firstActiveMatch: ' + firstActiveMatch + ', offset to use: -' + configuration.centralizeOffsets[matchCentre.configuration.deviceType][activeMatches] + ', matchOffsetIndex: ' + matchOffsetIndex);
				
		// Animate the fixtures to the new position
		$('.outer',configuration.fixturesObj).scrollTo({
			left: '+=' + (configuration.matchWidth*matchOffsetIndex) + 'px',
			top: 0
		},configuration.timings.centralizeMatches,{
			easing: configuration.easingEquation,
			onAfter: function(){
				// Update the selected index data attribute, so that previous and next handlers adapt to the automated animation
				configuration.fixturesObj.attr('data-selected-index',matchOffsetIndex);
				
				// Update the state of the previous/next handlers based on the data-selected-index attribute of the fixtures
				updatePreviousNext({initial: true});
			}
		});
	}
	
	/**
	* Initialize the handler for showing the mobile treatment of the current scores
	* @method mobileHandlersInit
	*/
	function mobileHandlersInit(){
		$('#fixtures-trigger').on('click',function(e){
			e.preventDefault();
			
			// Scroll the window to the top
			$.scrollTo({
				left: 0,
				top: 0
			},0);
			
			// Check to see whether the dropdown bar needs to be closed (if it's already expanded)
			if(matchCentre.dropdownBar.configuration.expanded){
				$('>UL LI',matchCentre.dropdownBar.configuration.dropdownBarObj)
					.removeClass('active')
					.end()
						.find('.flyout-content')
							.css({	
								height: 0,
								overflow: 'hidden'
							})
							
				matchCentre.dropdownBar.configuration.expanded = false;
				
				// Check to see whether a sub-group is expanded
				if(matchCentre.dropdownBar.configuration.subGroupExpanded){
					$('.module',matchCentre.dropdownBar.configuration.dropdownBarObj).remove();
					matchCentre.dropdownBar.configuration.subGroupExpanded = false;
				}
			}
			
			var heightToExpandTo;
			var originalHeight = parseInt(configuration.fixturesObj.attr('data-mobile-height'));
			var requiresInternalScrolling = false;
			
			// Determine whether the fixtures carousel requires internal scrolling or not
			if(!configuration.expanded){
				var internalScrollerObj = scrollerDimensions({
					fixturesContentObj: configuration.fixturesObj,
					originalHeight: originalHeight
				});
						
				// Internal scrolling is required
				if(originalHeight > internalScrollerObj.scrollHeight){
					heightToExpandTo = internalScrollerObj.scrollHeight;
					requiresInternalScrolling = true;
					
					// Set a data attribute, to define the need for an internal scroller
					configuration.fixturesObj.attr({
						'data-internal-scroller': true,
						'data-viewport-height': heightToExpandTo
					});
					
					// Add an internal <div> to attach internal padding values to that, since iScrol does not take into account the presence of paddings or margins when calculating the height of it's scroller
					setTimeout(function(){
						configuration.fixturesObj							
							.wrapInner(
								$('<div />')
									.addClass('internal-scroller')
							)
							
						configuration.iScrollObj = new IScroll('#' + configuration.fixturesObj.attr('id'),{
							click: true
						});

						// For mobile fixtures, centralize the matches in the best way possible
						if(configuration.centralizeOffsets.enabled){
							centralizeMatchesMobile();
						}
						
						// Disable (gesture-based) page scrollingm except for internal scrollers (iScroll)
						matchCentre.core.disableDefaultGestureScrolling();
					},configuration.timings.expandContract);
				}
				else{
					heightToExpandTo = originalHeight;
				}
			}
			else{
				// The carousel is being contracted, reset its height back to 0
				heightToExpandTo = 0;
				
				setTimeout(function(){
					if(configuration.fixturesObj.hasAttr('data-internal-scroller')){
						configuration.fixturesObj.removeAttr('data-internal-scroller');
						
						// Remove the internal iScroll <div /> DOM element
						$('.outer',configuration.fixturesObj).unwrap();
						
						// Enable (gesture-based) page scrolling
						matchCentre.core.enableDefaultGestureScrolling();
					}
				},configuration.timings.expandContract);
			}
			
			// Animate the carousel into its new height, whether being expanded or contracted
			configuration.fixturesObj
				.css({
					height: !configuration.expanded ? heightToExpandTo : 0,
					WebkitTransition: 'height ' + configuration.timings.expandContract + 'ms ease-in-out',
					transition: 'height ' + configuration.timings.expandContract + 'ms ease-in-out'
				})
							
			// Update boolean flag, representing if the fixtures is in an expanded state
			configuration.expanded = configuration.expanded ? false : true;
		});
	}
	
	/**
	* Determine whether the fixtures carousel requires internal scrolling or not
	* @method scrollerDimensions
	* @param {OBJECT} fixturesContentObj The container DOM object which contains the content for potential scrolling 
	* @param {INTEGER} originalHeight The natural height of the dropdown bar flyout content DOM object
	* @return {INTEGER} scrollHeight After how many pixels of a flyout contents depth internal scrolling should be activated
	* @return {BOOLEAN} scrollingRequired Whether or not internal scrolling should be activated or not
	*/
	function scrollerDimensions(obj){
		$.logEvent('[matchCentre.fixtures.scrollerDimensions]: ' + $.logJSONObj(obj));
		
		var contentOffsetTop = Math.floor(obj.fixturesContentObj.offset().top);
		var viewportHeight = window.innerHeight;		
		var scrollHeight = viewportHeight-contentOffsetTop;
		
		var dimensions = {
			scrollHeight: scrollHeight,
			scrollingRequired: obj.originalHeight > scrollHeight
		}
		
		return dimensions;
	}	
	
	/**
	* Initialize handlers for previous and next arrows
	* @method previousNextInit
	*/
	function previousNextInit(){
		$.logEvent('[matchCentre.fixtures.previousNextInit');
		
		var direction;
		var directionArithmetic;
		var linkObj;
		var selfObj = $(this);
		
		$('UL A',configuration.fixturesObj).on('click',function(e){
			e.preventDefault();
			
			// Do not process the handler if the site is in a locked state
			if(matchCentre.core.isSiteLocked()){
				return false;
			}
			
			linkObj = $(this);
			
			if(!linkObj.parent().hasClass('disabled')){
				direction = linkObj.hasClass('previous') ? 'previous' : 'next';
				directionArithmetic = (direction == 'previous') ? '-' : '+';
				
				// Lock the site
				matchCentre.core.siteLock({method: 'lock'});
									
				// Animate the fixtures to the new position
				$('.outer',configuration.fixturesObj).scrollTo({
					left: directionArithmetic + '=' + configuration.matchWidth + 'px',
					top: 0
				},configuration.timings.animateToNewMatch,{
					easing: configuration.easingEquation,
					onAfter: function(){
						// Update the state of the previous/next handlers based on the data-selected-index attribute of the fixtures
						updatePreviousNext({movementType: direction});
						
						// Unlock the site
						matchCentre.core.siteLock({method: 'unlock'});
					}
				});
			}
			else{
				return false;
			}
		});
	}	
	
	/**
	* Update the state of the previous/next handlers based on the data-selected-index attribute of the fixtures
	* @method updatePreviousNext
	* @param {STRING} movementType The direction of movement of the fixtures ('previous' || 'next')
	* @param {BOOLEAN} initial Whether this is the initial load of the fixtures or not
	*/
	function updatePreviousNext(obj){
		$.logEvent('[matchCentre.fixtures.updatePreviousNext]: ' + $.logJSONObj(obj));
			
		// Remove all previous selected states
		$('UL LI',configuration.fixturesObj).removeClass('disabled');
		
		// Having moved to a new position, update the data-selected-index property
		if(!obj.initial){
			var currentSelectedIndex = parseInt(configuration.fixturesObj.attr('data-selected-index'));			
			configuration.fixturesObj.attr('data-selected-index',currentSelectedIndex + ((obj.movementType == 'previous') ? -1 : 1));
		}
		else{
			// Handle the scenario where the carousel is not full
			if(parseInt(configuration.fixturesObj.attr('data-scores-contained')) <= configuration.matchesInViewport[matchCentre.configuration.deviceType]){
				$('UL A.next').parent().addClass('disabled');
			}
		}
		
		// Previous handler
		if(configuration.fixturesObj.attr('data-selected-index') == 0){
			$('UL A.previous').parent().addClass('disabled');
		}

		// There is a little more logic to work through for the disabling/enabling of the 'next' arrow
		if(matchCentre.core.isPositiveNumber(parseInt(configuration.fixturesObj.attr('data-overhang')))){
			// If the overhang is > 0, then there are hidden matches
			if(configuration.fixturesObj.attr('data-selected-index') == configuration.fixturesObj.attr('data-overhang')){
				$('UL A.next').parent().addClass('disabled');
			}
		}
	}
	
	/**
	* Resize the fixtures (inner match) widths, for mobile and tablet devices
	* @method resize
	* @param {STRING} deviceType 'mobile' || 'tablet' || 'desktop'		
	*/
	function resize(obj){
		$.logEvent('[matchCentre.fixtures.resize]: ' + $.logJSONObj(obj));
				
		// Adjust the match widths for mobile/tablet only
		if(obj.deviceType == 'tablet'){
			var fixturesWidth = $(window).width();
			var centralArea = fixturesWidth-50;
			var acceptableTotalWidth;
			var acceptableMatchWidth;
			var triggersOverhang;
			var unevenfixtures = false;
			
			for(var i=centralArea; i<=centralArea+5; i++){					
				if(i%5 == 0){
					acceptableTotalWidth = i;
					acceptableMatchWidth = i/5;
					break;
				}
			}
			
			// Calculate the width for the previous/next triggers
			triggersOverhang = Math.floor((fixturesWidth - (acceptableMatchWidth*5)) / 2);
		
			// Work out whether the previous/next triggers can be the same width or whether one has to be 1px wider than the other, due to an uneven pixel width of the fixtures
			if(!triggersOverhang%2 == 0){
				unevenfixtures = true;
			}
			
			// Set the width of .match elements
			configuration.matchWidth = acceptableMatchWidth;
			
			// Reset the width of the overall fixtures when resizing
			configuration.fixturesObj
				.css('width',fixturesWidth)
				.parent()
					.css('width',fixturesWidth);
			
			$('.match',configuration.fixturesObj).css('width',configuration.matchWidth);
			
			// Set the height of the previous/next triggers
			var triggersOverhangNext = unevenfixtures ? triggersOverhang : triggersOverhang-1;
			
			$('UL',configuration.fixturesObj)
				.find('A:eq(0)').css('width',triggersOverhang)
				.end()
				.find('A:eq(1)').css('width',triggersOverhangNext);
				
			// Set the width of the .inner element
			$('.inner',configuration.fixturesObj)
				.css({
					margin: '0 ' + (triggersOverhangNext-1) + 'px 0 ' + (triggersOverhang-1) + 'px',
					width: ((configuration.matchWidth * parseInt(configuration.fixturesObj.attr('data-scores-contained'))) + (triggersOverhang + triggersOverhangNext))
				});
				
			$.logEvent('[matchCentre.fixtures.resize]: windowWidth=' + $(window).width() + ', fixturesWidth=' + fixturesWidth + ', centralArea=' + centralArea + ', acceptableTotalWidth=' + acceptableTotalWidth + ', acceptableMatchWidth=' + acceptableMatchWidth + ', triggersOverhangPrevious=' + triggersOverhang + ', triggersOverhangNext=' + triggersOverhangNext + ', unevenfixtures=' + unevenfixtures);
		}
		else if(obj.deviceType == 'mobile'){
			configuration.fixturesObj
				.css('width','100%')
				.find('.inner')
					.css('width','100%')
		}
	}	
	
	/**
	* Process the Pusher real-time update
	* @method websocketCallback
	* @param {OBJECT} data A JSON update from Pusher
	*/	
	function websocketCallback(obj){
		$.logEvent('[matchCentre.fixtures.websocketCallback]: ' + $.logJSONObj(obj));
		
		var updatedMatchObj;
		var matchIds = [];
		
		// Capture any errors should the content for the Fixtures not be correct
		try{
			// Iterate through the In Summary JSON, and update the corresponding split bar with the new data
			$.each(obj.data.fixtures.content,function(index,dataObj){				
				matchIds.push(this.id);
				updatedMatchObj = $('#fixtures .match[data-match-id=' + matchIds[index] + ']');
				
				// There are 3 different types of update which can occur within the fixtures carousel:
				// 1. The textual status of the match can change eg: First half, Half time, Second half, Full time
				// 2. There can be a key incident eg: Goal, Red card, Sub
				// 3. There has been a change to the matches score
				
				if(this.hasOwnProperty('eventType')){
					// Add an overlay to the current match to show the key incident
					updatedMatchObj
						.prepend(
							$('<div />')
								.append(
									$('<p />')
										.html(this.eventType.replace(/-/g,' '))
								)
								.attr('class','event-update ' + this.eventType)
								.css('width',matchCentre.core.isMobile() ? '100%' : ((matchCentre.fixtures.configuration.matchWidth-1) + 'px'))
								.on('click',function(){
									top.location.href = $('A',$(this).parent()).attr('href');
								})
						)
		
					// Fade the event overlay into view
					$('.event-update',updatedMatchObj).fadeIn(configuration.updates.fadeInOut,function(){
						// At the point where the event is fully visible, initiate the blink effect of the inner text
						$('P',this)
							.blink({
								maxBlinks: configuration.updates.blinks, 
								blinkPeriod: configuration.updates.blinkPeriod, 
								onMaxBlinks: function(){					
									// Restore the team names behind the event text
									$('#fixtures .match[data-match-id=' + matchIds[index] + '] .event-update')
										.fadeOut(configuration.updates.fadeInOut,function(){
											$(this).remove();
										});
								}
							});
					});
				}

				// Update the textual status of the match
				if(matchCentre.configuration.isMatchDetailPage){
					$('TABLE THEAD TH',updatedMatchObj)
						.text(this.status);
				}
				else{
					$('TABLE',updatedMatchObj).attr('class',this.active ? 'active' : '')
						.find('THEAD TH').text(this.status);				
				}
				
				// Match scores can change in addition to event types (key incidents)
				if(this.hasOwnProperty('homeTeam')){
					$('TBODY TR:first TD',updatedMatchObj).text(this.homeTeam.score);
				}
				
				if(this.hasOwnProperty('awayTeam')){
					$('TBODY TR:last TD',updatedMatchObj).text(this.awayTeam.score);
				}
			});
		}
		catch(err){
			$.logEvent('[matchCentre.fixtures.websocketCallback]: an error occured populating the Fixtures component (via Pusher): ' + $.logJSONObj(err));
		}
	}
	
	/**
	* Reset the fixtures back to its initial load state
	* @method reset
	*/
	function reset(){
		$.logEvent('[matchCentre.fixtures.reset]');
		
		configuration.fixturesObj
			.attr('data-selected-index',0)
			.find('.outer').scrollTo({
				left: 0,
				top: 0
			},1)
			.find('.event-update') // Remove any event update overlays which may be currently visible
				.remove();
			
		// Update the state of the previous/next handlers based on the data-selected-index attribute of the fixtures
		updatePreviousNext({initial: true});
	}
	
	/**
	* Unload the fixtures
	* @method unload
	*/
	function unload(){
		$.logEvent('[matchCentre.fixtures.unload]');
		
		// Reset the left offset scroll position back to 0px
		$('.outer',configuration.fixturesObj).scrollTo({
			left: '0px',
			top: 0
		},1);
		
		// Remove the preset withs for the overall header element
		$('#header').css('width','');
		
		// Remove the preset widths for the previous/next handlers
		$('#header UL A').css('width','');
		
		// Reset the data-selected-index attribute back to 0, indicating that the first match is being viewed
		configuration.fixturesObj.attr('data-selected-index',0);
		
		// Remove preset margins for the .inner element
		$('.inner',configuration.fixturesObj).css('margin','');
		
		// Remove preset widths of all .match elements
		$('.match',configuration.fixturesObj).css('width','');
		
		// Remove the handler for showing the mobile treatment of the current scores
		$('#fixtures-trigger A').off('click');
		
		// Remove the handler covering all of the match area, taking the user to the match detail page
		$('.match TBODY A',configuration.fixturesObj).off('click');
	}
	
	return {
		build: build,
		configuration: configuration,
		init: init,
		reset: reset,
		resize: resize,
		unload: unload,
		websocketCallback: websocketCallback
	}
}());