#!/bin/sh

TO_REPO=$1

node makeTemplate.js
cp -R assets ${TO_REPO}/public/
rm ${TO_REPO}/public/assets/shared/scripts/matchCentre.core.js
rm ${TO_REPO}/public/assets/shared/scripts/matchCentre.footerBar.js
cp *.handlebars ${TO_REPO}/views/
