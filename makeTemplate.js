var fs = require ( 'fs' );

var matchDayLivePage = fs.readFileSync ( 'match-day-live.html' ).toString ();
var matchDetailPage = fs.readFileSync ( 'match-detail.html' ).toString ();
var jsCore = fs.readFileSync ( 'assets/shared/scripts/matchCentre.core.js' ).toString ();
var jsFooterBar = fs.readFileSync ( 'assets/shared/scripts/matchCentre.footerBar.js' ).toString ();

matchDetailPage = matchDetailPage.replace ( /<title>[^<]+<\/title>/, '<title>{{fixtureTitle}}</title>' );
matchDetailPage = matchDetailPage.replace ( /Match Centre title/, '{{fixtureTitle}}' );
matchDetailPage = matchDetailPage.replace ( /Match Centre description/, 'This is a trial of new, live football pages from the team at mirror.co.uk/football. If you have any comments or thoughts on this coverage, please leave some feedback on our MirrorFootball facebook page.\r\nWe hope you enjoy the matches! https://www.facebook.com/mirrorfootball?fref=ts' );
matchDetailPage = matchDetailPage.replace ( '<h1><a href="#">Mirror Football</a></h1>', '<h1><a href="{{baseURL}}/matchcentre-live">{{fixtureTitle}}</a></h1>' );
matchDetailPage = matchDetailPage.replace ( /<a\s+id=\"view\-all\-live\-games\"\s+href=\"[^\"]*\">/, '<a id="view-all-live-games" href="{{baseURL}}/matchcentre-live">' );
matchDetailPage = matchDetailPage.replace ( /http:\/\/development\.martinburford\.co\.uk/g, '{{baseURL}}' );
matchDetailPage = matchDetailPage.replace ( /fixtureId:[^\n]*\n/, 'fixtureId: {{fixtureId}},\r\n' );
matchDetailPage = matchDetailPage.replace ( /homeTeamId:[^\n]*\n/, 'homeTeamId: {{homeTeamId}},\r\n' );
matchDetailPage = matchDetailPage.replace ( /awayTeamId:[^\n]*\n/, 'awayTeamId: {{awayTeamId}},\r\n' );
matchDetailPage = matchDetailPage.replace ( /homeTeamName:[^\n]*\n/, 'homeTeamName: \'{{homeTeamName}}\',\r\n' );
matchDetailPage = matchDetailPage.replace ( /awayTeamName:[^\n]*\n/, 'awayTeamName: \'{{awayTeamName}}\',\r\n' );
matchDetailPage = matchDetailPage.replace ( /hometeam-vs-awayteam-YYYY-MM-DD/g, '{{fixtureIdentifierString}}' );

matchDetailPage = matchDetailPage.replace ( 'Heading text lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut', 'Live: {{homeTeamName}} vs {{awayTeamName}}' );
matchDetailPage = matchDetailPage.replace ( 'Summary text lorem ipsum dolor sit amet', 'This is a trial of new, live football pages from the team at mirror.co.uk/football. If you have any comments or thoughts on this coverage, please use the link at the top of the page to the feedback form.<br />We hope you enjoy the matches! @MirrorFootball<br /><br /><b>Please refresh your browser window once after the game has started.</b><br /><br />There is a known issue which causes a few of the elements on the page to only become active when the page loads while a game is in progress.<br />This would have been resolved by now, if we weren\'t so busy watching the game!' );

matchDayLivePage = matchDayLivePage.replace ( /http:\/\/development\.martinburford\.co\.uk/g, '{{baseURL}}' );
matchDayLivePage = matchDayLivePage.replace ( '<h1><a href="#">Mirror Football</a></h1>', '<h1><a href="{{baseURL}}/matchcentre-live">Mirror Football</a></h1>' );
matchDayLivePage = matchDayLivePage.replace ( 'Heading text lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut', 'Match Centre Live' );
matchDayLivePage = matchDayLivePage.replace ( 'Summary text lorem ipsum dolor sit amet', 'This is a trial of new, live football pages from the team at mirror.co.uk/football. If you have any comments or thoughts on this coverage, please use the link at the top of the page to the feedback form.<br />We hope you enjoy the matches! @MirrorFootball<br /><br /><b>Please refresh your browser window once after the game has started.</b><br /><br />There is a known issue which causes a few of the elements on the page to only become active when the page loads while a game is in progress.<br />This would have been resolved by now, if we weren\'t so busy watching the game!' );


jsCore = jsCore.replace ( /initialSiteContent:\s?\'[^\']*\'/, 'initialSiteContent: \'{{pulveriserUrl}}/matchCentre/main\'' );

jsFooterBar = jsFooterBar.replace ( /live:\s?\'[^\']*\'/, 'live: \'{{pulveriserUrl}}/matchCentre/footerBar\'' );

fs.writeFileSync ( 'single.handlebars', matchDetailPage );
fs.writeFileSync ( 'index.handlebars', matchDayLivePage );
fs.writeFileSync ( 'js-core.handlebars', jsCore );
fs.writeFileSync ( 'js-footerbar.handlebars', jsFooterBar );
